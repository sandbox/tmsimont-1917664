<?php

/**
 * Extends EntityAPIControllerExportable to add support for Field Instances
 */
class EntityAPIControllerExportableWithFields extends EntityAPIControllerExportable {

  /**
   * Save Pattern Type.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    $this->checkForDefaultFields($entity, $transaction);
    parent::save($entity, $transaction);
  }
  
  public function checkForDefaultFields($entity, $transaction = NULL) {
    try {
      // Load the stored entity, if any.
      if (!empty($entity->{$this->idKey}) && !isset($entity->original)) {
        // In order to properly work in case of name changes, load the original
        // entity using the id key if it is available.
        $entity->original = entity_load_unchanged($this->entityType, $entity->{$this->idKey});
      }
      $entity->is_new = !empty($entity->is_new) || empty($entity->{$this->idKey});
      if ($entity->is_new) {
        $this->attachDefaultFields($entity);
      }
    } catch (Exception $e) {
      if (isset($transaction)) {
        $transaction->rollback();
      }
      watchdog_exception($this->entityType, $e);
      throw $e;
    }
  }
  
  public function attachDefaultFields($entity) {
    //override and attach fields with importFields() to set defaults on type add
  }

  public function export($entity, $prefix = '') {
    $fields = field_info_fields();
    $entity_type = $this->entityInfo['bundle of'];
    $field_instances = field_info_instances($entity_type, $entity->type);
    if (count($field_instances)>0) {
      $entity->fields = array();
      foreach ($field_instances as $field_name => $field_instance) {
        $entity->fields[$field_name] = field_info_field($field_name);
        $entity->field_instances[$field_name] = $field_instance;
      }
    }
    return parent::export($entity, $prefix);
  }
  
  /**
   * Implements EntityAPIControllerInterface.
   *
   * @param $export
   *   A serialized string in JSON format as produced by the export() method.
   */
  public function import($export) {
    $this->importFields($export);
    return parent::import($export);
  }
  
  /**
   * Set up fields and instances for this bundle
   */
  public function importFields($export) {
    $vars = drupal_json_decode($export);
    if (isset($vars['field_instances'])) {
      $fields = $vars['field_instances'];
      foreach ($fields as $field_name => $instance) {
        //check for field, create if missing
        $field = field_info_field($instance['field_name']);
        if (!$field) {
          $field = isset($vars['fields'][$field_name])?$vars['fields'][$field_name]:false;
          if (!$field) {
            drupal_set_message(t("Export data is missing field definition for %field_name", array('%field_name' => $field_name)));
            return false;
          }
          field_create_field($field);
        } else {
          // drupal_set_message(t("A field that is already in use, %field_name, was added to this entity type.  Check that the field settings are appropriate for this instance.", array('%field_name' => $field_name)),"warning");
        }
        //check for instance, create if missing, update if existing
        $prior_instance = field_read_instance($instance['entity_type'], $instance['field_name'], $instance['bundle']);
        if (!$prior_instance) {
          field_create_instance($instance);
        } else {
          $instance['id'] = $prior_instance['id'];
          $instance['field_id'] = $prior_instance['field_id'];
          field_update_instance($instance);
        }
      }
    }
  }
  
  /**
   * Overridden to care about field instances.
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    $entities = $ids ? $this->load($ids) : FALSE;
    if ($entities) {
      parent::delete($ids, $transaction);
      foreach($entities as $entity) {
        $fields = field_info_fields();
        $entity_type = $this->entityInfo['bundle of'];
        $field_instances = field_info_instances($entity_type, $entity->type);
        foreach($field_instances as $instance) {
          field_delete_instance($instance);
        }
      }
    }
  }
  
}