<?php 

interface WeekBuilderInterface {

  public function setExceptions($list);
  public function exceptions();
  public function _getWeeks($data);
  public function getWeekBuilderData();
  
  /**
   * Get a "weeks array" -- an array of days in a week.
   * Each "day" in the weeks is an array with at least a 'dates' array.
   * The 'dates' array is a list of all the actual 1-time date occurrances
   * for the given day of the week.  Additional information may be returned
   * by implementors of this interface to sort out repeating rule intervals
   * or other data values required to sort out a pattern.
   *
   *  @return
   *   The array described above:
   *   array(
   *     [week number (0-3)] => array(
   *       [day number (0-6)] => array(
   *         [dates] => array(timestamp,timestamp,timestamp...)
   *         [other] => e.g "RRULE or week number relative to year"
   *       ),
   *       [day number (0-6)] => array(
   *         [dates] => array(timestamp,timestamp,timestamp...)
   *         [other] => e.g "RRULE or week number relative to year"
   *       )
   *     )
   *   )
   */
  public function weeks();
  
  public function getDay($yid, $wid, $did);
}


class WeekBuilder implements WeekBuilderInterface {

  public $pattern;
  
  public function __construct($pattern) {
    $this->pattern = $pattern;
  }

  public function setExceptions($list) {
    $this->_exceptions = $list;
  }

  public function exceptions() {
    if (isset($this->_exceptions)) {
      return $this->_exceptions;
    }
    return array();
  }
  
  public function _getWeeks($data) {
    return array();
  }

  public function getWeekBuilderData() {
  }
  
  public function weeks() {
    $data = $this->getWeekBuilderData();
    return $this->_getWeeks($data);
  }
  
  public function getDay($yid, $wid, $did) {
    $weeks = $this->weeks();
    return $weeks[$yid][$wid][$did];
  }
    
}

		
