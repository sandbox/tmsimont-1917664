<?php


/**
 * Implements hook_views_default_views().
 */
function dsm_views_default_views() {
  $view = new view();
  $view->name = 'dsm_calendar';
  $view->description = '';
  $view->tag = 'Calendar';
  $view->base_table = 'dateset';
  $view->human_name = 'DSM Calendar';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['link_display'] = 'page_1';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'date_views_pager';
  $handler->display->display_options['pager']['options']['date_id'] = 'month';
  $handler->display->display_options['style_plugin'] = 'calendar_style';
  $handler->display->display_options['style_options']['name_size'] = '3';
  $handler->display->display_options['style_options']['with_weekno'] = '0';
  $handler->display->display_options['style_options']['multiday_theme'] = '1';
  $handler->display->display_options['style_options']['theme_style'] = '1';
  $handler->display->display_options['style_options']['max_items'] = '0';
  $handler->display->display_options['row_plugin'] = 'calendar_entity';
  /* Field: Date set: Dates */
  $handler->display->display_options['fields']['dsm_dates']['id'] = 'dsm_dates';
  $handler->display->display_options['fields']['dsm_dates']['table'] = 'field_data_dsm_dates';
  $handler->display->display_options['fields']['dsm_dates']['field'] = 'dsm_dates';
  $handler->display->display_options['fields']['dsm_dates']['label'] = '';
  $handler->display->display_options['fields']['dsm_dates']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['dsm_dates']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['dsm_dates']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => '',
  );
  $handler->display->display_options['fields']['dsm_dates']['group_rows'] = FALSE;
  $handler->display->display_options['fields']['dsm_dates']['delta_offset'] = '0';
  /* Field: Date set: Date set ID */
  $handler->display->display_options['fields']['dsid']['id'] = 'dsid';
  $handler->display->display_options['fields']['dsid']['table'] = 'dateset';
  $handler->display->display_options['fields']['dsid']['field'] = 'dsid';
  /* Sort criterion: Date set: Dates -  start date (dsm_dates) */
  $handler->display->display_options['sorts']['dsm_dates_value']['id'] = 'dsm_dates_value';
  $handler->display->display_options['sorts']['dsm_dates_value']['table'] = 'field_data_dsm_dates';
  $handler->display->display_options['sorts']['dsm_dates_value']['field'] = 'dsm_dates_value';

  /* Display: Schedule Manager */
  $handler = $view->new_display('block', 'Schedule Manager', 'block_3');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'dsm_calendar_style';
  $handler->display->display_options['style_options']['name_size'] = '3';
  $handler->display->display_options['style_options']['mini'] = '0';
  $handler->display->display_options['style_options']['with_weekno'] = '0';
  $handler->display->display_options['style_options']['multiday_theme'] = '1';
  $handler->display->display_options['style_options']['theme_style'] = '1';
  $handler->display->display_options['style_options']['max_items'] = '0';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'calendar_entity';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Date set: Rendered Date set */
  $handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_dateset';
  $handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['label'] = '';
  $handler->display->display_options['fields']['rendered_entity']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['rendered_entity']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['rendered_entity']['display'] = 'view';
  $handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'schedule';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Date: Date (dateset) */
  $handler->display->display_options['arguments']['date_argument']['id'] = 'date_argument';
  $handler->display->display_options['arguments']['date_argument']['table'] = 'dateset';
  $handler->display->display_options['arguments']['date_argument']['field'] = 'date_argument';
  $handler->display->display_options['arguments']['date_argument']['default_action'] = 'default';
  $handler->display->display_options['arguments']['date_argument']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['date_argument']['add_delta'] = 'yes';
  $handler->display->display_options['arguments']['date_argument']['date_fields'] = array(
    'field_data_dsm_dates.dsm_dates_value' => 'field_data_dsm_dates.dsm_dates_value',
  );
  /* Contextual filter: Date set: Schedule(s) (dsm_sid) */
  $handler->display->display_options['arguments']['dsm_sid_target_id']['id'] = 'dsm_sid_target_id';
  $handler->display->display_options['arguments']['dsm_sid_target_id']['table'] = 'field_data_dsm_sid';
  $handler->display->display_options['arguments']['dsm_sid_target_id']['field'] = 'dsm_sid_target_id';
  $handler->display->display_options['arguments']['dsm_sid_target_id']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['dsm_sid_target_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['dsm_sid_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['dsm_sid_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['dsm_sid_target_id']['summary_options']['items_per_page'] = '25';

  /* Display: Schedule Manager - Week */
  $handler = $view->new_display('block', 'Schedule Manager - Week', 'block_4');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'dsm_calendar_style';
  $handler->display->display_options['style_options']['calendar_type'] = 'week_repeat';
  $handler->display->display_options['style_options']['name_size'] = '3';
  $handler->display->display_options['style_options']['mini'] = '0';
  $handler->display->display_options['style_options']['with_weekno'] = '0';
  $handler->display->display_options['style_options']['multiday_theme'] = '1';
  $handler->display->display_options['style_options']['theme_style'] = '1';
  $handler->display->display_options['style_options']['max_items'] = '0';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'calendar_entity';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['dsm_pid_target_id']['id'] = 'dsm_pid_target_id';
  $handler->display->display_options['relationships']['dsm_pid_target_id']['table'] = 'field_data_dsm_pid';
  $handler->display->display_options['relationships']['dsm_pid_target_id']['field'] = 'dsm_pid_target_id';
  $handler->display->display_options['relationships']['dsm_pid_target_id']['label'] = 'Pattern';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Date set: Rendered Date set */
  $handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_dateset';
  $handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['label'] = '';
  $handler->display->display_options['fields']['rendered_entity']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['rendered_entity']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['rendered_entity']['display'] = 'view';
  $handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'schedule';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Date: Date (dateset) */
  $handler->display->display_options['arguments']['date_argument']['id'] = 'date_argument';
  $handler->display->display_options['arguments']['date_argument']['table'] = 'dateset';
  $handler->display->display_options['arguments']['date_argument']['field'] = 'date_argument';
  $handler->display->display_options['arguments']['date_argument']['default_action'] = 'default';
  $handler->display->display_options['arguments']['date_argument']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['date_argument']['add_delta'] = 'yes';
  $handler->display->display_options['arguments']['date_argument']['date_fields'] = array(
    'field_data_dsm_dates.dsm_dates_value' => 'field_data_dsm_dates.dsm_dates_value',
  );
  /* Contextual filter: Date set: Pattern ID Reference (dsm_pid) */
  $handler->display->display_options['arguments']['dsm_pid_target_id']['id'] = 'dsm_pid_target_id';
  $handler->display->display_options['arguments']['dsm_pid_target_id']['table'] = 'field_data_dsm_pid';
  $handler->display->display_options['arguments']['dsm_pid_target_id']['field'] = 'dsm_pid_target_id';
  $handler->display->display_options['arguments']['dsm_pid_target_id']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['dsm_pid_target_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['dsm_pid_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['dsm_pid_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['dsm_pid_target_id']['summary_options']['items_per_page'] = '25';


  $views['dsm_calendar'] = $view;
  
  return $views;
}

