(function($){
/**
 * This file acts independently from the other main.js scripts.
 * This is called into the event_node_form for color picking
 */

Drupal.behaviors.schedule_manager_colorpicker = function(context){
	$("#schedule_manager-color_picker a").click(function(){
		$("#color-picker-picked").attr("class", "color-"+$(this).attr("rel"));
		$("#edit-field-color-0-value").val($(this).attr("rel"));
	});
}

})(jQuery);