(function($){
/**
 * Controller for ScheduleManager datesets
 */
ScheduleManager.controller.datesets = {
		
		busy:false,	
		
		/**
		 * retreive a datesset node/add form
		 * 
		 *  @param target
		 *   the dateset_dropzone target to which this dateset corresponds
		 *  
		 *  @param source
		 *   the source helper ui element carrying data from the event pool
		 */
		new_dateset: function(target, source){
			if(!this.busy){
				this.busy = true;
				ScheduleManager.controller.datesets.activity = "ADD";
				
				//store the requested data for callback use
				ScheduleManager.controller.datesets.hold_dateset(target,source);
				
				//open the dialog immediately, to acknowledge request
				ScheduleManager.dialog.open_relative(target, "New event placement");
	
				//put a new dateset node_form into the dialog
				ScheduleManager.dialog.jqdialog.load("/dateset/add/class_dates #dateset-form", function(){
				//ScheduleManager.dialog.jqdialog.load("/schedule-manager/get-dateset", function(){
					ScheduleManager.controller.datesets.transfer_data_from_dropzone_to_dateset(target);
					//transfer event nid to the dateset node form
					var event_nid = parseInt(ScheduleManager.controller.datesets.current_source.find("input[name='event_nid']").val());
					var event_title = ScheduleManager.controller.datesets.current_source.find("input[name='title']").val();
					$(ScheduleManager.dialog.selector).find('input[name="event_nid"]').val(event_nid);
					$(ScheduleManager.dialog.selector).find('.schedule-manager-event-ref-title h2').text(event_title);
					Drupal.attachBehaviors($(ScheduleManager.dialog.selector).dialog("widget"));
				});
			}
		},
		
		move_dateset:function(target, source){
			if(!this.busy){
				this.busy = true;
				ScheduleManager.controller.datesets.activity = "MOVE";
				//store the requested data for callback use
				ScheduleManager.controller.datesets.hold_dateset(target,source);

				//open the dialog immediately, to acknowledge request
				ScheduleManager.dialog.open_relative(target, "Move event placement");

				var dateset_nid = source.find("input[name='nid']").val();
				
				//put a new dateset node_form into the dialog
				ScheduleManager.dialog.jqdialog.load("schedule-manager/move-dateset/"+dateset_nid, function(){
					ScheduleManager.controller.datesets.transfer_data_from_dropzone_to_dateset(target);	
					Drupal.attachBehaviors($(ScheduleManager.dialog.selector).dialog("widget"));
				});
			}
		},
		
		/**
		 * retreive a datesset node/edit form 
		 * 
		 *  @param target
		 *   the dateset_dropzone target to which this dateset corresponds
		 *  
		 *  @param nid
		 *   the nid of the node to edit
		 */
		edit_dateset: function(target, nid){
			if(!this.busy){
				this.busy = true;
				ScheduleManager.controller.datesets.activity = "EDIT";
				
				//open the dialog immediately, to acknowledge request
				ScheduleManager.dialog.open_relative(target, "Edit event placement");
				
				this.editing_dateset_nid = nid;
	
				//put the node/edit form into the dialog for the requested nid
				ScheduleManager.dialog.jqdialog.load("/schedule-manager/get-dateset/"+nid, function(){
					ScheduleManager.controller.datesets.transfer_data_from_dropzone_to_dateset(target);
					Drupal.attachBehaviors($(ScheduleManager.dialog.selector).dialog("widget"));
				});
			}
		},
		
		/**
		 * Take the values stored in the dropzone that will help tie the new dateset
		 * back to this repeating pattern, and help create proper date repeats for 
		 * the new dateset
		 * 
		 * This meant to be a callback after the dialog has been populated by the dateset form
		 * It will not work unless the dialog is holding the node form
		 * 
		 * @param target
		 *  the dateset_dropzone target to which this dateset corresponds
		 */
		transfer_data_from_dropzone_to_dateset: function(target){
			$(ScheduleManager.dialog.selector).find('input[name="dates"]').val(target.find('input[name="dates"]').val());
			$(ScheduleManager.dialog.selector).find('input[name="rrule"]').val(target.find('input[name="rrule"]').val());
			$(ScheduleManager.dialog.selector).find('input[name="pid"]').val(target.find('input[name="pid"]').val());
			$(ScheduleManager.dialog.selector).find('input[name="wid"]').val(target.find('input[name="wid"]').val());
			$(ScheduleManager.dialog.selector).find('input[name="day"]').val(target.find('input[name="day"]').val());
			$(ScheduleManager.dialog.selector).find('input[name="year"]').val(target.find('input[name="year"]').val());
		},
		
		/**
		 * retreive a node/delete confirmation form 
		 * 
		 *  @param dateset
		 *   the element on the schedule representing the dateset node
		 *  
		 *  @param nid
		 *   the nid of the node to edit
		 *   
		 *  @param target
		 *   the dateset_dropzone target to which this dateset corresponds
		 */
		delete_dateset: function(dateset, nid, target){
			if(!this.busy){
				this.busy = true;
				ScheduleManager.controller.datesets.activity = "DELETE";
				ScheduleManager.controller.datesets.deleting_dateset = dateset;
				ScheduleManager.controller.datesets.deleting_target = target;
				
				//open the dialog immediately, to acknowledge request
				ScheduleManager.dialog.open_relative(dateset, "Remove event placement");
				
				//load the node/delete form for the requested nid
				ScheduleManager.dialog.jqdialog.load("/schedule-manager/delete-dateset/"+nid, function(){
					ScheduleManager.controller.datesets.transfer_data_from_dropzone_to_dateset(target);
					//override cancel link to not link anywhere, but instead close the dialog
					$("form#node-delete-confirm a").click(function(e){
						e.preventDefault();
						//close the dialog box, which also calls reset
						ScheduleManager.dialog.jqdialog.dialog('close');	
					});
					Drupal.attachBehaviors($(ScheduleManager.dialog.selector).dialog("widget"));
				});
			}
		},
		
		/**
		 * 
		 */
		deleting_dateset: {},
		deleting_target : {},
		/**
		 * what is the conroller doing?
		 * DELETE
		 * EDIT
		 * ADD
		 * MOVE
		 */
		activity: "",
		
		/**
		 * executes via Ajax plugin, when user submits a dateset form
		 */
		ajax_callback_submit: function(){
			if(ScheduleManager.controller.datesets.activity == "DELETE"){
				ScheduleManager.controller.datesets.deleting_dateset.remove();				
				ScheduleManager.controller.layout.row_grow.update_counter(-1, ScheduleManager.controller.datesets.deleting_target);
			}
			
			//if the user was handling a dateset
			if(ScheduleManager.controller.datesets.holding_dateset){
				if(ScheduleManager.controller.datesets.activity=="ADD"){
					//drop the dateset with source data on the target				
				  	ScheduleManager.controller.datesets.drop_new_dateset(
				  			ScheduleManager.controller.datesets.current_target,
				 			ScheduleManager.controller.datesets.current_source);			
				}
				if(ScheduleManager.controller.datesets.activity=="MOVE"){
					//drop the dateset with source data on the target				
				  	ScheduleManager.controller.datesets.drop_moving_dateset(
				  			ScheduleManager.controller.datesets.current_target,
				 			ScheduleManager.controller.datesets.current_source);	
				}
				
			}
			if(!ScheduleManager.controller.datesets.multiform){
				//close the dialog box, which also calls reset
				ScheduleManager.dialog.jqdialog.dialog('close');			
			}
			
			ScheduleManager.controller.datesets.activity = "";
		},
		

		/**
		 * hold a to-be placed dateset while the user creates a new node, it will be dropped after
		 * the node is actually saved
		 * 
		 * @param target
		 *  the target dropzone for the dateset
		 * @param source
		 *  the source helper containing event data
		 */
		hold_dateset: function(target, source){			
			ScheduleManager.controller.datesets.current_target = target;
			ScheduleManager.controller.datesets.current_source = source;
			ScheduleManager.controller.datesets.holding_dateset = true;
		},
		
		/**
		 * helper variables for hold_dateset -- 
		 * target, source and boolean for state
		 */
		current_target: {},
		current_source: {},
		holding_dateset: false,
		new_dateset_nid: 0,
		editing_dateset_nid: 0,
		
		/**
		 * drop a new dateset onto the calendar via drop_dateset, after first
		 * retreiving the html from the original event oncalendar contents
		 * 
		 * @param dropzone_container
		 *  the jquery parent object of a .date-dropzone-target element
		 * @param event
		 *  the jquery object container of an event
		 */
		drop_new_dateset: function(dropzone_container, event){			
			var html = '<div class="new-dateset event-placed-container-container" id="event-placed-container-'+
				ScheduleManager.controller.datesets.new_dateset_nid+'" >'+
				event.find(".oncalendar-contents").html()
				+'</div>';
			
			this.drop_dateset(dropzone_container, html);		
		  	//register events on the new dropped dateset
			Drupal.attachBehaviors("new-dateset");	

		},
		
		drop_moving_dateset: function(target, source){
			source.removeClass("behaviors-processed").removeClass("movable-processed");
			source.find(".behaviors-processed").removeClass("behaviors-processed");
			source.find(".movable-processed").removeClass("movable-processed");	
			source.find(".double-processed").removeClass("double-processed");			
			var html = source.wrap("<div>").parent().html();		
						
			ScheduleManager.controller.layout.row_grow.update_counter(-1, source.parents('.date-dropzone-container'));

			source.parent().remove();
				
			this.drop_dateset(target, html);	
		  	//register events on the moved dropped dateset
			Drupal.attachBehaviors("moved-dateset");		

		},
		
		/**
		 * 
		 * drop the held dateset onto the calendar, represented by the html passed into
		 * the function
		 * 
		 * @param dropzone_container
		 *  the jquery parent object of a .date-dropzone-target element
		 * @param event
		 *  the jquery object container of an event
		 *  
		 *  
		 */
		drop_dateset: function(dropzone_container, html){
			
			//place the dateset HTML into the dropzone, if there are 3 or less total
			dropzone_container.find('.date-dropzone-target').before(html);

			ScheduleManager.controller.layout.row_grow.update_counter(1, dropzone_container);
			
			
		  	//clear controller.datesets's held dateset
			ScheduleManager.controller.datesets.holding_dateset = false;
			ScheduleManager.controller.datesets.current_target = {};
			ScheduleManager.controller.datesets.current_source = {};
			this.reset();
		},
		
		//cancel operations by clearing state indication variables 
		reset: function(){
			this.busy = false;
			this.holding_dateset = false;
			this.current_source = {};
			this.current_target = {};
			this.new_dateset_nid = 0;
			this.editing_dateset_nid = 0;
			this.activity = "";
			this.deleting_dateset = {};
			this.deleting_target = {};
		}
}




/**
 * Behaviors for datesets that should be applicable to newly loaded elements,
 * and also exist for initiation on document ready
 */
ScheduleManager.behaviors.datesets = function(context){
	
	//initiate the JQuery dialog object
	if(!ScheduleManager.dialog.initalized) {
		ScheduleManager.dialog.set("#dateset_dialog");
  }
	
	//activate new clicks / hovers on newly placed datesets
	if(context=="new-dateset"){
		var obj = $('.new-dateset');
		obj.removeClass("behaviors-processed").find(".behaviors-processed").removeClass("behaviors-processed");
		obj.removeClass("new-dateset");
		ScheduleManager.behaviors.datesets.icon_hovers(obj);
		ScheduleManager.behaviors.datesets.drag_n_drop();
	}else{
		//all default placed elements
		ScheduleManager.behaviors.datesets.icon_hovers($("#events-schedule-dragndrop-container"));
		//activate drag and drop on events
		ScheduleManager.behaviors.datesets.drag_n_drop();
	}
	
	
	//jquery UI slider for week container
	//ScheduleManager.behaviors.datesets.slider();
	
	
};





/**
 * set hover class on all ui-icons
 * 
 * @param obj
 *  a jquery object containing the ui icon class 
 */
ScheduleManager.behaviors.datesets.icon_hovers = function(obj){
	//icon hovers/clicks on dateset elements
	obj.find("span.ui-icon:not(.behaviors-processed)").hover(function(){
			$(this).toggleClass("ui-state-icon-hover");
		},function(){
			$(this).toggleClass("ui-state-icon-hover");
	}).addClass("behaviors-processed");
	
	//edit button
	obj.find(".sm-list-event-edit:not(.behaviors-processed)").click(function(){
		var nid = $(this).siblings("input[name=nid]").val();
		ScheduleManager.controller.datesets.edit_dateset($(this).parents(".date-dropzone-zone"),nid);
	}).addClass("behaviors-processed");
	
	//delete button
	obj.find(".sm-list-event-delete:not(.behaviors-processed)").click(function(){
		var nid = $(this).siblings("input[name=nid]").val();
		ScheduleManager.controller.datesets.delete_dateset($(this).parents(".date-dropzone-zone .event-placed-container"),nid,$(this).parents(".date-dropzone-zone"));
	}).addClass("behaviors-processed");	
	
	
	//mover cancel
	$(".mover-cancel-close-dialog:not(.behaviors-processed)").click(function(){
		ScheduleManager.dialog.jqdialog.dialog('close');
	}).addClass("behaviors-processed");
};



/**
 * JQuery drag 'n drop functionality for events
 */
ScheduleManager.behaviors.datesets.drag_n_drop = function(context){	
	$(".event-draggable:not(.behaviors-processed)").draggable({
			opacity: 0.7,
			cursor: "move", 
			handle: ".event-instance-mover-icon",
			cursorAt:{
				top:5,
				left:30
			},
			helper: function( event ) {
				return $( $(this).find(".helper-contents").html() );
			},
			containment: "#content-main-inner-wrapper",
			snapTolerance: 60
		}).addClass("behaviors-processed");
	

	$(".dateset-event-instance-movable:not(.movable-processed)").draggable({
			appendTo : "#datesets-dropzone-container #dropzone-types",
			cursor: "move", 
			cursorAt:{
				top:5,
				left:30
			},
			helper: 'clone',
			start: function(){
				$(this).css('opacity',.7);
			},
			stop: function(){
				$(this).css('opacity',1);
			},
			containment: "#events-schedule-dragndrop-container",
			snapTolerance: 60
		}).addClass("movable-processed");
	
	
	
	
	$(".date-dropzone-container:not(.behaviors-processed)").droppable(
	{
			hoverClass: 'drop_hover',
			accept: ".event-draggable, .dateset-event-instance-movable",
			drop: function(event, ui){
				/*var str='';
					for(prop in event)
					{
					str+=prop + " value :"+ event[prop]+"\n";//Concate prop and its value from object
					}
					alert(str); //Show all properties and its value
				*/
				if(ui.draggable.hasClass("dateset-event-instance-movable")) {
					ScheduleManager.controller.datesets.move_dateset($(this),ui.draggable);
				} else {
					ScheduleManager.controller.datesets.new_dateset($(this),ui.draggable);
        }
				
			}
		}).addClass("behaviors-processed");
	
	
			
};


/**
 * JQuery UI Slider functionality
 */
ScheduleManager.behaviors.datesets.slider = function(context){		
	//scrollpane parts
	var scrollPane = $('.scroll-pane');
	var scrollContent = $('.date-dropzone-week');
	
	//build slider
	var scrollbar = $(".scroll-bar").slider({
		slide:function(e, ui){
			if( scrollContent.width() > scrollPane.width() ){ scrollContent.css('margin-left', Math.round( ui.value / 100 * ( scrollPane.width() - scrollContent.width() )) + 'px'); }
			else { scrollContent.css('margin-left', 0); }
		}
	});
	
	//append icon to handle
	var handleHelper = scrollbar.find('.ui-slider-handle')
	.mousedown(function(){
		scrollbar.width( handleHelper.width() );
	})
	.mouseup(function(){
		scrollbar.width( '100%' );
	})
	.append('<span class="ui-icon ui-icon-grip-dotted-vertical"></span>')
	.wrap('<div class="ui-handle-helper-parent"></div>').parent();
	
	//change overflow to hidden now that slider handles the scrolling
	scrollPane.css('overflow','hidden');
	
	
	var remainder = scrollContent.width() - scrollPane.width();
	var proportion = remainder / scrollContent.width();
	var handleSize = scrollPane.width() - (proportion * scrollPane.width());
	scrollbar.find('.ui-slider-handle').css({
		width: handleSize,
		'margin-left': -handleSize/2
	});
	handleHelper.width('').width( scrollbar.width() - handleSize);
};


})(jQuery);