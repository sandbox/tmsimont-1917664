(function($){
ScheduleManager.dialog = {
	
	initialized: false,
	
	/**
	 * Jquery UI options object for dialog
	 */
	dialog_options: {
		closeText: 'hide',
		closeOnEscape: false,
		modal:true,
		autoOpen: false,
		width: 450,
		minWidth: 450,
		close: function(event, ui) {
			ScheduleManager.dialog.reset();
		},
		resizable: true
	},

	jqdialog: {},
	selector: "",
	dialog_default: "",
	
	
	/**
	 * open the dialog centered over an element on the page
	 * @param target
	 *  the element over which the dialog will open
	 */
	open_relative: function(target, title){
		/*
		 * this would open centered over the clicked element:

		var dialog = ScheduleManager.dialog.jqdialog;
		var offset = target.offset();		
		var top = offset.top - 10;
		var left = offset.left + target.width()/2 - dialog.dialog("widget").width()/2;

		*/
		
		//put the dialog in the middle of the viewport..
		var dialog = ScheduleManager.dialog.jqdialog;
		var window_height= $(window).height();
		var window_width= $(window).width();
		var top = window_height/2 - 150;
		var left = window_width/2 - dialog.dialog("widget").width()/2;
		
		
		dialog.dialog('option', 'title', title);
		dialog.dialog('option',"position", [top,left]);
		dialog.dialog('open');
	},
	
	/**
	 * initialize the dialog onto an element
	 * @param selector
	 *  the element selector that is to become a dialog
	 */
	set: function(selector){
		ScheduleManager.dialog.initalized = true;
		ScheduleManager.dialog.dialog_default = $(selector).html();
		ScheduleManager.dialog.jqdialog = $(selector);
		ScheduleManager.dialog.selector = selector;
		$(selector).dialog(ScheduleManager.dialog.dialog_options);
	},
	
	/**
	 * reset the contents to the default loading icon, and 
	 * reset the dialog via UI destroy method
	 */
	reset: function(){
		ScheduleManager.dialog.jqdialog.dialog("destroy");
		ScheduleManager.dialog.jqdialog.html(ScheduleManager.dialog.dialog_default);
		ScheduleManager.dialog.jqdialog.dialog(ScheduleManager.dialog.dialog_options);
		ScheduleManager.controller.datesets.reset();
	}
};

})(jQuery);