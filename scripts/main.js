/** 
 * initiate a schedule manager namespace variable
 * 
 * TODO: perhaps refactor into function ScheduleManager(){} format with prototype-based definitions 
 * 
 */
var ScheduleManager = { init:false, nid:0, controller:{}, behaviors: {} };
(function($){

  /**
   * This will activate the schedule manager on $(document).ready() via Drupal 
   * behaviors object, after all definitions of schedule_manager are loaded 
   * 
   * see hook_init() in schedule_manager.module for list of loaded scripts
   */
  Drupal.behaviors.schedule_manager = {
    attach:function(context){
      
      if(ScheduleManager.init==false){
        ScheduleManager.nid = parseInt($("#schedule_manager-nid").val());
        ScheduleManager.controller.loader.loading_html = $("#schedule_manager-html-for-loading-state").html();
        ScheduleManager.init = true;
      }
      ScheduleManager.behaviors.datesets(context);
      ScheduleManager.behaviors.patterns(context);
      ScheduleManager.behaviors.layout(context);
      ScheduleManager.behaviors.calendar_navigator(context);
      ScheduleManager.behaviors.detail(context);

    }
  };

})(jQuery);