(function($){
/**
 * Controller for ScheduleManager detail page and behaviors
 */
ScheduleManager.controller.detail = {
	
	add_tab:function(dropzone){
	
		$("#dropzone-types").tabs("option","tabTemplate",
		"<li class='details-tab new-detail-tab'><a href='#{href}'>#{label}</a> <span class='ui-icon ui-icon-close'>Remove Tab</span></li>");

		$("#dropzone-types").tabs({
			   load: function(event, ui) {
					Drupal.attachBehaviors("#dropzone-types");
				}
			});
		
		var pid = dropzone.find(".schedule_manager-data_transfer input[name='pid']").val();
		var week = dropzone.find(".schedule_manager-data_transfer input[name='wid']").val();
		var day = dropzone.find(".schedule_manager-data_transfer input[name='day']").val();
		var year = dropzone.find(".schedule_manager-data_transfer input[name='year']").val();
		
		
		$("#dropzone-types").tabs("add", "/schedule-manager/day-view/"+pid+"/"+week+"/"+day+"/"+year,"Day detail");
		var id = $('#dropzone-types .new-detail-tab a').attr("href");
		$(id).html($("#schedule_manager-html-for-loading-state").html()).attr("rel",dropzone.attr("id")).addClass("detail-tab-container");
		$(id+" div").height(200);
		
		var index = $( "li", $("#dropzone-types") ).index( $('#dropzone-types .new-detail-tab') );
		$("#dropzone-types").tabs("select",index);
		$('#dropzone-types .new-detail-tab').removeClass("new-detail-tab");		
		
	},
	
	update_tabs:function(){
		
		var how_many = $("#dropzone-types").tabs("length");
				
		if(how_many>2){
			for( var i = 2; i<how_many; i++){
				var showing = false;
				$(".detail-tab-container").each(function(){
					$(this).html($("#schedule_manager-html-for-loading-state").html());
					$(this).children("div").height(200);
					if(!$(this).hasClass('ui-tabs-hide'))
						showing = i;
				});	
			}			
			//reload contents now if it's visible
			if(showing)
				$("#dropzone-types").tabs("load",showing);
		}
	}

}


ScheduleManager.behaviors.detail = function(obj){
	
	//tab
	$(".dateset-day_link-container:not(.behaviors-processed)").click(function(){
		ScheduleManager.controller.detail.add_tab($(this).parents(".date-dropzone-zone"));
	}).addClass("behaviors-processed");;
	
	$("#datesets-dropzone-container .ui-icon-close:not(.double-processed)").click(function(){		
		var index = $( "li", $("#dropzone-types") ).index( $( this ).parent() );
		$("#dropzone-types").tabs( "remove", index );
	}).addClass("double-processed");
	
	
	//edit link on datesets
	$(".detail-dateset-edit:not(.behaviors-processed)").click(function(){
		var nid = $(this).siblings("input[name=nid]").val();
		//get dropzone id from tab panel rel attribute:
		var id = "#"+$(this).parents(".ui-tabs-panel").attr("rel");
		ScheduleManager.controller.datesets.edit_dateset($(id),nid);
	}).addClass("behaviors-processed");
	
	//delete link on datesets
	$(".detail-dateset-delete:not(.behaviors-processed)").click(function(){
		var nid = $(this).siblings("input[name=nid]").val();
		//get dropzone id from tab panel rel attribute:
		var id = "#"+$(this).parents(".ui-tabs-panel").attr("rel");
		ScheduleManager.controller.datesets.delete_dateset($("#"+id+" .event-instance-dateset-"+nid),nid,$(id));
	}).addClass("behaviors-processed");	
	
	
}

})(jQuery);