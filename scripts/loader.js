(function($){
ScheduleManager.controller.loader = {
	is_requesting: false,
	loading_target : "",
	loading_html : "",
	loading_state : "",
	loading_container :"",
	
	set_target : function(selector){
		this.loading_target = selector;
		this.loading_state = "";
	},
	
	set_container : function(selector){
		this.loading_container = selector;
	},
	
	show_loader : function(){				
		if(this.loading_state != "loader"){
			//lock the height at a reasonable size ( >200 ), or its current size
			if($(this.loading_target).height()<200)
				$(this.loading_target).css("height", "210px");
			else
				$(this.loading_target).css("height", $(this.loading_target).height()+"px");
			
			$(this.loading_target).html(this.loading_html);
		}
		$(this.loading_container).show();
		this.loading_state = "loader";
	},
	
	begin_loading: function (show_loading){
		this.is_requesting = true;
		if(show_loading)
			this.show_loader();		
	},
	loaded : function(){
		this.is_requesting = false;
		//TODO: don't double-show?
		$(this.loading_container).show();
		Drupal.attachBehaviors($(this.loading_target));
		this.loading_state = "loaded";
		$(this.loading_target).css("height", "");
	},
	
	/**
	 * Use JQuery load function to load a sourceURL with "/" separated 
	 * arguments into the loader target, while also showing and hiding
	 * the loading icon when necessary over the designated target, while
	 * performing an optional additional callback on top of the necessary
	 * callbacks relating to this controller and the the loading icon
	 * 
	 * @param sourceURL
	 *  string, no trailing slash
	 *  
	 * @param args
	 *  an array of arguments that will be "/" separated
	 *  
	 * @param callback
	 *  function to call after load is complete
	 *  
	 */
	load_to_target : function(sourceURL, args, callback, show_loading){
		if(!this.is_requesting){
			
			arg_string = "";
			for(i in args){
				arg_string += "/"+args[i];
			}
								
			this.begin_loading(true);			
			$(ScheduleManager.controller.loader.loading_target).load(sourceURL+arg_string,
					function(){					
						ScheduleManager.controller.loader.loaded();
						if(callback != undefined)
							callback();
					}
			);
		}
	}
}

})(jQuery);