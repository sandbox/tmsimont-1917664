All these scripts used to create a drag/drop D6 calendar.  

That interface required certain content types to exist, used my own tables rather than entities,
and used <input> elements to pass data around between dialogs and node forms.

It was not migrateable to D7 for too many reasons and was abandoned.  All of this code 
remains here in case it needs to be revived.  It would probably be better to start from 
scratch.

They need to be removed...