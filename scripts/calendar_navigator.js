(function($){
/**
 * Controller for ScheduleManager calendar navigation
 */
ScheduleManager.controller.calendar_navigator = {
	initialized:false,
	init:function(){
		this.initialized = true;
	},
	loader:{
		change_month:function(amount){
			var current_month = parseInt($("#schedule_manager-month-picker-month").val());
			var current_year = parseInt($("#schedule_manager-month-picker-year").val());
			
			var target_month, target_year;
			
			if((current_month+amount)>12){
				target_month = 1;
				target_year = current_year+1;
			}else{
				if((current_month+amount)<1){
					target_month = 12;
					target_year = current_year-1;
				}else{
					target_month = (current_month+amount);
					target_year = current_year;
				}
			}
			
			//alert("target_month:"+target_month+", target_year:"+target_year)
			this.load_month(target_month, target_year);	
		},
		load_month:function(month,year){
			var args = [
				month,
				year,
				ScheduleManager.nid
			         ];
			ScheduleManager.controller.loader.set_target("#static-dates-calendar");
			ScheduleManager.controller.loader.set_container("#static-date-container");			
			ScheduleManager.controller.loader.load_to_target("/schedule-manager/get-month-pattern",args,function(){				
			});
		}
	}
}

ScheduleManager.behaviors.calendar_navigator = function(context){
	if(!ScheduleManager.controller.calendar_navigator.initialized)
		ScheduleManager.controller.calendar_navigator.init();

	$("#schedule_manager-month-nav-next:not(.behaviors-processed)").click(function(){
		ScheduleManager.controller.calendar_navigator.loader.change_month(1);
	}).addClass("behaviors-processed");
	$("#schedule_manager-month-nav-prev:not(.behaviors-processed)").click(function(){
		ScheduleManager.controller.calendar_navigator.loader.change_month(-1);
	}).addClass("behaviors-processed");
}

})(jQuery);