(function($){
/**
 * Ajax Forms plugin for ScheduleManager
 * 
 * @param {String} hook
 * @param {Object} args
 * @return {Bool}
 */
Drupal.Ajax.plugins.ScheduleManager = function(hook, args) {
	
	/**
	 * callback after the submission was successful
	 * 
	 */
	
	if (hook === 'complete') { 
		if(args['form_id']=="dateset_node_form"){
			//a user wants to add a new location for this dateset
			/*
			 * Handle the dateset like normal, but give the user an add location form
			 */
			if(args.messages_status[0]['value']=="add_new_location"){
				//args.messages_status[1][value] is the set message of the nid in hook_nodeapi... only way to get new nid
				if(args.messages_status[1]['value']==parseInt(args.messages_status[1]['value'])){
					//set the nid of the new dateset
					var nid = args.messages_status[1]['value'];
					ScheduleManager.controller.datesets.new_dateset_nid = nid;
					ScheduleManager.controller.datesets.current_source.find("input[name=nid]").val(nid);
				}else{
					var nid = ScheduleManager.controller.datesets.editing_dateset_nid;
				}
					
				ScheduleManager.controller.datesets.multiform = true;
				ScheduleManager.controller.datesets.ajax_callback_submit();
				ScheduleManager.controller.detail.update_tabs();
				
				ScheduleManager.dialog.jqdialog.html(ScheduleManager.dialog.dialog_default);					
				delete args.messages_status[0];
				delete args.messages_status[1];
				ScheduleManager.dialog.jqdialog.load("schedule-manager/get-location/"+nid, function(){
					Drupal.attachBehaviors();
				});
				
			/*
			 * Nothing location related going on..
			 */
			}else{
				//args.messages_status[0][value] is the set message of the nid in hook_nodeapi... only way to get new nid
				if(args.messages_status[0]['value']==parseInt(args.messages_status[0]['value'])){
					//set the nid of the new dateset
					var nid = args.messages_status[0]['value'];
					ScheduleManager.controller.datesets.new_dateset_nid = nid;
					ScheduleManager.controller.datesets.current_source.find("input[name=nid]").val(nid);
				}	
			
				ScheduleManager.controller.datesets.ajax_callback_submit();
				ScheduleManager.controller.detail.update_tabs();
			}
		}
		if(args['form_id']=="location_node_form"){
			ScheduleManager.controller.datesets.multiform = false;
			ScheduleManager.controller.datesets.ajax_callback_submit();
			ScheduleManager.controller.detail.update_tabs();
		}
	}
	
	if (hook === 'message') {
		//dateset move callback
		if(args['data']['form_id']=='_schedule_manager_move_dateset_form'){
			ScheduleManager.controller.datesets.ajax_callback_submit();
			ScheduleManager.controller.detail.update_tabs();
		}
		
		
		//node_delete_confirm doesn't hit "complete" -- but there's no reason it 
		//would show any other message than the completion confirmation 
		if(args['data']['form_id']=='node_delete_confirm'){
			ScheduleManager.controller.datesets.ajax_callback_submit();
			ScheduleManager.controller.detail.update_tabs();
		}
		
		if(args['data']['form_id']=="_schedule_manager_new_weekly_pattern_form"||
		args['data']['form_id']=="_schedule_manager_edit_weekly_pattern_form"){
			//if no errors...
			if(args['data'].messages_error.length ==0 ){
				var messages = args['data'].messages_status;
				if(args['data']['form_id']=="_schedule_manager_new_weekly_pattern_form"){
					/* 
					 * _schedule_manager_new_pattern_pass_back from 
					 * templates/patterns/patterns_weekly_pattern_form.inc
					 * passes back validated values in drupal_set_message, available via args['data'].messages_status
					 */
					ScheduleManager.controller.patterns.get_new_weekly_pattern(
						messages[0]['value'],
						messages[1]['value'],
						messages[2]['value'],
						messages[3]['value'],
						messages[4]['value']
					);
				}else{
					/*
					 * _schedule_manager_new_pattern_pass_back from 
					 * templates/patterns/patterns_weekly_pattern_form.inc
					 * passes back validated values in drupal_set_message, available via messages
					 */
					ScheduleManager.controller.patterns.update_showing_weekly_pattern(
						messages[0]['value'],
						messages[1]['value'],
						messages[2]['value'],
						messages[3]['value'],
						messages[4]['value']
					);
					ScheduleManager.controller.detail.update_tabs();
				}
				//cancel display of messages
				delete args['data'].messages_status[0];
				delete args['data'].messages_status[1];
				delete args['data'].messages_status[2];
				delete args['data'].messages_status[3];
				delete args['data'].messages_status[4];
				
				args['options'].action='clear';
			}else{
				/*
				var length = args['data'].messages_error.length;
				args['data'].messages_error[length] = {
					id :length,
					value : "<script type='text/javascript'>ScheduleManager.controller.layout.resize_accordion()</script>"
					}
					*/
			}
				    
		}
		
		//fix "loading.." text
		ScheduleManager.dialog.jqdialog.dialog('widget').find("#edit-submit").val("Save");
		
	}
	
		
	
	return true;
};

})(jQuery);