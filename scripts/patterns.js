(function($){
ScheduleManager.controller.patterns = {
		
		/**
		 * make sure the user has entered values into the m_start and m_finish inputs
		 */
		input_datesets : {
			initialized : false,
			input_start : "",
			input_finish : "",
			/**
			 * initialize the tracker
			 * @param start_input
			 *  JQuery selector for an input element object for start date
			 * @param end_input
			 *  JQuery selector for an input element object for end date
			 */
			init : function(start_input, end_input){
				this.input_start = start_input;
				this.input_finish = end_input;
								
				/**
				 * http://jqueryui.com/demos/datepicker/#event-search
				 * Use the datepicker to restrict TO dates based on chosen FROM date
				 */
				var dates = $(this.input_start+","+this.input_finish).datepicker({
					onSelect: function(selectedDate) { 						
						var option = this.id == $(start_input).attr("id") ? "minDate" : "maxDate",
							instance = $( this ).data( "datepicker" );
							date = $.datepicker.parseDate(
								instance.settings.dateFormat ||
								$.datepicker._defaults.dateFormat,
								selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
				});				
				this.intialized = true;
			}
		},

		
		get_new_weekly_pattern : function(schedule_nid, start, finish, num_weeks,title){

			var args = [
				schedule_nid,
				start,
				finish,
				num_weeks				
				];

			//show the pattern table
			$("#weekly-patterns-accordion").accordion("activate",1);
			

			ScheduleManager.controller.loader.set_target("#date-dropzone-week-new");
			ScheduleManager.controller.loader.set_container("#currently-editing-weekly-pattern");			
			ScheduleManager.controller.loader.load_to_target("/schedule-manager/get-weekly-repeat",args,function(){

				ScheduleManager.controller.patterns.clear_table_classes();
				
				//add the newly created pattern in the pattern table 
				var new_pid = $("#currently-editing-pattern").val();
				$("#schedule-patterns-existing-weekly-patterns .odd").removeClass("odd").addClass("even-t");
				$("#schedule-patterns-existing-weekly-patterns .even").removeClass("even").addClass("odd-t");
				$("#schedule-patterns-existing-weekly-patterns .odd-t").removeClass("odd-t").addClass("odd");
				$("#schedule-patterns-existing-weekly-patterns .even-t").removeClass("even-t").addClass("even");
				$("#schedule-patterns-existing-weekly-patterns tbody tr:first").before(
						Drupal.theme("existing_table_row",title,start,finish,new_pid,num_weeks));
				$("#schedule-patterns-existing-weekly-patterns .empty-table-notification").remove();				
				
				//revert the entry form
				$("#get-weekly-datepicker input#m_start, #get-weekly-datepicker input#m_finish").val("").datepicker("refresh");
				$("#edit-title").val("");
				$("#get-week").text("Get new weekly pattern");

				ScheduleManager.controller.layout.resize_accordion();
			});	
			
		},
		
		/**
		 * Create an existing weekly pattern by pid
		 */
		get_exisiting_weekly_pattern : function(pid){
			var args = [pid, ScheduleManager.nid];

			ScheduleManager.controller.loader.set_target("#date-dropzone-week-new");
			ScheduleManager.controller.loader.set_container("#currently-editing-weekly-pattern");			
			ScheduleManager.controller.loader.load_to_target("/schedule-manager/get-existing-pattern",args,function(){
				ScheduleManager.controller.layout.resize_accordion();
			});
		},
		get_exisiting_weekly_pattern_data : function(pid,callback){
			console.log("/schedule-manager/get-existing-pattern/"+pid+'/'+ScheduleManager.nid);
			$.get("/schedule-manager/get-existing-pattern/"+pid+'/'+ScheduleManager.nid,function(html){
				var data = {
					num_weeks : $("#weekly-pattern-pattern-data #currently-editing-pattern-num_weeks").val(),
					m_start : $("#weekly-pattern-pattern-data #currently-editing-pattern-start").val(),
					m_finish : $("#weekly-pattern-pattern-data #currently-editing-pattern-finish").val()	
				};
				callback(data);
			});
		},
		
		showing_pattern_id : 0,
		edit_showing_weekly_pattern : function(link){		
			this.showing_pattern_id = link.attr("rel");
			var pid = this.showing_pattern_id;
			var num_weeks = $("#weekly-pattern-pattern-data #currently-editing-pattern-num_weeks").val();
			var m_start = $("#weekly-pattern-pattern-data #currently-editing-pattern-start").val();
			var m_finish = $("#weekly-pattern-pattern-data #currently-editing-pattern-finish").val();	
			ScheduleManager.controller.patterns.edit_given_weekly_pattern(link,pid, num_weeks, m_start, m_finish);
		},		
		edit_given_weekly_pattern:function(relto,pid, num_weeks, m_start, m_finish){		
			//open the dialog immediately, to acknowledge request
			ScheduleManager.dialog.open_relative(relto, "Edit pattern");
			//load the node/delete form for the requested nid
			ScheduleManager.dialog.jqdialog.load("/schedule-manager/edit-weekly-pattern/"+ScheduleManager.nid +
			  "/"+m_start+"/"+m_finish+"/"+num_weeks, function(){				
				Drupal.attachBehaviors($(ScheduleManager.dialog.selector).dialog("widget"));
			});
		},
		
		update_showing_weekly_pattern : function(schedule_nid, start, finish, num_weeks,title){
			var pid = this.showing_pattern_id;
			var args = [
				pid,
				schedule_nid,
				start,
				finish,
				num_weeks				
				];

			ScheduleManager.controller.loader.set_target("#date-dropzone-week-new");
			ScheduleManager.controller.loader.set_container("#currently-editing-weekly-pattern");			
			ScheduleManager.controller.loader.load_to_target("/schedule-manager/update-and-reget-pattern",args,function(){
				//inject updated table row in place of the selected row
				$("#schedule-patterns-existing-weekly-patterns .selected").replaceWith(
					Drupal.theme("existing_table_row",title,start,finish,pid,num_weeks)
				);
				//close the dialog box, which also calls reset
				ScheduleManager.dialog.jqdialog.dialog('close');	
			});	
			
		},
		
		
		clear_table_classes:function(){
			$(".existing-weekly-pattern").each(function(){
				if($(this).hasClass("hover"))
					$(this).removeClass("hover");
				if($(this).hasClass("selected"))
					$(this).removeClass("selected");
			});
		}
		
}


ScheduleManager.behaviors.patterns = function(context){
	//new pattern form
	ScheduleManager.behaviors.patterns.new_pattern_form(context)
	//created pattern table:
	ScheduleManager.behaviors.patterns.created_patterns_table(context);
	
	$("#edit-pattern:not(.behaviors-processed)").click(function(){
		ScheduleManager.controller.patterns.edit_showing_weekly_pattern($(this));
	}).addClass("behaviors-processed");
}




ScheduleManager.behaviors.patterns.new_pattern_form = function(context){
	if(!ScheduleManager.controller.patterns.input_datesets.initialized){
		ScheduleManager.controller.patterns.input_datesets.init(
				"#get-weekly-datepicker input#m_start",
				"#get-weekly-datepicker input#m_finish"
				);
	}
}
ScheduleManager.behaviors.patterns.created_patterns_table = function(context){
	//edit/delete links on table
	/*
	//TODO: alter ajax plugin reaction to form submission if it's an edit that may have edited title
	$(".existing-weekly-pattern:not(.behaviors-processed) .ewp-pattern-edit").click(
		function(){
			var link = $(this);
			var pid = $(this).attr('pid');
			ScheduleManager.controller.patterns.get_exisiting_weekly_pattern_data(pid,			
				function(data){
					ScheduleManager.controller.patterns.edit_given_weekly_pattern(
						link,pid, data.num_weeks, data.m_start, data.m_finish
					);
				});
		}
	);
	//TODO: alter ajax plugin reaction to delete confirm submission
	//TODO: build delete confirm form, this is not a node
	//TODO: poop pants... maybe move this into a loaded pattern interface, as edit currently works
	$(".existing-weekly-pattern:not(.behaviors-processed) .ewp-pattern-delete").click(
		function(){
			var link = $(this);
			var pid = $(this).attr('pid');
			ScheduleManager.controller.patterns.get_exisiting_weekly_pattern_data(pid,			
				function(data){
					ScheduleManager.controller.patterns.edit_given_weekly_pattern(
						link,pid, data.num_weeks, data.m_start, data.m_finish
					);
				});
		}
	);
	*/

	//hover and click chained
	$(".existing-weekly-pattern:not(.behaviors-processed)").hover(
			function(){$(this).toggleClass("hover")},
			function(){$(this).toggleClass("hover")}).click(function(e){
			if(e.target!='javascript:void(0)'){
				ScheduleManager.controller.patterns.clear_table_classes();
				$(this).addClass("selected");
				ScheduleManager.controller.patterns.get_exisiting_weekly_pattern($(this).find('input[name="pid"]').val());
			}
	}).addClass("behaviors-processed");
	
	
}

})(jQuery);