(function($){
ScheduleManager.behaviors.layout = function(context){
	if(!ScheduleManager.controller.layout.initialized)
		ScheduleManager.controller.layout.init();
	
	//dateset rows:
	$(".date-dropzone-zone .dateset-more-show-link:not(.behaviors-processed)").click(function(){
		ScheduleManager.controller.layout.row_grow.expand($(this));
	}).addClass("behaviors-processed");
	$(".date-dropzone-zone .dateset-more-collapse-link:not(.behaviors-processed)").click(function(){
		ScheduleManager.controller.layout.row_grow.collapse($(this));
	}).addClass("behaviors-processed");
	
}

ScheduleManager.controller.layout = {
	initialized:false,
	init:function(){
		if(Drupal.settings.schedule_manager.default_datesets_showing_per_day)
			this.row_grow.show_by_default = Drupal.settings.schedule_manager.default_datesets_showing_per_day;
	
		//accordion
		var active = 0;
		if(Drupal.settings.schedule_manager){
			if(Drupal.settings.schedule_manager.has_patterns)
				active = 1;
		}
		
		$("#weekly-patterns-accordion").css("height","400px");
		
		$("#weekly-patterns-accordion").accordion({active: active, fillSpace:true});
		
		//tabs		
		$("#dropzone-types").tabs();
		this.initialized = true;
	},
	resize_accordion:function(){
		//unlock the height
		$("#weekly-patterns-accordion").css("height","");
		//resize accordion
		$("#weekly-patterns-accordion").accordion("resize");
		//re-lock height to prevent jumpiness
		$("#weekly-patterns-accordion").css("height",$("#weekly-patterns-accordion").height()+"px");
	},
	
	row_grow:{
		dateset_height:25,
		show_by_default:2, // overridden on init by Drupal.settings.schedule_manager.default_datesets_showing_per_day if set
		
		expand:function(source){		
			var placed_sets = source.parents(".date-dropzone-zone").find(".date-dropzone-list input[name=dropped-count]").val();
			var new_height = this.dateset_height*placed_sets;
			
			// for all cells to grow together: $(this).parents("tr").find(".date-dropzone-list").animate({
			// only 1 cell because making others taller would make life hard ( update all "more" counters )
			source.parents(".date-dropzone-zone").find(".date-dropzone-list").animate({
			    height: new_height
			  }, 100, function() {
				  
			  }).addClass('row-grow-indicate-expanded');
			
			source.hide();
			source.parents(".date-dropzone-zone").find('.dateset-more-collapse-link').show();
		},
		collapse:function(source){
			var new_height = this.dateset_height*this.show_by_default;

			// for all cells to grow together: $(this).parents("tr").find(".date-dropzone-list").animate({
			// only 1 cell because making others taller would make life hard ( update all "more" counters )
			source.parents(".date-dropzone-zone").find(".date-dropzone-list").animate({
			    height: new_height
			  }, 100, function() {
				  
			  }).addClass('row-grow-indicate-collapsed');
			
			source.hide();
			source.parents(".date-dropzone-zone").find(".dateset-more-show-link").show();
			
			//make sure the count text is going to be accurate
			var count = parseInt(
					source.parents(".date-dropzone-zone").find('input[name=dropped-count]').val()
				);
			if(count>this.show_by_default){
				count -= this.show_by_default;
				source.parents(".date-dropzone-zone").find('.dateset-more-count').text(count);
			}
			
		},

		update_counter: function(update, dropzone_container){

			var count = parseInt(
					dropzone_container.find('input[name=dropped-count]').val()
				);
			count += update;
			
			dropzone_container.find('input[name=dropped-count]').val(count);
			dropzone_container.find('.dateset-more-count').text((count-this.show_by_default));
			
			//if there are more than default showing, display the more notice
			if(count>this.show_by_default)
				dropzone_container.find('.dateset-more-notice').show();
			else{
				dropzone_container.find('.dateset-more-notice').hide();
				if(dropzone_container.find(".date-dropzone-list").hasClass('row-grow-indicate-expanded'))
					ScheduleManager.controller.layout.row_grow.collapse(dropzone_container.find(".dateset-more-collapse-link"));
			}
			
			//if the list is already expanded..
			if(dropzone_container.find(".date-dropzone-list").hasClass('row-grow-indicate-expanded')){
				var currently_showing = dropzone_container.find(".date-dropzone-list").height() / this.dateset_height; 
				count -= currently_showing;
				//if adding, indicate that there are now even more
				if(count>0){
					dropzone_container.find('.dateset-more-collapse-link').hide();
					dropzone_container.find('.dateset-more-show-link').show();
					dropzone_container.find('.dateset-more-count').text(count);
				}else{
					dropzone_container.find('.dateset-more-collapse-link').show();
					dropzone_container.find('.dateset-more-show-link').hide();
				}
				
				
			}	
			
			if(count==0)
				dropzone_container.find('.dateset-day_link-container').hide();
			else
				dropzone_container.find('.dateset-day_link-container').show();
			
				
		}
		
	}
}



Drupal.theme.prototype.existing_table_row = function(title,start,end,pid,num_weeks){
	var row = '<tr class="existing-weekly-pattern odd selected"><td>'+title+'</td><td>'+start;
	row += '</td><td>'+end+'</td><td>'+num_weeks+'-week<input type="hidden" name="pid" value="';
	row += pid+ '"></td>';
	//TODO: see patterns.js
	//row += '<td><a href="javascript:void(0)" class="ewp-pattern-op ewp-pattern-edit" pid="'+pid+'">edit</a>';
	//row += ' | <a href="javascript:void(0)" class="ewp-pattern-op ewp-pattern-delete" pid="'+pid+'">delete</a></td>';
	row += '</tr>';
	return row;
}

})(jQuery);