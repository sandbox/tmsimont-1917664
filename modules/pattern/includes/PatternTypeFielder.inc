<?php

interface PatternTypeFielderInterface {
  public function attachFields($entity);
}

class PatternTypeFielder extends EntityAPIControllerExportableWithFields implements PatternTypeFielderInterface {
  public function attachFields($entity) {
  }
}