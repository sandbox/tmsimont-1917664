<?php

//@ todo: move this plugin to dsm -- extend for pattern/schedule
  
class pattern_plugin_style extends calendar_plugin_style {
  function get_builder() {
    $builder_type = 'pattern_builder_' . $this->options['calendar_type'];
    
    //after the builder is set, switch back "week_repeat" type to "month" to trick theme functions
    if ($this->options['calendar_type'] == 'week_repeat') {
      $this->options['calendar_type'] = 'month';
      $this->date_info->calendar_type = $this->options['calendar_type'];
    }
    
    return new $builder_type($this);
  }
  
  function query() {
    //force all sets to be displayed regardless of date range for a particular pattern
    if ($this->options['calendar_type'] =="week_repeat") {
      unset($this->view->query->where['date']);
    }
    parent::query();
  }
  
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['calendar_type']['#options']['week_repeat'] = "Repeating weeks";
    unset($form['calendar_type']['#options']['year']);
    unset($form['calendar_type']['#options']['week']);
    unset($form['calendar_type']['#options']['day']);
  }
  
  //@ todo: move this plugin to dsm -- extend for pattern/schedule
  function get_pattern() {
    if (empty($this->view->args[1])) {
      return false;
    } 
    $id = $this->view->args[1];
    if (isset($this->view->argument['dsm_pid_target_id'])) {
      $pattern = pattern_load($id);
      return $pattern;
    } elseif (isset($this->view->argument['dsm_sid_target_id'])) {
      $schedule = schedule_load($id);
      return $schedule;
    }
    return false;
  }
}

class pattern_builder_month extends calendar_builder_month {
  function get_week_builder() {
    return new pattern_builder_week($this->plugin);
  }
}

class pattern_builder_week extends calendar_builder_week {
  function get_day_builder($wday) {
    return new pattern_builder_day_weekday_setdate($this, $wday);
  }
}

class pattern_builder_weekday extends calendar_builder_day_weekday {
  function add_new_dateset_link($type, $id, $yid, $wid, $did) {
    $uid = arg(1);
    $addlink = l('Add', "dsm/$uid/$type/$id/$yid/$wid/$did/dateset/add", array("query"=>drupal_get_destination()));
    $this->week->add_singleday_bucket($this->wday, $addlink);
  }
}

class pattern_builder_day_weekday_setdate extends pattern_builder_weekday {
  function calendar_build_day() {
    parent::calendar_build_day();
    $schedule = $this->plugin->get_pattern();
    $sid = $schedule->sid;
    $yid = intval($this->plugin->curday->format("Y"));
    $wid = intval($this->plugin->curday->format("W"));
    $did = $this->wday;
    $this->add_new_dateset_link('schedule', $sid, $yid, $wid, $did);
  }
}




class pattern_builder_week_repeat extends calendar_builder_month {

  /**
   * Build one month.
   */
  function calendar_build_month() {
    //only continue if the pattern is known
    if(!($pattern = $this->plugin->get_pattern())) {
      return parent::calendar_build_month();
    }
    $start = $pattern->builder->startDate;
    $end = $pattern->builder->endDate;
    
    // start on a week of the month that has all of the repeats in it
    if ($pattern->builder->initDay > 0) {
      date_modify($start, '+ ' . $pattern->builder->interval . ' days');
    }
    
    $plugin = $this->plugin;
    $plugin->curday = clone($start);
    $this->month = date_format($plugin->curday, 'n');
    $this->curday_date = date_format($plugin->curday, DATE_FORMAT_DATE);
    
    //start at the beginning of the starting week
    date_modify($plugin->curday, '-' . strval(date_format($plugin->curday, 'w')) . ' days');
    
    //end at the end of 1 pattern cycle
    if ($pattern->builder->initDay > 0) {
      $end_of_cycle = clone($plugin->curday);
      date_modify($end_of_cycle, '+ ' . ($pattern->builder->interval - 1) . ' days');
    }
    
    $rows = array();
    $plugin->wid = 0;
    do {
      $init_day = clone($plugin->curday);
      $this->month = date_format($plugin->curday, 'n');

      //use a week builder to grab week buckets and total rows count
      $week_builder = $this->get_week_builder();
      $week_rows = $week_builder->calendar_build_week(TRUE);
      $this->multiday_buckets = $week_rows['multiday_buckets'];
      $this->singleday_buckets = $week_rows['singleday_buckets'];
      $this->total_rows = $week_rows['total_rows'];
      $final_day = clone($plugin->curday);

      $output = $this->calendar_month_build_weeks($init_day);

      $plugin->curday = $final_day;

      // Add the row into the row array....
      $rows[] = array('data' => $output);
      $this->curday_date = date_format($plugin->curday, DATE_FORMAT_DATE);
      $plugin->wid++;
    } while ($this->curday_date <= date_format($end_of_cycle, DATE_FORMAT_DATE));
    // Merge the day names in as the first row.
    $rows = array_merge(array(calendar_week_header($plugin->view)), $rows);
    return $rows;
  }
  
  /**
   * Don't show dateboxes because each date represents numerous dates
   */
  function get_datebox($wday) {
    return array();
  }
  function process_multiday_buckets($index, $wday) {
    //cancel the "today" class when processing buckets -- don't wanna see that in this context
    $this->today = false;
    return parent::process_multiday_buckets($index, $wday);
  }
  function process_singleday_buckets($index, $wday, $multi_count) {
    //cancel the "today" class when processing buckets -- don't wanna see that in this context
    $this->today = false;
    return parent::process_singleday_buckets($index, $wday, $multi_count);
  }
  /**
   * Don't consider dates as in another month because each date represents numerous dates
   */
  function in_another_month() {
    return false;
  }
  
  function get_week_builder() {
    return new pattern_builder_week_repeats($this->plugin);
  }
  
}


class pattern_builder_week_repeats extends pattern_builder_week {
  function in_another_month() {
    return false;
  }
  function get_day_builder($wday) {
    return new pattern_builder_day_repeating_weekday($this, $wday);
  }
}



class pattern_builder_day_repeating_weekday extends pattern_builder_weekday {
  function calendar_build_day() {
    parent::calendar_build_day();
    $pattern = $this->plugin->get_pattern();
    $pid = $pattern->pid;
    $yid = 0;
    $wid = $this->plugin->wid;
    $did = $this->wday;
    $this->add_new_dateset_link('pattern', $pid, $yid, $wid, $did);
  }
}