<?php

class PatternTypeController extends EntityAPIControllerExportableWithFields {
   public function create(array $values = array()) {
    $values += array(
      'label' => '',
      'description' => '',
    );
    return parent::create($values);
  }

  /**
   * Save Pattern Type.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    parent::save($entity, $transaction);
    // Rebuild menu registry. We do not call menu_rebuild directly, but set
    // variable that indicates rebuild in the end.
    // @see http://drupal.org/node/1399618
    variable_set('menu_rebuild_needed', TRUE);
  }
  
  public function attachDefaultFields($entity) {
    //save type_of_pattern
    $fielderType = "PatternTypeFielder" . $entity->type_of_pattern;
    $fielder = new $fielderType('pattern');
    $fielder->attachFields($entity);
  }
  
}