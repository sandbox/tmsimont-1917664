<?php


class PatternController extends EntityAPIController {

  public function create(array $values = array()) {
    global $user;
    $values += array(
      'title' => '',
      'description' => '',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
    );
    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('pattern', $entity);
    
    // @todo: make use of settings on "Manage display" screen
    
    //$content['author'] = array('#markup' => t('Created by: !author', array('!author' => $wrapper->uid->name->value(array('sanitize' => TRUE)))));
    
    $content = parent::buildContent($entity, $view_mode, $langcode, $content);
    
    $content['dsm_duration']['#weight'] = 0;
    $ui_type = "PatternWeekUI" . $entity->typeOfPattern();
    $ui = new $ui_type($entity);
    $content['dateset_pattern'] = array(
      '#markup' => $ui->getPatternManager(),
      '#weight' => 1,
    );
    $content['dsm_length_of_pattern']['#weight'] = 2;
    
          
    return $content;
  }
  
  
  /**
   * Implements EntityAPIControllerInterface.
   *
   * Delete all child datesets of this pattern
   *
   * @param $transaction
   *   Optionally a DatabaseTransaction object to use. Allows overrides to pass
   *   in their transaction object.
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    $patterns = pattern_load_multiple($ids);
    $dsids = array();
    foreach($patterns as $pattern) {
      $datesets = $pattern->datesets_flat();
      foreach ($datesets as $dateset) {
        $dsids[] = $dateset->dsid;
      }
    }
    dateset_delete_multiple($dsids);
    parent::delete($ids, $transaction);
  }
  
}