<?php

/**
 * UI controller for Pattern Type.
 */
class PatternTypeUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage pattern types.';
    return $items;
  }
}
