<?php

/**
 * Pattern class.
 */
class Pattern extends Entity {

  public $builder;
  
  public function __construct(array $values = array(), $entityType = NULL) {
    parent::__construct($values, $entityType);
    $builderType = "PatternWeekBuilder" . $this->typeOfPattern();
    $this->builder = new $builderType($this);    
  }

  protected function defaultLabel() {
    return $this->title;
  }

  protected function defaultUri() {
    return array('path' => 'dsm/'.$this->uid.'/pattern/' . $this->identifier());
  }
  
  public function typeOfPattern() {
    $entity_type = pattern_type_load($this->type);
    return $entity_type->type_of_pattern;
  }
  
  /**
   * Get the datesets that correspond to this pattern in an associative
   * array keyed by year, then week, then day
   * @return array()
	 * 	[yid]
	 * 		[week_id (0-4) ]
	 * 			[day_id (0-6) ]
	 * 				[dateset entity]
	 * 				[dateset entity]
	 * 				[dateset node nid]
	 * 			[day_id (0-6) ]
	 * 		[week_id (0-4) ]
	 * 			[day_id (0-6) ]
	 * 				[dateset node nid]
	 * 				[dateset node nid]
	 * 			[day_id (0-6) ]
   */
  public function datesets() {
    
    $datesets = $this->datesets_flat();
    
    $datesets_by_pattern_indexes = array();
    
    foreach($datesets as $i => $ds) {
      //get positioning data from the dateset
      extract($ds->getPatternPositions());
      
      //initialize array keys
      if (!isset($datesets_by_pattern_indexes[$yid])) {
        $datesets_by_pattern_indexes[$yid] = array();
      }
      if (!isset($datesets_by_pattern_indexes[$yid][$wid])) {
        $datesets_by_pattern_indexes[$yid][$wid] = array();
      }
      if (!isset($datesets_by_pattern_indexes[$yid][$wid][$did])) {
        $datesets_by_pattern_indexes[$yid][$wid][$did] = array();
      }
      
      //set into associative array
      $datesets_by_pattern_indexes[$yid][$wid][$did][] = $ds;
    }
    
    return $datesets_by_pattern_indexes;
  }
  
  /**
   * rather than returning the elaborate array that datesets()
   * returns, just get a simpler flat list of datesets on this
   * pattern.
   */
  public function datesets_flat() {
    //TODO: use CER and a "datesets" entity refernce field?
    $query = new EntityFieldQuery();

    $query->entityCondition('entity_type', 'dateset')
      //->entityCondition('bundle', '') ? limit to bundle?
      ->fieldCondition('dsm_pid', 'target_id', $this->pid, '=');
      //->addMetaData('account', user_load(1)); // Run the query as user 1.

    $result = $query->execute();
    $dsids = array();
    if (count($result) == 0) {
      return array();
    }
    foreach ($result['dateset'] as $dsid) {
      $dsids[] = $dsid->dsid;
    }
    
    return dateset_load_multiple($dsids);
    
  }
  
  public function duration_start() {
    return $this->dsm_duration[LANGUAGE_NONE][0]['value'];
  }
  public function duration_end() {
    return $this->dsm_duration[LANGUAGE_NONE][0]['value2'];
  }
  
  public function length() {
    return $this->dsm_length_of_pattern[LANGUAGE_NONE][0]['value'];
  }
  
  /**
   * update child datesets in pattern on pattern update.
   * @todo: only do this when a change occurs that requires children to be updated..
   */
  public function save() {
    if (!isset($this->is_new)) {
      $datesets = $this->datesets();
      if (count($datesets)>0) {
        $datesets_flat = $this->datesets_flat();
        
        // check for shrinkage
        if (count($datesets[0]) > $this->length()) {
          $this->handleShrinkage($datesets, $datesets_flat);
        }
        
        // save datesets in the pattern for adjusted pattern
        foreach ($datesets_flat as $dateset) {
          $dateset->save();
        }
      }
    }
    parent::save();
  }
  
  /**
   * when the numweeks decreases, bump all of the datesets in 
   * lost weeks back to last week of new pattern
   */
  public function handleShrinkage($datesets, &$datesets_flat) {
    $bumped = array();
    
    for ($i = (count($datesets[0])-1); $i >= $this->length(); $i--) {
      foreach ($datesets[0][$i] as $day_of_datesets) {
        foreach ($day_of_datesets as $dateset) {
          $dateset->setWid(($this->length()-1));
          $dateset->save();
          $bumped[] = $dateset->dsid;
        }
      }
    }
    
    // alert to shifted weeks, remove dsids from mass save for efficiency
    if (count($bumped)>0) {
      drupal_set_message(t("Some dates were altered to work with the shorter repeating pattern."));
      foreach ($bumped as $dsid) {
        unset($datesets_flat[$dsid]);
      }
    }
  }
  
}