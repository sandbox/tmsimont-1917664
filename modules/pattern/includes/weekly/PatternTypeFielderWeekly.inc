<?php

class PatternTypeFielderWeekly extends PatternTypeFielder {
  public function attachFields($entity) {
    $bundle_name = $entity->type;
    //TODO: set up to give different fields to different types of patterns (monthly vs. weekly)
    $export = '{
  "fields" : {
    "dsm_duration" : {
      "translatable" : "0",
      "entity_types" : [],
      "settings" : {
        "granularity" : {
          "month" : "month",
          "day" : "day",
          "year" : "year",
          "hour" : 0,
          "minute" : 0,
          "second" : 0
        },
        "tz_handling" : "none",
        "timezone_db" : "",
        "cache_enabled" : 0,
        "cache_count" : "4",
        "repeat" : "0",
        "profile2_private" : false,
        "todate" : "required"
      },
      "storage" : {
        "type" : "field_sql_storage",
        "settings" : [],
        "module" : "field_sql_storage",
        "active" : "1",
        "details" : { "sql" : {
            "FIELD_LOAD_CURRENT" : { "field_data_dsm_duration" : { "value" : "dsm_duration_value", "value2" : "dsm_duration_value2" } },
            "FIELD_LOAD_REVISION" : { "field_revision_dsm_duration" : { "value" : "dsm_duration_value", "value2" : "dsm_duration_value2" } }
          }
        }
      },
      "foreign keys" : [],
      "indexes" : [],
      "field_name" : "dsm_duration",
      "type" : "datetime",
      "module" : "date",
      "active" : "1",
      "locked" : "0",
      "cardinality" : "1",
      "deleted" : "0",
      "columns" : {
        "value" : {
          "type" : "datetime",
          "mysql_type" : "datetime",
          "pgsql_type" : "timestamp without time zone",
          "sqlite_type" : "varchar",
          "sqlsrv_type" : "smalldatetime",
          "not null" : false,
          "sortable" : true,
          "views" : true
        },
        "value2" : {
          "type" : "datetime",
          "mysql_type" : "datetime",
          "pgsql_type" : "timestamp without time zone",
          "sqlite_type" : "varchar",
          "sqlsrv_type" : "smalldatetime",
          "not null" : false,
          "sortable" : true,
          "views" : false
        }
      },
      "bundles" : { "pattern" : [ "' . $bundle_name . '" ] }
    },
    "dsm_length_of_pattern" : {
      "translatable" : "0",
      "entity_types" : [],
      "settings" : {
        "allowed_values" : {
          "1" : "1-week pattern (weekly)",
          "2" : "2-week pattern (biweekly)",
          "3" : "3-week pattern",
          "4" : "4-week pattern"
        },
        "allowed_values_function" : "",
        "profile2_private" : false
      },
      "storage" : {
        "type" : "field_sql_storage",
        "settings" : [],
        "module" : "field_sql_storage",
        "active" : "1",
        "details" : { "sql" : {
            "FIELD_LOAD_CURRENT" : { "field_data_dsm_length_of_pattern" : { "value" : "dsm_length_of_pattern_value" } },
            "FIELD_LOAD_REVISION" : { "field_revision_dsm_length_of_pattern" : { "value" : "dsm_length_of_pattern_value" } }
          }
        }
      },
      "foreign keys" : [],
      "indexes" : { "value" : [ "value" ] },
      "field_name" : "dsm_length_of_pattern",
      "type" : "list_integer",
      "module" : "list",
      "active" : "1",
      "locked" : "0",
      "cardinality" : "1",
      "deleted" : "0",
      "columns" : { "value" : { "type" : "int", "not null" : false } },
      "bundles" : { "pattern" : [ "' . $bundle_name . '" ] }
    }
  },
  "field_instances" : {
    "dsm_duration" : {
      "label" : "Duration",
      "widget" : {
        "weight" : "0",
        "type" : "date_popup",
        "module" : "date",
        "active" : 1,
        "settings" : {
          "input_format" : "m\/d\/Y - H:i:s",
          "input_format_custom" : "",
          "year_range" : "-3:+3",
          "increment" : "15",
          "label_position" : "above",
          "text_parts" : [],
          "display_all_day" : 0,
          "repeat_collapsed" : 0
        }
      },
      "settings" : {
        "default_value" : "now",
        "default_value_code" : "",
        "default_value2" : "same",
        "default_value_code2" : "",
        "user_register_form" : false
      },
      "display" : { "default" : {
          "label" : "above",
          "type" : "date_default",
          "settings" : {
            "format_type" : "long",
            "multiple_number" : "",
            "multiple_from" : "",
            "multiple_to" : "",
            "fromto" : "both",
            "show_repeat_rule" : "show"
          },
          "module" : "date",
          "weight" : 0
        }
      },
      "required" : 1,
      "description" : "",
      "field_name" : "dsm_duration",
      "entity_type" : "pattern",
      "bundle" : "' . $bundle_name . '",
      "deleted" : "0"
    },
    "dsm_length_of_pattern" : {
      "label" : "Length of pattern",
      "widget" : {
        "weight" : "1",
        "type" : "options_select",
        "module" : "options",
        "active" : 1,
        "settings" : []
      },
      "settings" : { "user_register_form" : false },
      "display" : { "default" : {
          "label" : "above",
          "type" : "list_default",
          "settings" : [],
          "module" : "list",
          "weight" : 1
        }
      },
      "required" : 1,
      "description" : "",
      "default_value" : null,
      "field_name" : "dsm_length_of_pattern",
      "entity_type" : "pattern",
      "bundle" : "' . $bundle_name . '",
      "deleted" : "0"
    }
  }
}';
    $this->importFields($export);
  }
}