<?php

class PatternWeekUIWeekly extends PatternWeekUI {
  function prepare_dropzone_weeksdata() {
    //get datesets from pattern
    $datesets = $this->pattern->datesets();
    
    //get weeks from pattern
    $weeks = $this->pattern->builder->weeks();
    
    //remove yid-layer fromw weeks and datesets(year is non-existent in weekly patterns)
    $this->weeks = $weeks[0];
    $this->datesets = isset($datesets[0])?$datesets[0]:array();
    
  }
  function get_week_of_dropzones_data($wid) {
    $pid = $this->pattern->pid;
    $datesets = isset($this->datesets[$wid])?$this->datesets[$wid]:array();
    $dates = $this->weeks[$wid];
    $dropzones_data = array();
    for($did=0; $did<7; $did++){
      $dropzones_data[] = array(
        'id' => $pid.'-'.$wid.'-'.$did,
        'pid' => $pid,
        'wid' => $wid,
        'did' => $did,
        'datesets' => isset($datesets[$did])?$datesets[$did]:array(),
        'dates' => $dates[$did]['dates'],
        'rrule' => $dates[$did]['data']['rrule'],
        'container_type' => 'week'
      );
    }
    return $dropzones_data;
  }
  
  public function getPatternManager() {
    $args = arg();
    //first 2 args should be "pattern" and "pid"
    array_splice($args, 0, 2);
    $view = views_get_view("dsm_calendar");
    if (!$view || !$view->access("block_4")) {
      return;
    }
    $view->override_path = "pattern/" . $this->pattern->pid;
    $this->prepare_dropzone_weeksdata();
    return $view->preview("block_4", array($this->pattern->builder->startDate->format("Y-m"), $this->pattern->pid));
  }
  
  
  // @todo: remove -- this isn't really necessary since views integration
  public function getPatternManagerManual() {
    $themed_weeks = array();
    $this->prepare_dropzone_weeksdata();
    foreach($this->weeks as $wid=>$week) {
      $themed_weeks[] = theme('pattern_week_of_dropzones', array('dropzones_data' => $this->get_week_of_dropzones_data($wid)));
    }
    return theme('pattern_weekly_pattern_container',array('weeks'=>$themed_weeks));
  }
}