<?php

class PatternWeekBuilderWeekly extends WeekBuilder {
  
  /**
   * Get a "weeks array" -- an array of days in a week.
   * Each "day" in the weeks is an array with 'dates' and 'rrule' values.
   * 'dates' is an array. It's a list of all the actual 1-time date occurrances
   * for the given day of the week.  Each date is spaced apart by the number of
   * days in a week * the number of weeks in the pattern.
   * For example, in a 1-week pattern each 'date' is 7 days apart.  
   * In a 2-week pattern, each 'date' is 14 days appart.  
   * The explicit list of dates is described by the 'rrule' in the "day" array.
   * 
   * @param $data['m_start']
   *  the master start date for the recurring period
   *  mm-dd-yyyy
   * @param $data['m_finish']
   *  the master finish date for the recurring period
   *  mm-dd-yyyy
   * @param $data['num_weeks']
   *  the number of weeks in the period (default =1)
   * @param $data['timezone']
   *  the field input timezone of the date field and the time of day field
   * @param $data['timezone_db']
   *  the database storage timezone
   *  
   *  @return
   *   The array described above:
   *   array(
   *    [yid (year id is always 0 in repeating dates)] => array(
   *     [week number (0-3)] => array(
   *       [day number (0-6)] => array(
   *         [dates] => array(timestamp,timestamp,timestamp...)
   *         [data] => array( [rrule] => RRULE:FREQ=DAILY;INTERVAL=[7/14/21/28];UNTIL[mdate];WKST=SU )
   *       ),
   *       [day number (0-6)] => array(
   *         [dates] => array(timestamp,timestamp,timestamp...)
   *         [data] => array( [rrule] => RRULE:FREQ=DAILY;INTERVAL=[7/14/21/28];UNTIL[mdate];WKST=SU )
   *       )
   *     )
   *    )
   *   )
   */
  public function _getWeeks($data) {
    $this->m_start = $data['m_start'];
    $this->m_finish = $data['m_finish'];
    $this->num_weeks = $data['num_weeks'];
    $this->timezone = $data['timezone'];
    $this->timezone_db = $data['timezone_db'];
    
    // create date objects for the start and end dates of the pattern
    $start = explode("-",$this->m_start);
    $end = explode("-",$this->m_finish);
    $this->startDate = new DateObject(array('year'=>$start[2],'month'=>$start[0], 'day'=>$start[1]));  
    $this->endDate = new DateObject(array('year'=>$end[2],'month'=>$end[0], 'day'=>$end[1]));
    
    // set an interval between repeating days in the pattern
    $this->interval = $this->get_interval();
    
    // get numeric day of week (0-6) for the initial day in the repeat pattern date range
    $this->initDay = intval($this->startDate->format('w'));

    
    
    $this->process_pattern();
    
    // return week-indexed days wrapped in array for "yid" support (year id is 0 in repeat patterns)
    return array(
      0=>$this->get_pattern_days_by_week_indexes()
    );
  }
  
  /**
   * Because this is a weekly pattern, return the number of weeks in the pattern
   * multiplied by 7 days in one week.
   */
  function get_interval() {
    return $this->num_weeks * 7;
  }
  
  /**
   * cycle through every unique day of the pattern and build dates into
   * each day of the pattern
   */
  function process_pattern() {
    // initialize an array of days in this pattern
    $this->pattern_days = array();
    
    for($i = 0; $i<$this->interval; $i++){
      $this->pattern_days[$i] = array();
      
      // determine how many days this day of the pattern is after the start date day of week
      $offset = $i-$this->initDay;
       
      // bump the offset value to the next repetition if the day of week in this pattern preceeds the start date
      if($i<$this->initDay) {
        $offset += $this->interval;
      }
      $this->pattern_days[$i]['offset'] = $offset;
      
      // get a relative day DateObject for this day in the repeat pattern based on start date and "offset" adjuster
      $arr = $this->startDate->toArray();
      $dayRel = new DateObject(array('year'=>$arr['year'],'month'=>$arr['month'], 'day'=>$arr['day']));
      $dayRel->add(new DateInterval("P".$offset."D"));
      
      // set up an RRULE to pass to the date_repeat_calc function:
      $rrule = "RRULE:FREQ=DAILY;INTERVAL=".$this->interval.";UNTIL=".$this->endDate->format("Ymd")."T".$this->endDate->format("Hi")."00Z;WKST=SU";
      
      $this->pattern_days[$i]['dates'] = $this->get_dates($rrule, $dayRel, $this->endDate, $this->exceptions());
      
      // store the rrule
      $this->pattern_days[$i]['data'] = array(
        'rrule' => $rrule,
      );
    }
  }
  
  /**
   * use the date module to get an array of date objects for a day in the pattern
   */
  function get_dates($rrule, $start, $end, $exceptions) {
    $dates = array();
    
    // use the date module to figure what the stamps are for all of the repeats relative to this day in the pattern
    $calcRule = date_repeat_calc($rrule, $start->format('Y-m-d'), $end->format('Y-m-d'), $exceptions, $this->timezone_db);

    // take this calcRule array and push the values into the array to be returned       
    foreach($calcRule as $key => $datetime){
      // set as 00:00 in the input timezone
      $day = new DateObject($datetime, $this->timezone);
      // even though date_repeat_calc gets exceptions, we might still have them returned, @see: http://drupal.org/node/1842708
      if (!in_array($day->format("Y-m-d"), $exceptions)) {
        // convert and store as timezone_db timezone
        $day->setTimezone(new DateTimeZone($this->timezone_db));
        $dates[] = $day;
      }
    }
    
    return $dates;
  }
  
  /**
   * In a 2 week pattern, pattern days are build with a 0-13 index,
   * convert that to a [week index][day of week index] array
   */
  function get_pattern_days_by_week_indexes() {
    // days may have days up to 7*4 -- return it in a 7-day week format:
    $week_based_days = array();
    
    foreach($this->pattern_days as $day_idx=> $values){		
      $translate = $this->_days_to_week($day_idx);
      $week_based_days[$translate['week_number']][$translate['day_number']] = $values;
    }
    
    return $week_based_days;
  }
    

  /**
   * recursive helper function that will aide in taking
   * part of an array of day-related values ( indexed 0 to (7*num_weeks))
   * and return its corresponding "week number" and "day number" in a [week number][0-6] format
   * 
   * @param $day_idx
   *  the day number as it exists in a 0 to 7*4 limit
   * @param $week
   *  for recursion -- the week key to use ( increments each time the $day_idx is greater than 6 )
   * 
   * @return
   *  returns an array
   *  'week_number' for the numeric week representation
   *  'day_number for the relative day representation
   *  
   */
  private function _days_to_week($day_idx, $week=0){
    if($day_idx>6){
      $day_idx -= 7;  
      $week++;
      return $this->_days_to_week($day_idx, $week);
    }else
      return array(
        'week_number' => $week,
        'day_number' => $day_idx
      );
  }
  
  public function getWeekBuilderData() {
    //dsm_dates is the "time of day" field -- the tz handling needs to match that field
    $field = field_info_field("dsm_dates");
    $duration = $this->pattern->dsm_duration[LANGUAGE_NONE][0];
    
    $timezone_db = date_get_timezone_db($field['settings']['tz_handling']);
    $timezone = isset($duration['timezone']) ? $duration['timezone'] : '';
    $timezone = date_get_timezone($field['settings']['tz_handling'], $timezone);
    $m_start = date("m-d-Y",strtotime($duration['value']));
    $m_finish = date("m-d-Y",strtotime($duration['value2']));
    
    $num_weeks = $this->pattern->dsm_length_of_pattern[LANGUAGE_NONE][0]['value'];
    
    return array('m_start'=>$m_start, 'm_finish'=>$m_finish, 'num_weeks'=>$num_weeks, 'timezone_db'=>$timezone_db, 'timezone'=>$timezone);
  }
    
}