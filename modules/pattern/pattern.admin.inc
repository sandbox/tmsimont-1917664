<?php

/**
 * Generates the pattern type editing form.
 */
function pattern_type_form($form, &$form_state, $pattern_type, $op = 'edit') {

  if ($op == 'clone') {
    $pattern_type->label .= ' (cloned)';
    $pattern_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $pattern_type->label,
    '#description' => t('The human-readable name of this pattern type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($pattern_type->type) ? $pattern_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $pattern_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'pattern_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this pattern type. It must only contain lowercase letters, numbers, and underscores.'),
  );
  
  $form['type_of_pattern'] = array(
    '#title' => t("Type of pattern"),
    '#type' => 'select',
    '#options' => array(
      'Weekly' => t("Repeating weekly dates"),
      'Monthly' => t("Repeating monthly dates"),
    ),
    '#default_value' => isset($pattern_type->type_of_pattern) ? $pattern_type->type_of_pattern : '',
    '#description' => t("This cannot be changed after the pattern type is saved.")
  );
  
  if (!$pattern_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['type_of_pattern']['#disabled'] = true;
  }

  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($pattern_type->description) ? $pattern_type->description : '',
    '#description' => t('Description about the pattern type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save pattern type'),
    '#weight' => 40,
  );

  if (!$pattern_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete pattern type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('pattern_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Submit handler for creating/editing pattern_type.
 */
function pattern_type_form_submit(&$form, &$form_state) {
  $pattern_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  pattern_type_save($pattern_type);

  // Redirect user back to list of pattern types.
  $form_state['redirect'] = 'admin/structure/dsm/pattern-types';
}

function pattern_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/dsm/pattern-types/' . $form_state['pattern_type']->type . '/delete';
}

/**
 * Pattern type delete form.
 */
function pattern_type_form_delete_confirm($form, &$form_state, $pattern_type) {
  $form_state['pattern_type'] = $pattern_type;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['pattern_type_id'] = array('#type' => 'value', '#value' => entity_id('pattern_type' ,$pattern_type));
  return confirm_form($form,
    t('Are you sure you want to delete pattern type %title?', array('%title' => entity_label('pattern_type', $pattern_type))),
    'pattern' . entity_id('pattern_type' ,$pattern_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Pattern type delete form submit handler.
 */
function pattern_type_form_delete_confirm_submit($form, &$form_state) {
  $pattern_type = $form_state['pattern_type'];
  pattern_type_delete($pattern_type);

  watchdog('pattern_type', '@type: deleted %title.', array('@type' => $pattern_type->type, '%title' => $pattern_type->label));
  drupal_set_message(t('@type %title has been deleted.', array('@type' => $pattern_type->type, '%title' => $pattern_type->label)));

  $form_state['redirect'] = 'admin/structure/dsm/pattern-types';
}

/**
 * Page to select pattern Type to add new pattern.
 */
function pattern_add_page($user) {
  $items = array();
  foreach (pattern_types() as $pattern_type_key => $pattern_type) {
    $items[] = l(entity_label('pattern_type', $pattern_type), 'dsm/' . $user->uid . '/pattern/add/' . $pattern_type_key);
  }
  return array('list' => array('#theme' => 'item_list', '#items' => $items, '#title' => t('Select type of pattern to create.')));
}

/**
 * Add new pattern page callback.
 */
function pattern_add($type) {
  $pattern_type = pattern_types($type);

  $pattern = entity_create('pattern', array('type' => $type));
  drupal_set_title(t('Create @name pattern', array('@name' => entity_label('pattern_type', $pattern_type))));

  $output = drupal_get_form('pattern_form', $pattern);

  return $output;
}

/**
 * Pattern Form.
 */
function pattern_form($form, &$form_state, $pattern) {
  $form_state['pattern'] = $pattern;

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name'),
    '#default_value' => $pattern->title,
  );
  

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $pattern->uid,
  );
  
  $account = user_load($pattern->uid);
  if (!_dsm_show_pattern_tab($account)) {
    drupal_set_message("You can't have a repeating date pattern without access to at least 1 schedule.", "error");
    drupal_goto("dsm/$uid");
  }

  field_attach_form('pattern', $pattern, $form, $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => $submit + array('pattern_form_submit'),
  );

  // Show Delete button if we edit pattern.
  $pattern_id = entity_id('pattern' ,$pattern);
  if (!empty($pattern_id) && pattern_access('edit', $pattern)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('pattern_form_submit_delete'),
    );
  }

  $form['#validate'][] = 'pattern_form_validate';

  return $form;
}

function pattern_form_validate($form, &$form_state) {
  $start = new DateObject($form_state['values']['dsm_duration'][LANGUAGE_NONE][0]['value']);
  $end = new DateObject($form_state['values']['dsm_duration'][LANGUAGE_NONE][0]['value2']);
  $duration_length = $start->difference($end, "weeks");
  
  $pattern_length = $form_state['values']['dsm_length_of_pattern'][LANGUAGE_NONE][0]['value'];
  
  if ($pattern_length > $duration_length) {
    //if the duration is shorter than the pattern, there'd be "dead" days on the calendar dropzones
    form_set_error("dsm_duration", "The duration is not long enough for the set pattern length.");
    return false;
  }
  
  //TODO: maybe run through datesets, see if any changes will cause problems?
  
  
}

/**
 * Pattern submit handler.
 */
function pattern_form_submit($form, &$form_state) {
  $pattern = $form_state['pattern'];

  entity_form_submit_build_entity('pattern', $pattern, $form, $form_state);

  pattern_save($pattern);

  $pattern_uri = entity_uri('pattern', $pattern);

  $form_state['redirect'] = $pattern_uri['path'];

  drupal_set_message(t('Pattern %title saved.', array('%title' => entity_label('pattern', $pattern))));
}

function pattern_form_submit_delete($form, &$form_state) {
  $pattern = $form_state['pattern'];
  $pattern_uri = entity_uri('pattern', $pattern);
  $form_state['redirect'] = $pattern_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function pattern_delete_form($form, &$form_state, $pattern) {
  $form_state['pattern'] = $pattern;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['pattern_type_id'] = array('#type' => 'value', '#value' => entity_id('pattern' ,$pattern));
  $pattern_uri = entity_uri('pattern', $pattern);
  return confirm_form($form,
    t('Are you sure you want to delete pattern %title?', array('%title' => entity_label('pattern', $pattern))),
    $pattern_uri['path'],
    t('This will also delete all dates placed onto this pattern.  This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function pattern_delete_form_submit($form, &$form_state) {
  $pattern = $form_state['pattern'];
  $uid = $pattern->uid;
  pattern_delete($pattern);

  drupal_set_message(t('Pattern %title deleted.', array('%title' => entity_label('pattern', $pattern))));

  $form_state['redirect'] = 'dsm/' . $uid . '/pattern';
}
