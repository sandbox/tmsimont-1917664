<?php

/**
 * Pattern view callback.
 */
function pattern_view($pattern) {
  //drupal_set_title(entity_label('pattern', $pattern));
  return entity_view('pattern', array(entity_id('pattern', $pattern) => $pattern), 'full');
}

