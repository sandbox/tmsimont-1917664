<?php 

/**
 * This theme function simply arranges 7 dropzone templates
 * for the days sunday through saturday
 */
function theme_pattern_week_of_dropzones($variables){
  $output = "";
  foreach($variables['dropzones_data'] as $data) {
    $output .= theme('dateset_dropzone', $data);
	}
	return $output;
}
