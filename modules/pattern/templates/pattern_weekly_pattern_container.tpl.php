<?php 
/**
 * The week container contains up to 4 themed "dropzone" weeks.
 */
?>
<table id="weekly-patterns-table">
<?php 
foreach($weeks as $idx => $week):
?>
	<tr id="date-dropzone-week-row-<?php echo $idx;?>" class='dateset-dropzone-week-row'>
    <?php print $week; ?>		
	</tr>
<?php 
endforeach; 
?>
</table>