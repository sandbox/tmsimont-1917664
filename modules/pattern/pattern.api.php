<?php
/**
 * @file
 * Hooks provided by this module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Acts on pattern being loaded from the database.
 *
 * This hook is invoked during $pattern loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array $entities
 *   An array of $pattern entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_pattern_load(array $entities) {
  $result = db_query('SELECT pid, foo FROM {pattern} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
 * Responds when a $pattern is inserted.
 *
 * This hook is invoked after the $pattern is inserted into the database.
 *
 * @param pattern $pattern
 *   The $pattern that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_pattern_insert(pattern $pattern) {
  db_insert('pattern')
    ->fields(array(
      'id' => entity_id('pattern', $pattern),
      'extra' => print_r($pattern, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a $pattern being inserted or updated.
 *
 * This hook is invoked before the $pattern is saved to the database.
 *
 * @param pattern $pattern
 *   The $pattern that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_pattern_presave(pattern $pattern) {
  $pattern->name = 'foo';
}

/**
 * Responds to a $pattern being updated.
 *
 * This hook is invoked after the $pattern has been updated in the database.
 *
 * @param pattern $pattern
 *   The $pattern that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_pattern_update(pattern $pattern) {
  db_update('pattern')
    ->fields(array('extra' => print_r($pattern, TRUE)))
    ->condition('id', entity_id('pattern', $pattern))
    ->execute();
}

/**
 * Responds to $pattern deletion.
 *
 * This hook is invoked after the $pattern has been removed from the database.
 *
 * @param pattern $pattern
 *   The $pattern that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_pattern_delete(pattern $pattern) {
  db_delete('pattern')
    ->condition('pid', entity_id('pattern', $pattern))
    ->execute();
}

/**
 * Act on a pattern that is being assembled before rendering.
 *
 * @param $pattern
 *   The pattern entity.
 * @param $view_mode
 *   The view mode the pattern is rendered in.
 * @param $langcode
 *   The language code used for rendering.
 *
 * The module may add elements to $pattern->content prior to rendering. The
 * structure of $pattern->content is a renderable array as expected by
 * drupal_render().
 *
 * @see hook_entity_prepare_view()
 * @see hook_entity_view()
 */
function hook_pattern_view($pattern, $view_mode, $langcode) {
  $pattern->content['my_additional_field'] = array(
    '#markup' => $additional_field,
    '#weight' => 10,
    '#theme' => 'mymodule_my_additional_field',
  );
}

/**
 * Alter the results of entity_view() for patterns.
 *
 * @param $build
 *   A renderable array representing the pattern content.
 *
 * This hook is called after the content has been assembled in a structured
 * array and may be used for doing processing which requires that the complete
 * pattern content structure has been built.
 *
 * If the module wishes to act on the rendered HTML of the pattern rather than
 * the structured content array, it may use this hook to add a #post_render
 * callback. Alternatively, it could also implement hook_preprocess_pattern().
 * See drupal_render() and theme() documentation respectively for details.
 *
 * @see hook_entity_view_alter()
 */
function hook_pattern_view_alter($build) {
  if ($build['#view_mode'] == 'full' && isset($build['an_additional_field'])) {
    // Change its weight.
    $build['an_additional_field']['#weight'] = -10;

    // Add a #post_render callback to act on the rendered HTML of the entity.
    $build['#post_render'][] = 'my_module_post_render';
  }
}

/**
 * Acts on pattern_type being loaded from the database.
 *
 * This hook is invoked during pattern_type loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array $entities
 *   An array of pattern_type entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_pattern_type_load(array $entities) {
  $result = db_query('SELECT pid, foo FROM {pattern} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
 * Responds when a pattern_type is inserted.
 *
 * This hook is invoked after the pattern_type is inserted into the database.
 *
 * @param patternType $pattern_type
 *   The pattern_type that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_pattern_type_insert(patternType $pattern_type) {
  db_insert('pattern')
    ->fields(array(
      'id' => entity_id('pattern_type', $pattern_type),
      'extra' => print_r($pattern_type, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a pattern_type being inserted or updated.
 *
 * This hook is invoked before the pattern_type is saved to the database.
 *
 * @param patternType $pattern_type
 *   The pattern_type that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_pattern_type_presave(patternType $pattern_type) {
  $pattern_type->name = 'foo';
}

/**
 * Responds to a pattern_type being updated.
 *
 * This hook is invoked after the pattern_type has been updated in the database.
 *
 * @param patternType $pattern_type
 *   The pattern_type that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_pattern_type_update(patternType $pattern_type) {
  db_update('pattern')
    ->fields(array('extra' => print_r($pattern_type, TRUE)))
    ->condition('id', entity_id('pattern_type', $pattern_type))
    ->execute();
}

/**
 * Responds to pattern_type deletion.
 *
 * This hook is invoked after the pattern_type has been removed from the database.
 *
 * @param patternType $pattern_type
 *   The pattern_type that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_pattern_type_delete(patternType $pattern_type) {
  db_delete('pattern')
    ->condition('pid', entity_id('pattern_type', $pattern_type))
    ->execute();
}

/**
 * Define default pattern_type configurations.
 *
 * @return
 *   An array of default pattern_type, keyed by machine names.
 *
 * @see hook_default_pattern_type_alter()
 */
function hook_default_pattern_type() {
  $defaults['main'] = entity_create('pattern_type', array(
    // …
  ));
  return $defaults;
}

/**
 * Alter default pattern_type configurations.
 *
 * @param array $defaults
 *   An array of default pattern_type, keyed by machine names.
 *
 * @see hook_default_pattern_type()
 */
function hook_default_pattern_type_alter(array &$defaults) {
  $defaults['main']->name = 'custom name';
}
