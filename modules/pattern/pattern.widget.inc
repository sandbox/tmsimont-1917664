<?php

/**
 * This concept was abandoned
 
 
=======================================================================================
Fix Entity Reference "Dateset Pattern" widget confusion
=======================================================================================
Currently the pattern.module defines a widget type that is used on an entity reference 
field. That widget requires that certain fields exist in a dateset entity.  
Specifically:
1) Year of Pattern: dsm_yid
2) Week of Pattern: dsm_wid
3) Day of Pattern: dsm_did

The widget should only be available when the entityreference field is referncing an 
entity that has these fields. For now, there are no checks in place.
Furthermore, the "dateset pattern" widgets really don't make sense for other 
entityreference fields. For now, hook_form_alter in pattern.module tries to prevent 
the pattern widgets from appearing in non-pattern entity types.

To fix this, here are some ideas:
1) Explore making the field "private" to the pattern entity by defining it in 
pattern.module 
-- downside to this would be loss of any entityrefernce field magic with views and 
other modules

2) Find a way to validated the fields on the bundles the entityreference field is 
referencing

3) Find a better (less hackish) way of removing the "dateset pattern" 
entityreference widgets from other fields.
 */

/*******************************************************************************
 ************************ Date set Reference Widgets ***************************
 ******************************************************************************/


/**
 * Implements hook_field_widget_info().
 */
function pattern_field_widget_info() {
  $widgets['pattern_datesets_weekly'] = array(
    'label' => t('Weekly date set pattern'),
    'description' => t('Build a week-based pattern.'),
    'field types' => array('entityreference'),
    'settings' => array(
      // 'match_operator' => 'CONTAINS',
      // 'size' => 60,
      // // We don't have a default here, because it's not the same between
      // // the two widgets, and the Field API doesn't update default
      // // settings when the widget changes.
      // 'path' => '',
    ),
  );
  
  return $widgets;
  
}

/**
 * Implements hook_field_widget_settings_form().
 */
function pattern_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'] + field_info_widget_settings($widget['type']);

  $form = array();

  if ($widget['type'] == 'pattern_datesets_weekly') {
    $form['size'] = array(
      '#type' => 'textfield',
      '#title' => t('Size of textfield (test)'),
      '#element_validate' => array('_element_validate_integer_positive'),
      '#required' => TRUE,
    );
  }

  return $form;
}

/**
 * Implements hook_field_widget_form().
 */
function pattern_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $entity_type = $instance['entity_type'];
  $entity = isset($element['#entity']) ? $element['#entity'] : NULL;
  
  $handler = entityreference_get_selection_handler($field, $instance, $entity_type, $entity);

  if ($instance['widget']['type'] == 'pattern_datesets_weekly') {


    $entity_ids = array();
    $entity_labels = array();

    // Build an array of entities ID.
    foreach ($items as $item) {
      $entity_ids[] = $item['target_id'];
    }

    // Load those entities and loop through them to extract their labels.
    $entities = entity_load($field['settings']['target_type'], $entity_ids);

    foreach ($entities as $entity_id => $entity_item) {
      $label = $handler->getLabel($entity_item);
      $key = "$label ($entity_id)";
      // Labels containing commas or quotes must be wrapped in quotes.
      if (strpos($key, ',') !== FALSE || strpos($key, '"') !== FALSE) {
        $key = '"' . str_replace('"', '""', $key) . '"';
      }
      $entity_labels[] = $key;
    }
    
    $element['test'] = array(
      '#type' => 'textfield',
      '#title' => 'widget form textfield',
    );
  
  }
  
  
  return $element;
}

/**
 * Implements hook_field_widget_error().
 */
function pattern_field_widget_error($element, $error) {
  form_error($element, $error['message']);
}


/**
 * Implements hook_form_alter()
 */
function pattern_form_alter(&$form, &$form_state, $form_id) {

  /**
   * Try to supress the pattern entityreference field widget on non-pattern entities
   * @todo: a more elegant way of preventing problems with this widget appearing on unexpected entities
   */
  if ($form_id=='field_ui_widget_type_form' || $form_id=='field_ui_field_overview_form') {
    if ($form_id=='field_ui_field_overview_form') {
      if ($form['#entity_type'] != 'pattern') {
        unset($form['fields']['_add_new_field']['widget_type']['#options']['Entity Reference']['pattern_datesets_weekly']);
        foreach($form['#attached']['js'] as $i => $js) {
          if (isset($form['#attached']['js'][$i]['data']) && isset($form['#attached']['js'][1]['data']['fieldWidgetTypes']) && isset($form['#attached']['js'][1]['data']['fieldWidgetTypes']['entityreference'])) {
            if (is_array($form['#attached']['js'][$i]['data'])) {
              unset($form['#attached']['js'][$i]['data']['fieldWidgetTypes']['entityreference']['pattern_datesets_weekly']);
            }
          }
        }
      }
    } elseif($form_id == 'field_ui_widget_type_form') {
      if ($form['#entity_type'] != 'pattern') {
        $field = field_info_field($form['#field_name']);
        if ($field['type'] == 'entityreference') {
          unset($form['basic']['widget_type']['#options']['pattern_datesets_weekly']);
        }
      }
    }
  }
}