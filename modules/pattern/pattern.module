<?php
/**
 * @see http://www.trellon.com/content/blog/creating-own-entities-entity-api
 * this module creates 'pattern' and 'schedule' entities.  
 * A schedule is a collection of patterns.  A pattern is a collection of dates.
 */


/**
 * Implements hook_entity_info().
 */
function pattern_entity_info() {
  $return = array(
    'pattern' => array(
      'label' => t('Pattern'),
      'entity class' => 'Pattern',
      'controller class' => 'PatternController',
      'base table' => 'pattern',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'pid',
        'bundle' => 'type',
      ),
      'bundle keys' => array(
        'bundle' => 'type',
      ),
      'bundles' => array(),
      'load hook' => 'pattern_load',
      'view modes' => array(
        'full' => array(
          'label' => t('Default'),
          'custom settings' => FALSE,
        ),
        'schedule' => array(
          'label' => t('Schedule Manager'),
          'custom settings' => FALSE,
        ),
        'calendar' => array(
          'label' => t('Public Calendar'),
          'custom settings' => FALSE,
        ),
      ),
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'module' => 'pattern',
      'access callback' => 'pattern_access',
    ),
  );
  $return['pattern_type'] = array(
    'label' => t('Pattern type'),
    'entity class' => 'PatternType',
    'controller class' => 'PatternTypeController',
    'base table' => 'pattern_type',
    'fieldable' => FALSE,
    'bundle of' => 'pattern',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'name' => 'type',
      'label' => 'label',
    ),
    'module' => 'pattern',
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => 'admin/structure/dsm/pattern-types',
      'file' => 'pattern.admin.inc',
      'controller class' => 'PatternTypeUIController',
    ),
    'access callback' => 'pattern_type_access',
  );

  return $return;
}

/**
 * Implements hook_entity_info_alter().
 */
function pattern_entity_info_alter(&$entity_info) {
  foreach (pattern_types() as $type => $info) {
    $entity_info['pattern']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'path' => 'admin/structure/dsm/pattern-types/manage/%pattern_type',
        'real path' => 'admin/structure/dsm/pattern-types/manage/' . $type,
        'bundle argument' => 5,
      ),
    );
  }
}

/**
 * Implements hook_menu().
 */
function pattern_menu() {
  $items = array();

  $items['dsm/%user/pattern/add'] = array(
    'title' => 'Add a repeating date pattern',
    'page arguments' => array(1),
    'weight' => 1,
    'page callback' => 'pattern_add_page',
    'access arguments' => array('create pattern entities'),
    'file' => 'pattern.admin.inc',
    'type' => MENU_LOCAL_ACTION,
  );

  $pattern_uri = 'dsm/%user/pattern/%pattern';
  $pattern_uri_argument_position = 3;

  $items[$pattern_uri] = array(
    'title callback' => 'pattern_title',
    'title arguments' => array($pattern_uri_argument_position, 'view'),
    'page callback' => 'pattern_view',
    'page arguments' => array($pattern_uri_argument_position),
    'access callback' => 'entity_access',
    'menu_name' => 'navigation',
    'access arguments' => array('view', 'pattern', $pattern_uri_argument_position),
    'file' => 'pattern.pages.inc',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items[$pattern_uri . '/delete'] = array(
    'title callback' => 'pattern_title',
    'title arguments' => array($pattern_uri_argument_position, 'delete'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pattern_delete_form', $pattern_uri_argument_position),
    'access callback' => 'entity_access',
    'access arguments' => array('edit', 'pattern', $pattern_uri_argument_position),
    'file' => 'pattern.admin.inc',
    'menu_name' => 'navigation',
    'type' => MENU_CALLBACK,
  );

  $items[$pattern_uri . '/edit'] = array(
    'title callback' => 'pattern_title',
    'title arguments' => array($pattern_uri_argument_position, 'edit'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pattern_form', $pattern_uri_argument_position),
    'access callback' => 'entity_access',
    'access arguments' => array('edit', 'pattern', $pattern_uri_argument_position),
    'file' => 'pattern.admin.inc',
    'menu_name' => 'navigation',
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  foreach (pattern_types() as $type => $info) {
    $items['dsm/%user/pattern/add/' . $type] = array(
      'title' => 'Add pattern',
      'page callback' => 'pattern_add',
      'page arguments' => array(4),
      'access callback' => 'entity_access',
      'access arguments' => array('create', 'pattern', $type),
      'file' => 'pattern.admin.inc',
    );
  }

  $items['admin/structure/dsm/pattern-types/%pattern_type/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pattern_type_form_delete_confirm', 4),
    'access arguments' => array('administer pattern types'),
    'weight' => 1,
    'type' => MENU_NORMAL_ITEM,
    'file' => 'pattern.admin.inc',
  );
  
  return $items;
}

/**
 * Implements hook_menu_local_tasks_alter()
 */
function pattern_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  $path_parts = arg();
  if (  arg(0)=="dsm" 
      && is_numeric(arg(1)) 
      && arg(2)=='pattern' 
      && arg(3) != "add" 
      && arg(4) != "delete"  
      && (!(arg(4) == "exclusion" && count(arg())>5))
      && arg(5) != "add" 
      && arg(5) != "delete" 
      && arg(4)!="dateset" 
      && arg(7)!="dateset"
  ) {
    $user = user_load(arg(1));
    $pid = arg(3);
    if (isset($pid) && is_numeric($pid)) {
      unset($data['actions']['output'][0]);
      $view = menu_get_item("dsm/" .$user->uid. "/pattern/$pid");
      $edit = menu_get_item("dsm/" . $user->uid . "/pattern/$pid/edit");
      
      
      $data['tabs'][1]['output'] = array(
        array(
          '#theme' => 'menu_local_task',
          '#link' => $view,
        ),
        array(
          '#theme' => 'menu_local_task',
          '#link' => $edit,
        )
      );
      
      if (module_exists("exclusion")) {
        $exc = menu_get_item("dsm/" . $user->uid . "/pattern/$pid/exclusion");
        $data['tabs'][1]['output'][] = array(
          '#theme' => 'menu_local_task',
          '#link' => $exc,
        );
      }
      
      $data['tabs'][1]['count'] = count($data['tabs'][1]['output']);
      
    }
  }
}

/**
 * Page title callback for schedule ops
 */
function pattern_title($schedule, $op = 'view') {
  $label = entity_label('pattern', $schedule);
  switch ($op) {
    case 'view' :
      $title = t("Manage dates");
      // drupal_set_title("Schedule Manager: ".$label);
      break;
    case 'edit' :
      $title = t("Edit pattern");
      drupal_set_title($label);
      break;
    case 'delete' :
      $title = t("Delete") . $label;
      break;
  }
  return $title;
}

/**
 * Implements hook_permission().
 */
function pattern_permission() {
  $permissions = array(
    'administer pattern types' => array(
      'title' => t('Administer pattern types'),
      'description' => t('Allows users to configure pattern types and their fields.'),
      'restrict access' => TRUE,
    ),
    'create pattern entities' => array(
      'title' => t('Create pattern'),
      'description' => t('Allows users to create pattern.'),
      'restrict access' => TRUE,
    ),
    'view pattern entities' => array(
      'title' => t('View pattern'),
      'description' => t('Allows users to view pattern.'),
      'restrict access' => TRUE,
    ),
    'edit any pattern entities' => array(
      'title' => t('Edit any pattern'),
      'description' => t('Allows users to edit any pattern.'),
      'restrict access' => TRUE,
    ),
    'edit own pattern entities' => array(
      'title' => t('Edit own pattern'),
      'description' => t('Allows users to edit own pattern.'),
      'restrict access' => TRUE,
    ),
  );

  return $permissions;
}


/**
 * Implements hook_entity_property_info_alter().
 */
function pattern_entity_property_info_alter(&$info) {
  $properties = &$info['pattern']['properties'];
  $properties['created'] = array(
    'label' => t("Date created"),
    'type' => 'date',
    'description' => t("The date the pattern was posted."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer nodes',
    'schema field' => 'created',
  );
  $properties['changed'] = array(
    'label' => t("Date changed"),
    'type' => 'date',
    'schema field' => 'changed',
    'description' => t("The date the pattern was most recently updated."),
  );
  $properties['uid'] = array(
    'label' => t("Author"),
    'type' => 'user',
    'description' => t("The author of the pattern."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer pattern entities',
    'required' => TRUE,
    'schema field' => 'uid',
  );
}






/**
 * Implements hook_theme().
 *
 */
function pattern_theme($existing, $type, $theme, $path) {
  return array(	
    //weekly patterns
    'pattern_dropzone_weeks' => array(
      'variables' => array('pattern'=>null),
      'file' => 'templates/pattern.theme.inc'
    ),
    'pattern_weekly_pattern_container' => array(
      'variables' => array('weeks'=>array()),
      'file' => 'templates/pattern.theme.inc',
      'template' => 'templates/pattern_weekly_pattern_container'
    ),
    'pattern_week_of_dropzones' => array(
      'variables' => array('pattern'=>null, 'datesets'=>null, 'wid'=>null, 'dates'=>null),
      'file' => 'templates/pattern.theme.inc',
    ),
  );
}


/*******************************************************************************
 ********************************* Pattern API's **********************************
 ******************************************************************************/

/**
 * Access callback for pattern.
 */
function pattern_access($op, $pattern, $account = NULL, $entity_type = NULL) {
  global $user;

  if (!isset($account)) {
    $account = $user;
  }
  switch ($op) {
    case 'create':
      return user_access('administer pattern entities', $account)
          || user_access('create pattern entities', $account);
    case 'view':
      return user_access('administer pattern entities', $account)
          || user_access('view pattern entities', $account);
    case 'edit':
      return user_access('administer pattern entities')
          || user_access('edit any pattern entities')
          || (user_access('edit own pattern entities') && ($pattern->uid == $account->uid));
  }
}

/**
 * Load a pattern.
 */
function pattern_load($pid, $reset = FALSE) {
  $pattern = pattern_load_multiple(array($pid), array(), $reset);
  return reset($pattern);
}

/**
 * Load multiple pattern based on certain conditions.
 */
function pattern_load_multiple($pids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('pattern', $pids, $conditions, $reset);
}

/**
 * Save pattern.
 */
function pattern_save($pattern) {
  entity_save('pattern', $pattern);
}

/**
 * Delete single pattern.
 */
function pattern_delete($pattern) {
  entity_delete('pattern', entity_id('pattern' ,$pattern));
}

/**
 * Delete multiple pattern.
 */
function pattern_delete_multiple($pattern_ids) {
  entity_delete_multiple('pattern', $pattern_ids);
}


/*******************************************************************************
 ****************************** Pattern Type API's ********************************
 ******************************************************************************/

/**
 * Access callback for pattern Type.
 */
function pattern_type_access($op, $entity = NULL) {
  return user_access('administer pattern types');
}

/**
 * Load pattern Type.
 */
function pattern_type_load($pattern_type) {
  return pattern_types($pattern_type);
}

/**
 * List of pattern Types.
 */
function pattern_types($type_name = NULL) {
  $types = entity_load_multiple_by_name('pattern_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Save pattern type entity.
 */
function pattern_type_save($pattern_type) {
  entity_save('pattern_type', $pattern_type);
}

/**
 * Delete single case type.
 */
function pattern_type_delete($pattern_type) {
  entity_delete('pattern_type', entity_id('pattern_type' ,$pattern_type));
}

/**
 * Delete multiple case types.
 */
function pattern_type_delete_multiple($pattern_type_ids) {
  entity_delete_multiple('pattern_type', $pattern_type_ids);
}

/**
  * Implements hook_views_api().
  */
function pattern_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'pattern'),
  );
}



/*******************************************************************************
 ****************************** Pattern retreival ********************************
 ******************************************************************************/
 
function pattern_get($machine_name) {
  
  $result = db_select("pattern","a")
    ->fields("a",array("machine_name","pid"))
    ->condition("machine_name", $machine_name, '=')
    ->execute()
    ->fetchAssoc();
  
  if (isset($result['machine_name']) && isset($result['pid'])) {
    $entity = pattern_load($result['pid']);
    $value = pattern_eval_return($entity->loadcode,array('entity'=>$entity));
    if ($value == '' || empty($value) || $value==null) {
      return $entity;
    } else {
      return $value;
    }
  }
  
  return false;
  
}



/**
 * sort dateset objects into associative array keyed by pid
 */
function pattern_get_repeating_datesets_by_pattern($datesets) {
  $datesets_by_pattern = array();
  foreach ($datesets as $dateset) {
    if ($dateset->isRepeating()) {
      if (!isset($datesets_by_pattern[$dateset->pid()])) {
        $datesets_by_pattern[$dateset->pid()] = array();
      }
      $datesets_by_pattern[$dateset->pid()][] = $dateset;
    } 
  }
  return $datesets_by_pattern;
}


/**
 * Evalutes the given PHP code, with the given variables defined. This is like
 * rules_php_eval() but does return the returned data from the PHP code.
 *
 * @param $code
 *   The PHP code to run, without <?php ?>
 * @param $arguments
 * Array containing variables to be extracted to the code.
 *
 * @return
 *   The return value of the evaled code.
 */
function pattern_eval_return($code, $arguments = array()) {
  extract($arguments);
  return eval($code);
}
 