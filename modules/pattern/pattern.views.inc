<?php

/**
 * @file
 *
 * Creates pattern displays of Views results.
 */
/**
 * Implementation of hook_views_plugins
 */
function pattern_views_plugins() {
  $views_path = drupal_get_path('module', 'views');
  $calendar_path = drupal_get_path('module', 'calendar');
  $module_path = drupal_get_path('module', 'pattern');
  $theme_path = $module_path;
  module_load_include('inc', 'pattern', 'theme/theme');

  // Limit these plugins to base tables that represent entities.
  $base = array_keys(date_views_base_tables());

  $data = array(
    'module' => 'pattern', // This just tells our themes are elsewhere.

    'style' => array(
      'dsm_calendar_style' => array(
        'title' => t('Date set manager'),
        'help' => t('Present view results as a manageable Calendar.'),
        'handler' => 'pattern_plugin_style',
        'path' => "$module_path/includes/views",
        'theme' => 'calendar_style',
        'theme file' => 'theme.inc',
        'theme path' => "$calendar_path/theme",
        'additional themes' => array(
          'calendar_mini' => 'style',
          'calendar_day' => 'style',
          'calendar_week' => 'style',
          'calendar_month' => 'style',
          'calendar_year' => 'style',
          'calendar_day_overlap' => 'style',
          'calendar_week_overlap' => 'style',
        ),
        'uses fields' => TRUE,
        'uses grouping' => FALSE,
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'even empty' => TRUE,
        'base' => $base,
      ),
    ),
    /*
    'row' => array(
      'pattern_entity' => array(
        'title' => t('Pattern Entities'),
        'help' => t('Displays each selected entity as a pattern item.'),
        'handler' => 'pattern_plugin_row',
        'theme' => 'views_view_fields',
        'path' => "$module_path/includes/views",
        'base' => $base,
        'uses options' => TRUE,
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
    ),
    */
  );
  return $data;
}
