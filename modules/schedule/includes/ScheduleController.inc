<?php

class ScheduleController extends EntityAPIController {

  public function create(array $values = array()) {
    global $user;
    $values += array(
      'title' => '',
      'description' => '',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
    );
    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('schedule', $entity);
    
    //$content['author'] = array('#markup' => t('Created by: !author', array('!author' => $wrapper->uid->name->value(array('sanitize' => TRUE)))));
    
    $ui = new ScheduleWeekUI($entity);
    $content['dateset_pattern'] = array(
      '#markup' => $ui->getPatternManager(),
      '#weight' => 1,
    );

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
  
  
  /**
   * Implements EntityAPIControllerInterface.
   *
   * @param $transaction
   *   Optionally a DatabaseTransaction object to use. Allows overrides to pass
   *   in their transaction object.
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    $schedules = schedule_load_multiple($ids);
    $delete = array();
    $adjust = array();
    $adjust_by_id = array();
    foreach($schedules as $schedule) {
      $datesets = $schedule->datesets();
      foreach ($datesets as $dateset) {
        $delete[] = $dateset->dsid;
        foreach ($dateset->dsm_sid[LANGUAGE_NONE] as $idx => $val) {
          if ($val['target_id'] != $schedule->sid) {
            $adjust[] = $dateset->dsid;
          } else {
            unset($dateset->dsm_sid[LANGUAGE_NONE][$idx]);
            $adjust_by_id[$dateset->dsid] = $dateset;
          }
        }
      }
    }
    $adjust = array_unique($adjust);
    $delete = array_diff($delete,$adjust);
    
    foreach($adjust as $dsid) {
      dateset_save($adjust_by_id[$dsid]);
    }
    
    dateset_delete_multiple($delete);
    
    parent::delete($ids, $transaction);
  }
}