<?php

/**
 * UI controller for schedule Type.
 */
class ScheduleTypeUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage schedule types.';
    return $items;
  }
}