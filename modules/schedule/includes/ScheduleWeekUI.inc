<?php

class ScheduleWeekUI {
  public $schedule;
  public function __construct($entity) {
    $this->schedule = $entity;
  }
  public function getPatternManager() {
    $args = arg();
    //first 2 args should be "schedule" and "sid"
    array_splice($args, 0, 2);
    $view = views_get_view("dsm_calendar");
    if (!$view || !$view->access("block_3")) {
      return;
    }
    $view->override_path = "schedule/" . $this->schedule->sid;
    return $view->preview("block_3", array(date('Y') . '-' . date('m'), $this->schedule->sid));
  }
}