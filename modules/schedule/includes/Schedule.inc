<?php

/**
 * schedule class.
 */
class Schedule extends Entity {

  public $builder;
  
  public function __construct(array $values = array(), $entityType = NULL) {
    parent::__construct($values, $entityType);
    $this->builder = new ScheduleWeekBuilder($this);
  }
  
  protected function defaultLabel() {
    return $this->title;
  }
  
  public function datesets() {
    //TODO: use CER and a "datesets" entity refernce field?
    $query = new EntityFieldQuery();

    $query->entityCondition('entity_type', 'dateset')
      //->entityCondition('bundle', '') ? limit to bundle?
      ->fieldCondition('dsm_sid', 'target_id', $this->sid, '=');
      //->addMetaData('account', user_load(1)); // Run the query as user 1.

    $result = $query->execute();
    $dsids = array();
    if (count($result) == 0) {
      return array();
    }
    foreach ($result['dateset'] as $dsid) {
      $dsids[] = $dsid->dsid;
    }
    
    return dateset_load_multiple($dsids);
  }

  protected function defaultUri() {
    return array('path' => 'dsm/'.$this->uid.'/schedule/' . $this->identifier());
  }
}