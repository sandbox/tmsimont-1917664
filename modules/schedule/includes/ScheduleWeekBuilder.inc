<?php 

class ScheduleWeekBuilder extends WeekBuilder{

  public $month;
  public $year;
  
  public function __construct($pattern) {
    $this->month = date('n');
    $this->year = date('Y');
    parent::__construct($pattern);
  }
  
  /**
   * Return an array of weeks 0-5 that hold
   * a rel_week attribute identifying the week's yearly index (week of year)
   * and array of days 0-6 that each hold a timestamp corresponding to that
   * day
   * 
   * @param $month ( 0-11 )
   * @param $year (yyyy)
   */
  function _getWeeks($data){
    extract($data);
    $month = intval($month);
    $year = intval($year);
    $weeks = array();
    $day_of_month=1;
    $date = new DateObject(array('year'=>$year, 'month'=>$month, 'day'=>$day_of_month));
    $start = $date->format('w'); //numeric starting day of week (0-6)
    $last_day_of_month = $date->format('t'); // number of days in month
     
    $week_of_year = 0;
    
    do{
      //add one when calculating date 'W' (ISO-8601 week number of year) because weeks start on monday in this format
      $date->setDate($year, $month, ($day_of_month+1));
      $week_of_year = intval($date->format("W"));
      
      //check for weeks in January that are starting the year before
      if ($month==1 && $week_of_year > 50) {
        $week_of_year = 0;
      }
      
      //roll through days in week
      for ($day_of_week=$start; $day_of_week<7; $day_of_week++) {
        if ($day_of_month <= $last_day_of_month) {
          $date->setDate($year, $month, $day_of_month++);
          $weeks[$week_of_year][$day_of_week]['dates'] = array(clone $date);
        }
      }
      
      //reset start of week so subsequent weeks aren't offset;
      $start = 0;
      
    } while ($day_of_month <= $last_day_of_month);
    
    return array($year => $weeks);
  }
  
  public function getWeekBuilderData() {
    $data = array(
      'month' => $this->month,
      'year' => $this->year
    );
    return $data;
  }
  
  public function getDay($yid, $wid, $did) {
    //set this up for the right month for the given wid id and yid
    $this->month = date('n', strtotime('1 January ' . $yid . ' +' . $wid . ' weeks'));
    $this->year = $yid;
    $weeks = $this->weeks();
    return $weeks[$yid][$wid][$did];
  }
}