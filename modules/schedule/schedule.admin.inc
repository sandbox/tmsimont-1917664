<?php

/**
 * Generates the schedule type editing form.
 */
function schedule_type_form($form, &$form_state, $schedule_type, $op = 'edit') {

  if ($op == 'clone') {
    $schedule_type->label .= ' (cloned)';
    $schedule_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $schedule_type->label,
    '#description' => t('The human-readable name of this schedule type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($schedule_type->type) ? $schedule_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $schedule_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'schedule_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this schedule type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($schedule_type->description) ? $schedule_type->description : '',
    '#description' => t('Description about the schedule type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save schedule type'),
    '#weight' => 40,
  );

  if (!$schedule_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete schedule type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('schedule_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Submit handler for creating/editing schedule_type.
 */
function schedule_type_form_submit(&$form, &$form_state) {
  $schedule_type = entity_ui_form_submit_build_entity($form, $form_state);
  
  // Save and go back.
  schedule_type_save($schedule_type);

  // Redirect user back to list of schedule types.
  $form_state['redirect'] = 'admin/structure/dsm/schedule-types';
}

function schedule_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/dsm/schedule-types/' . $form_state['schedule_type']->type . '/delete';
}

/**
 * schedule type delete form.
 */
function schedule_type_form_delete_confirm($form, &$form_state, $schedule_type) {
  $form_state['schedule_type'] = $schedule_type;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['schedule_type_id'] = array('#type' => 'value', '#value' => entity_id('schedule_type' ,$schedule_type));
  return confirm_form($form,
    t('Are you sure you want to delete schedule type %title?', array('%title' => entity_label('schedule_type', $schedule_type))),
    'schedule' . entity_id('schedule_type' ,$schedule_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * schedule type delete form submit handler.
 */
function schedule_type_form_delete_confirm_submit($form, &$form_state) {
  $schedule_type = $form_state['schedule_type'];
  schedule_type_delete($schedule_type);

  watchdog('schedule_type', '@type: deleted %title.', array('@type' => $schedule_type->type, '%title' => $schedule_type->label));
  drupal_set_message(t('@type %title has been deleted.', array('@type' => $schedule_type->type, '%title' => $schedule_type->label)));

  $form_state['redirect'] = 'admin/structure/dsm/schedule-types';
}

/**
 * Page to select schedule Type to add new schedule.
 */
function schedule_add_page($user) {
  $items = array();
  foreach (schedule_types() as $schedule_type_key => $schedule_type) {
    $items[] = l(entity_label('schedule_type', $schedule_type), 'dsm/' . $user->uid . '/schedule/add/' . $schedule_type_key);
  }
  return array('list' => array('#theme' => 'item_list', '#items' => $items, '#title' => t('Select type of schedule to create.')));
}

/**
 * Add new schedule page callback.
 */
function schedule_add($type) {
  $schedule_type = schedule_types($type);

  $schedule = entity_create('schedule', array('type' => $type));
  drupal_set_title(t('Create @name schedule', array('@name' => entity_label('schedule_type', $schedule_type))));

  $output = drupal_get_form('schedule_form', $schedule);

  return $output;
}

/**
 * schedule Form.
 */
function schedule_form($form, &$form_state, $schedule) {
  $form_state['schedule'] = $schedule;

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name'),
    '#default_value' => $schedule->title,
  );
  
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $schedule->uid,
  );

  field_attach_form('schedule', $schedule, $form, $form_state);
  
  $form['sharing'] = array(
    '#type' => 'vertical_tabs',
  );
  
  $form['visibility'] = array(
    '#description' => t("Decide if this schedule should be kept private or shared with the site's visitors.  Some dates on the schedule may override this setting."),
    '#title' => t("Privacy"),
    '#type' => 'fieldset',
    '#group' => 'sharing',
  );
  $form['visibility']['privacy'] = array(
    '#title' => t("Schedule display is"),
    '#type' => 'radios',
    '#options' => array(
      'public' => t("Public"),
      'private' => t("Private"),
    ),
  );
  
  $form['user_relationships'] = array(
    '#title' => t("Shared management"),
    '#type' => 'fieldset',
    '#group' => 'sharing',
  );
  $form['user_relationships']['share'] = array(
    '#title' => t("Allow other users to manage dates on this schedule"),
    '#type' => 'checkbox',
    '#value' => 'Yes',
  );
  $form['user_relationships']['authoring'] = array(
    '#title' => t("These users can manage my schedule"),
    '#type' => 'checkboxes',
    '#options' => array(
      t("Users of x relation type"),
      t("Users of y relation type"),
    ),
  );
  if (module_exists("entityavailability")) {
    $form['conflicts'] = array(
      '#title' => t("Conflicts"),
      '#type' => 'fieldset',
      '#group' => 'sharing',
    );
    $form['conflicts']['no_approval'] = array(
      '#title' => t("Actions allowed without my approval"),
      '#description' => t("These settings allow you to give certain users the ability to resolve conflicts without your approval."),
      '#type' => 'fieldset',
      '#collapsible' => true,
      '#collapsed' => false,
    );
    $form['conflicts']['approval'] = array(
      '#title' => t("Actions allowed only with my approval"),
      '#description' => t("These settings allow you to give certain users the ability to send you a message so conflicts can be resolved only with your approval."),
      '#type' => 'fieldset',
      '#collapsible' => true,
      '#collapsed' => true,
    );

    foreach ( entityavailabiltiy_conflict_resolution_types() as $machine => $readable) {
      $form['conflicts']['no_approval'][$machine] = array(
        '#title' => t("These users can ") . $readable,
        '#type' => 'checkboxes',
        '#options' => array(
          t("Me"),
          t("Users of x relation type"),
          t("Users of y relation type"),
        ),
      );
      $form['conflicts']['approval']['with_approval_' . $machine] = array(
        '#title' => t("These users can ") . $readable,
        '#type' => 'checkboxes',
        '#options' => array(
          t("Me"),
          t("Users of x relation type"),
          t("Users of y relation type"),
        ),
      );
    }
  }

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => $submit + array('schedule_form_submit'),
  );

  // Show Delete button if we edit schedule.
  $schedule_id = entity_id('schedule' ,$schedule);
  if (!empty($schedule_id) && schedule_access('edit', $schedule)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('schedule_form_submit_delete'),
    );
  }

  $form['#validate'][] = 'schedule_form_validate';

  return $form;
}

function schedule_form_validate($form, &$form_state) {

}

/**
 * schedule submit handler.
 */
function schedule_form_submit($form, &$form_state) {
  $schedule = $form_state['schedule'];

  entity_form_submit_build_entity('schedule', $schedule, $form, $form_state);

  schedule_save($schedule);

  $schedule_uri = entity_uri('schedule', $schedule);

  $form_state['redirect'] = $schedule_uri['path'];

  drupal_set_message(t('Schedule %title saved.', array('%title' => entity_label('schedule', $schedule))));
}

function schedule_form_submit_delete($form, &$form_state) {
  $schedule = $form_state['schedule'];
  $schedule_uri = entity_uri('schedule', $schedule);
  $form_state['redirect'] = $schedule_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function schedule_delete_form($form, &$form_state, $schedule) {
  $form_state['schedule'] = $schedule;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['schedule_type_id'] = array('#type' => 'value', '#value' => entity_id('schedule' ,$schedule));
  $schedule_uri = entity_uri('schedule', $schedule);
  return confirm_form($form,
    t('Are you sure you want to delete schedule %title?', array('%title' => entity_label('schedule', $schedule))),
    $schedule_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function schedule_delete_form_submit($form, &$form_state) {
  $schedule = $form_state['schedule'];
  schedule_delete($schedule);

  drupal_set_message(t('schedule %title deleted.', array('%title' => entity_label('schedule', $schedule))));

  $form_state['redirect'] = 'dsm';
}
