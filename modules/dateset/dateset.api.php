<?php
/**
 * @file
 * Hooks provided by this module.
 */

/**
 * @addtogroup hooks
 * @{
 */
 
/*
 * Acts on dateset looking for exclusions
 *
 * This hook is invoked during $dateset->exclusions() when it asks
 * Drupal what exclusions have been applied to the date.
 *
 * @param Dateset $dateset
 *   The Dateset entity that is looking for exclusions
 *
 */
function hook_dateset_exclusions_add(Dateset $dateset) {
  
}

/**
 * Acts on dateset being loaded from the database.
 *
 * This hook is invoked during $dateset loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array $entities
 *   An array of $dateset entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_dateset_load(array $entities) {
  $result = db_query('SELECT pid, foo FROM {dateset} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
 * Responds when a $dateset is inserted.
 *
 * This hook is invoked after the $dateset is inserted into the database.
 *
 * @param dateset $dateset
 *   The $dateset that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_dateset_insert(dateset $dateset) {
  db_insert('dateset')
    ->fields(array(
      'id' => entity_id('dateset', $dateset),
      'extra' => print_r($dateset, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a $dateset being inserted or updated.
 *
 * This hook is invoked before the $dateset is saved to the database.
 *
 * @param dateset $dateset
 *   The $dateset that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_dateset_presave(dateset $dateset) {
  $dateset->name = 'foo';
}

/**
 * Responds to a $dateset being updated.
 *
 * This hook is invoked after the $dateset has been updated in the database.
 *
 * @param dateset $dateset
 *   The $dateset that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_dateset_update(dateset $dateset) {
  db_update('dateset')
    ->fields(array('extra' => print_r($dateset, TRUE)))
    ->condition('id', entity_id('dateset', $dateset))
    ->execute();
}

/**
 * Responds to $dateset deletion.
 *
 * This hook is invoked after the $dateset has been removed from the database.
 *
 * @param dateset $dateset
 *   The $dateset that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_dateset_delete(dateset $dateset) {
  db_delete('dateset')
    ->condition('pid', entity_id('dateset', $dateset))
    ->execute();
}

/**
 * Act on a dateset that is being assembled before rendering.
 *
 * @param $dateset
 *   The dateset entity.
 * @param $view_mode
 *   The view mode the dateset is rendered in.
 * @param $langcode
 *   The language code used for rendering.
 *
 * The module may add elements to $dateset->content prior to rendering. The
 * structure of $dateset->content is a renderable array as expected by
 * drupal_render().
 *
 * @see hook_entity_prepare_view()
 * @see hook_entity_view()
 */
function hook_dateset_view($dateset, $view_mode, $langcode) {
  $dateset->content['my_additional_field'] = array(
    '#markup' => $additional_field,
    '#weight' => 10,
    '#theme' => 'mymodule_my_additional_field',
  );
}

/**
 * Alter the results of entity_view() for datesets.
 *
 * @param $build
 *   A renderable array representing the dateset content.
 *
 * This hook is called after the content has been assembled in a structured
 * array and may be used for doing processing which requires that the complete
 * dateset content structure has been built.
 *
 * If the module wishes to act on the rendered HTML of the dateset rather than
 * the structured content array, it may use this hook to add a #post_render
 * callback. Alternatively, it could also implement hook_preprocess_dateset().
 * See drupal_render() and theme() documentation respectively for details.
 *
 * @see hook_entity_view_alter()
 */
function hook_dateset_view_alter($build) {
  if ($build['#view_mode'] == 'full' && isset($build['an_additional_field'])) {
    // Change its weight.
    $build['an_additional_field']['#weight'] = -10;

    // Add a #post_render callback to act on the rendered HTML of the entity.
    $build['#post_render'][] = 'my_module_post_render';
  }
}

/**
 * Acts on dateset_type being loaded from the database.
 *
 * This hook is invoked during dateset_type loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array $entities
 *   An array of dateset_type entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_dateset_type_load(array $entities) {
  $result = db_query('SELECT pid, foo FROM {dateset} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
 * Responds when a dateset_type is inserted.
 *
 * This hook is invoked after the dateset_type is inserted into the database.
 *
 * @param datesetType $dateset_type
 *   The dateset_type that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_dateset_type_insert(datesetType $dateset_type) {
  db_insert('dateset')
    ->fields(array(
      'id' => entity_id('dateset_type', $dateset_type),
      'extra' => print_r($dateset_type, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a dateset_type being inserted or updated.
 *
 * This hook is invoked before the dateset_type is saved to the database.
 *
 * @param datesetType $dateset_type
 *   The dateset_type that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_dateset_type_presave(datesetType $dateset_type) {
  $dateset_type->name = 'foo';
}

/**
 * Responds to a dateset_type being updated.
 *
 * This hook is invoked after the dateset_type has been updated in the database.
 *
 * @param datesetType $dateset_type
 *   The dateset_type that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_dateset_type_update(datesetType $dateset_type) {
  db_update('dateset')
    ->fields(array('extra' => print_r($dateset_type, TRUE)))
    ->condition('id', entity_id('dateset_type', $dateset_type))
    ->execute();
}

/**
 * Responds to dateset_type deletion.
 *
 * This hook is invoked after the dateset_type has been removed from the database.
 *
 * @param datesetType $dateset_type
 *   The dateset_type that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_dateset_type_delete(datesetType $dateset_type) {
  db_delete('dateset')
    ->condition('pid', entity_id('dateset_type', $dateset_type))
    ->execute();
}

/**
 * Define default dateset_type configurations.
 *
 * @return
 *   An array of default dateset_type, keyed by machine names.
 *
 * @see hook_default_dateset_type_alter()
 */
function hook_default_dateset_type() {
  $defaults['main'] = entity_create('dateset_type', array(
    // …
  ));
  return $defaults;
}

/**
 * Alter default dateset_type configurations.
 *
 * @param array $defaults
 *   An array of default dateset_type, keyed by machine names.
 *
 * @see hook_default_dateset_type()
 */
function hook_default_dateset_type_alter(array &$defaults) {
  $defaults['main']->name = 'custom name';
}
