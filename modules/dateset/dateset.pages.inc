<?php

/**
 * Date set view callback.
 */
function dateset_view($dateset) {
  drupal_set_title(entity_label('dateset', $dateset));
  return entity_view('dateset', array(entity_id('dateset', $dateset) => $dateset), 'full');
}

