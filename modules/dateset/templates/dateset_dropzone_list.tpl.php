<?php 
/**
 * This is a list of datesets that correspond to a particular dropzone.
 * 
 * If it is empty, there will still need to be a list container for new 
 * datesets to be dropped into.
 * 
 */

?>
<div class="date-dropzone-list">
	<?php 
	if(is_array($datesets)){
		foreach ($datesets as $idx => $placed_dateset) {
  ?>
    <?php if ($idx < $datesets_showing_per_day): ?>
			<div class='dateset-showing'><?php print $placed_dateset; ?></div>
    <?php else: ?>
			<div class='dateset-hidden' style="display:none"><?php print $placed_dateset; ?></div>
    <?php endif; ?>
  <?php
		}
	}
	?>
</div>
<div <?php
	if(!is_array($datesets) || count($datesets)<($datesets_showing_per_day +1)){
		echo "style='display:none;' ";
	}
?>class='dateset-more-notice'>
	<p class='dateset-more-count-container'>
		<a class='dateset-more-show-link' href='javascript:void(0)' >
			<span class='dateset-more-count'><?php echo (count($datesets)-($datesets_showing_per_day)); ?></span> more &#9660;			
		</a>
		<a class='dateset-more-collapse-link' style="display:none" href='javascript:void(0)' >			
			<span class='dateset-less-count'>hide &#9650;
		</a>
	</p>
</div>
<div <?php 
if(!is_array($datesets) || count($datesets)==0){
	echo "style='display:none;' ";
}
?>class="dateset-day_link-container">
	<a href="javascript:void(0)" class="dateset-day_link">details &raquo;</a>
</div>