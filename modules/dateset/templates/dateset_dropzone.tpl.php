<?php 
/**
 * This is a "dropzone" --
 * A html element that is a droppable and clickable target.
 * It represents an entry point for dateset creation.
 */
?>
<td class="weekly-dropzone-cell">
  <div id="dropzone-id-<?php echo $id; ?>" class="date-dropzone-zone date-<?php echo $container_type; ?>-dropzone date-dropzone-container dropzone-day-<?php echo $did; ?>">
    <?php print $label; ?>
    <div class="date-dropzone-target">
      <?php echo $addlink; ?>
    </div>
    <?php print $list; ?>
  </div>
</td>