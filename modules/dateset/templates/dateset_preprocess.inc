<?php 

function dateset_preprocess_dateset_dropzone(&$vars){
	$vars['yid'] = isset($vars['yid'])?$vars['yid']:0;
  extract($vars);
  
  $vars['addlink'] = l('Add', "dateset/add/class_dates/$pid/$yid/$wid/$did", array("query"=>drupal_get_destination()));
  
	$vars['label']= theme('dateset_dropzone_label',array('did'=>$vars['did'],'dates'=>$vars['dates'],'container_type'=>$vars['container_type']));
	$vars['list']= theme('dateset_dropzone_list', array('datesets'=>$vars['datesets']));
}

function dateset_preprocess_dateset_dropzone_label(&$vars){
  if($vars['container_type']=='week'){
    $gday = $vars['did'];
    $jdday = ($gday==0 ? 6 : $gday-1 );
    $vars['label'] = jddayofweek($jdday,1);
  } elseif($vars['container_type']=='month') {
    $vars['label'] = $vars['dates'][0]->format('j');
  }
}

function dateset_preprocess_dateset_dropzone_list(&$vars){
	  $datesets = $vars['datesets'];

	  //TODO pull this from the schedule_manager.module file so it is set in only 1 place
	  $datesets_showing_per_day = 2;
    
	  $vars['datesets_showing_per_day'] = $datesets_showing_per_day;
	  if(is_array($datesets)){
		  $themed_datesets = array();
		  foreach($datesets as $idx=>$dateset){
		  	//TODO: use a view mode here
		  	$themed_datesets[] = '<b>' . $dateset->title . '</b><br/>';
		  }
		  $vars['datesets'] = $themed_datesets;
	  }

}