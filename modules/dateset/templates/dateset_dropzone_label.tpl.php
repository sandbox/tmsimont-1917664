<?php 
/**
 * This is a simple visual element that will show the user what needs
 * to be known about this dropzone.
 * 
 * @see dateset_preprocess_dateset_dropzone_label()
 */
?>
<?php if ($container_type=='week'): ?>
<h6><?php echo $label; ?></h6>
<?php	elseif ($container_type=='month'): ?>
<div class="month-date-clear-container">
	<div class='month-date-label clearfix'><?php echo $label; ?></div>
</div>
<?php endif; ?>