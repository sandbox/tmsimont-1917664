<?php

/**
 * Generates the dateset type editing form.
 */
function dateset_type_form($form, &$form_state, $dateset_type, $op = 'edit') {

  if ($op == 'clone') {
    $dateset_type->label .= ' (cloned)';
    $dateset_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $dateset_type->label,
    '#description' => t('The human-readable name of this date set type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($dateset_type->type) ? $dateset_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $dateset_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'dateset_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this date set type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($dateset_type->description) ? $dateset_type->description : '',
    '#description' => t('Description about the date set type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save date set type'),
    '#weight' => 40,
  );

  if (!$dateset_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete date set type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('dateset_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Submit handler for creating/editing dateset_type.
 */
function dateset_type_form_submit(&$form, &$form_state) {
  $dateset_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  dateset_type_save($dateset_type);

  // Redirect user back to list of dateset types.
  $form_state['redirect'] = 'admin/structure/dsm/dateset-types';
}

function dateset_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/dsm/dateset-types/' . $form_state['dateset_type']->type . '/delete';
}

/**
 * Date set type delete form.
 */
function dateset_type_form_delete_confirm($form, &$form_state, $dateset_type) {
  $form_state['dateset_type'] = $dateset_type;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['dateset_type_id'] = array('#type' => 'value', '#value' => entity_id('dateset_type' ,$dateset_type));
  return confirm_form($form,
    t('Are you sure you want to delete date set type %title?', array('%title' => entity_label('dateset_type', $dateset_type))),
    'dateset' . entity_id('dateset_type' ,$dateset_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Date set type delete form submit handler.
 */
function dateset_type_form_delete_confirm_submit($form, &$form_state) {
  $dateset_type = $form_state['dateset_type'];
  dateset_type_delete($dateset_type);

  watchdog('dateset_type', '@type: deleted %title.', array('@type' => $dateset_type->type, '%title' => $dateset_type->label));
  drupal_set_message(t('@type %title has been deleted.', array('@type' => $dateset_type->type, '%title' => $dateset_type->label)));

  $form_state['redirect'] = 'admin/structure/dsm/dateset-types';
}

/**
 * Page to select dateset Type to add new dateset.
 */
function dateset_add_page($user, $parent_type, $parent, $yid, $wid, $did) {
  $items = array();
  foreach (dateset_types() as $dateset_type_key => $dateset_type) {
    $items[] = l(entity_label('dateset_type', $dateset_type), 'dsm/' . $user->uid . '/' . $parent_type . '/' . $parent->identifier() . '/dateset/add/' . $dateset_type_key . '/' . $yid . '/' . $wid . '/' . $did, array('query'=>drupal_get_destination()));
  }
  return array(
    'list' => array(
      '#theme' => 'item_list',
      '#items' => $items,
      '#title' => t('Select type of date set to create.')
    )
  );
}

/**
 * Add new dateset page callback.
 */
function dateset_add($user, $parent_entity, $entity, $type, $yid, $wid, $did) {
  
  $dateset_type = dateset_types($type);

  $dateset = entity_create('dateset', array('type' => $type));
  drupal_set_title(t('Create @name', array('@name' => entity_label('dateset_type', $dateset_type))));

  $output = drupal_get_form('dateset_form', $dateset, $parent_entity, $yid, $wid, $did);
  
  
  return $output;
}

/**
 * Date set Form.
 */
function dateset_form($form, &$form_state, $dateset, $parent_entity, $yid = null, $wid = null, $did = null) {

  //@todo: use a function of the parent_entity to alter form?
  $parent_type = get_class($parent_entity);
  if ($parent_type == "Schedule") {
    $sid = $parent_entity->sid;
  }
  if ($parent_type == "Pattern") {
    $pid = $parent_entity->pid;
  }
  

  $form_state['dateset'] = $dateset;

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name'),
    '#default_value' => $dateset->title,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $dateset->uid,
  );
  
  field_attach_form('dateset', $dateset, $form, $form_state);
  
  if (isset($sid) && $sid != 0) {
    $form['dsm_sid'][$form['dsm_sid']['#language']]['#default_value'] = array($sid);
  }
  
  //never show pattern-related fields... it won't make sense to the user
  $form['dsm_pid']['#access'] = false;
  $form['dsm_yid']['#access'] = false;
  $form['dsm_wid']['#access'] = false;
  $form['dsm_did']['#access'] = false;
  
  if (isset($pid) && $pid != 0) {
    $form['dsm_pid'][$form['dsm_pid']['#language']]['#value'] = $pid;
  }
  if (isset($yid)) {
    $form['dsm_yid'][$form['dsm_yid']['#language']][0]['value']['#value'] = $yid;
  }
  if (isset($wid)) {
    $form['dsm_wid'][$form['dsm_wid']['#language']][0]['value']['#value'] = $wid;
  }
  if (isset($did)) {
    $form['dsm_did'][$form['dsm_did']['#language']][0]['value']['#value'] = $did;
  }
  
  
  //@todo: midnight value will trigger "All day" in date module
  //@see: Issue #874322
  if ($dateset->isAllDay()) {
    $allDay = $dateset->getAllDayDateObject(true);
    $form['dsm_times'][LANGUAGE_NONE][0]['#default_value'] = array(
      'value' => $allDay->format("Y-m-d")."T".$allDay->format("H:i:s")
    );
    //only allow numerous times per day if all day is not checked
    if (!isset($form_state['values']) && !isset($form_state['values']['dsm_times_add_more'])) {
      unset($form['dsm_times'][LANGUAGE_NONE][1]);
    }
  }
  foreach (element_children($form['dsm_times'][LANGUAGE_NONE]) as $idx) {
    $form['dsm_times'][LANGUAGE_NONE][$idx]['#states'] = array(
      'invisible' => array (
        ':input[name="dsm_times[und][0][all_day]"]' => array("checked" => TRUE)
      )
    );
  }
  
  $form['dsm_dates']['#access'] = false;  
  $form['dsm_days']['#states'] = array(
    'visible' => array (
      ':input[name="dsm_times[und][0][all_day]"]' => array("checked" => TRUE)
    )
  );
  
  
  //$form['dsm_dates_exceptions'] = $form['dsm_dates']['und'][0]['rrule']['exceptions'];
  //unset($form['dsm_dates_exceptions']['#states']);
  //dpm($form);
  /*
  for ($i = 0; $i < max($form_state['num_exceptions'][$instance], 1) ; $i++) {
    $EXCEPT = '';
    if (!empty($exceptions[$i]['datetime'])) {
      $ex_date = new DateObject($exceptions[$i]['datetime'], $exceptions[$i]['tz']);
      date_timezone_set($ex_date, timezone_open($timezone));
      $EXCEPT = date_format($ex_date, DATE_FORMAT_DATETIME);
    }
    $element['exceptions']['EXDATE'][$i] = array(
      '#tree' => TRUE,
      'datetime' => array(
        '#name' => 'exceptions|' . $instance,
        '#type' => $element['#date_repeat_widget'],
        '#default_value' => $EXCEPT,
        '#date_timezone' => !empty($element['#date_timezone']) ? $element['#date_timezone'] : date_default_timezone(),
        '#date_format' => !empty($element['#date_format']) ? date_limit_format($element['#date_format'], array('year', 'month', 'day')) : 'Y-m-d',
        '#date_text_parts'  => !empty($element['#date_text_parts']) ? $element['#date_text_parts'] : array(),
        '#date_year_range'  => !empty($element['#date_year_range']) ? $element['#date_year_range'] : '-3:+3',
        '#date_label_position' => !empty($element['#date_label_position']) ? $element['#date_label_position'] : 'within',
        '#date_flexible' => 0,
        ),
      'tz' => array('#type' => 'hidden', '#value' => $element['#date_timezone']),
      'all_day' => array('#type' => 'hidden', '#value' => 1),
      'granularity' => array('#type' => 'hidden', '#value' => serialize(array('year', 'month', 'day'))),
      );
  }
  */


  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => $submit + array('dateset_form_submit'),
  );

  // Show Delete button if we edit dateset.
  $dateset_id = entity_id('dateset' ,$dateset);
  if (!empty($dateset_id) && dateset_access('edit', $dateset)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('dateset_form_submit_delete'),
    );
  }

  $form['#validate'][] = 'dateset_form_validate';

  return $form;
}

/**
 * Implements hook_entityavailability_availability_dates_alter().
 */
function dateset_entityavailability_availability_dates_alter(&$dates_to_check, $context) {
  $rule = $context['rule'];
  $form = $context['form'];
  $form_state = $context['form_state'];

  if ($rule['referrer_to_restrict_date'] == 'dsm_dates' 
    && ( $rule['referrer_to_restrict_entity'] == 'dateset'
         || $rule['referrer_to_restrict_entity'] == 'all' )
    && $context['referrer_type'] == 'dateset') {
    $dateset = $context['referrer'];
    entity_form_submit_build_entity('dateset', $dateset, $form, $form_state);
    $dateset->prepareDateField();
    $dates_to_check = $dateset->dsm_dates[LANGUAGE_NONE];
  }
}

function dateset_form_validate($form, &$form_state) {
  // repeating dateset validation
  if (isset($form_state['values']['dsm_pid'][LANGUAGE_NONE][0]) && $form_state['values']['dsm_pid'][LANGUAGE_NONE][0]['target_id'] != 0) {
    if ($pattern = pattern_load($form_state['values']['dsm_pid'][LANGUAGE_NONE][0]['target_id'])) {
      $wid = $form_state['values']['dsm_wid'][LANGUAGE_NONE][0]['value'];
      $did = $form_state['values']['dsm_did'][LANGUAGE_NONE][0]['value'];
      $numdays = $form_state['values']['dsm_days'][LANGUAGE_NONE][0]['value'];
    
      // dsm_times is NULL when "All Day" is checked
      if ($form_state['values']['dsm_times'][LANGUAGE_NONE][0]['value'] == NULL) {
        // don't allow crazy spinning overlaps (some overlap is allowed (1.5 x patterns))
        if ( $numdays > ( 1.5 * $pattern->length() * 7) ) {
          form_set_error("dsm_days", "Please use a smaller number of days.");
          return false;
        }
        
        //warn about overlap of pattern
        if ( $numdays > $pattern->length() ) {
          drupal_set_message("The dates entered exceed the pattern length.  Make sure the date wrap is acceptable.", "warning");
        }
      } else {
        $form_state['values']['dsm_days'][LANGUAGE_NONE][0]['value'] = 1;
      }
    }
  }
}

/**
 * Date set submit handler.
 */
function dateset_form_submit($form, &$form_state) {
  $dateset = $form_state['dateset'];

  entity_form_submit_build_entity('dateset', $dateset, $form, $form_state);

  dateset_save($dateset);

  $dateset_uri = entity_uri('dateset', $dateset);

  $form_state['redirect'] = $dateset_uri['path'];

  drupal_set_message(t('Date set %title saved.', array('%title' => entity_label('dateset', $dateset))));
}

function dateset_form_submit_delete($form, &$form_state) {
  $dateset = $form_state['dateset'];
  $dateset_uri = entity_uri('dateset', $dateset);
  
  $form_state['redirect'] = array(
    $dateset_uri['path'] . '/delete', array(
      'query' => drupal_get_destination()
    )
  );

  unset($_GET['destination']);
  drupal_static_reset('drupal_get_destination');
  drupal_get_destination();
}

/**
 * Delete confirmation form.
 */
function dateset_delete_form($form, &$form_state, $dateset) {
  $form_state['dateset'] = $dateset;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['dateset_type_id'] = array('#type' => 'value', '#value' => entity_id('dateset' ,$dateset));
  $dateset_uri = entity_uri('dateset', $dateset);
  return confirm_form($form,
    t('Are you sure you want to delete date set %title?', array('%title' => entity_label('dateset', $dateset))),
    $dateset_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function dateset_delete_form_submit($form, &$form_state) {
  $dateset = $form_state['dateset'];
  dateset_delete($dateset);

  drupal_set_message(t('Date set %title deleted.', array('%title' => entity_label('dateset', $dateset))));

  $form_state['redirect'] = '<front>';
}
