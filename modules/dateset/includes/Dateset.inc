<?php


/**
 * Date set class.
 */
class Dateset extends Entity {
  protected function defaultLabel() {
    return $this->title;
  }

  protected function defaultUri() {
    extract($this->getPatternPositions());
    if ($this->isRepeating()) {
      return array('path' => "dsm/$uid/pattern/$pid/dateset/" . $this->identifier());
    } else {
      return array('path' => "dsm/$uid/schedule/$sid/dateset/" . $this->identifier());
    }
  }
  
  /**
   * Pull various pattern-positioning values from fields on the dateset
   */
  public function getPatternPositions() {
    return array(
      'uid' => !empty($this->uid)?$this->uid:0,
      'sid' => !empty($this->dsm_sid[LANGUAGE_NONE])?$this->dsm_sid[LANGUAGE_NONE][0]['target_id']:0,
      'pid' => !empty($this->dsm_pid[LANGUAGE_NONE])?$this->dsm_pid[LANGUAGE_NONE][0]['target_id']:0,
      'yid' => !empty($this->dsm_yid[LANGUAGE_NONE])?$this->dsm_yid[LANGUAGE_NONE][0]['value']:0,
      'wid' => !empty($this->dsm_wid[LANGUAGE_NONE])?$this->dsm_wid[LANGUAGE_NONE][0]['value']:0,
      'did' => !empty($this->dsm_did[LANGUAGE_NONE])?$this->dsm_did[LANGUAGE_NONE][0]['value']:0,
    );
  }
  
  /**
   * Helper function to set the Week ID value
   */
  public function setWid($value) {
    $this->dsm_wid = array(
      LANGUAGE_NONE => array(
        0 => array(
          'value' => $value
        )
      )
    );
  }
  
  /**
   * Get the pid of this dateset
   */
  public function pid() {
    extract($this->getPatternPositions());
    return $pid;
  }
  
  /**
   * For multi-day patterns, how many days are between start
   * and end date.
   */
  public function numDays() {
    if (!empty($this->dsm_days[LANGUAGE_NONE])) {
      if ($this->dsm_days[LANGUAGE_NONE][0]['value'] > 1) {
        return $this->dsm_days[LANGUAGE_NONE][0]['value'] - 1;
      }
    }
    return 0;
  }
  
  /**
   * Is this a repeating dateset
   */
  public function isRepeating() {
    // this only works on previously-existing entities, new entities need a different approach
    if (isset($this->dsm_dates[LANGUAGE_NONE][0]['rrule'])) {
      return true;
    }
    if (isset($this->is_new) && $this->is_new && !empty($this->dsm_pid[LANGUAGE_NONE])) {
      return true;
    }
    return false;
  }
  
  /**
   * Is this an all-day dateset?
   * @todo: midnight value will trigger "All day" in date module
   * @see: Issue #874322
   */
  public function isAllDay() {
    if (!(isset($this->is_new) && $this->is_new) || (isset($this->op) && $this->op == "Save")) {
      if (!isset($this->dsm_times[LANGUAGE_NONE])) {
        return true;
      } elseif (count($this->dsm_times[LANGUAGE_NONE])==0) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * return an appropriate date builder object with exclusions passed in
   */
  public function getBuilder() {
    extract($this->getPatternPositions());
    //TODO: how to handle co-dependent modules?
    //TODO: extend a base class maybe... better "builder" handling
    if ($this->isRepeating()) {
      $pattern = pattern_load($pid);
      $builder = $pattern->builder;
    } else {
      $schedule = schedule_load($sid);
      $builder = $schedule->builder;
    }
    $this->passExclusionsToBuilder($builder);
    return $builder;
  }
  
  /**
   * get a list of exclusions for this Dateset
   */
  public function exclusions() {
    $exclusions = array();
    $module_exclusions = module_invoke_all("dateset_exclusions_add", $this);
    foreach ($module_exclusions as $set) {
      $exclusions = array_merge($exclusions, $set);
    }
    return array_unique($exclusions);
  }
  
  /**
   * given a builder object, pass in the exclusions in this dateset
   */
  public function passExclusionsToBuilder(&$builder) {
    $exclusions = $this->exclusions();
    if (count($exclusions)>0) {
      $builder->setExceptions($exclusions);
    }
  }
  
  /**
   * override Entity API CRUD save.
   * configure the dates field based on pattern data.
   * This override makes it possible for the pattern entity UI to manage complex
   * repeat rules.
   */
  public function save() {
    $this->prepareDateField();
    parent::save();
  }
  
  public function prepareDateField() {
    extract($this->getPatternPositions());
    
    $builder = $this->getBuilder();

    $day = $builder->getDay($yid, $wid, $did);
    
    
    if ($this->isAllDay()) {
      $field = $this->getDateFieldAllDay($day);
    } else {
      $times = array();
      $field_info = field_info_field("dsm_times");
      $timezone_db = date_get_timezone_db($field_info['settings']['tz_handling']);
      $db_format = date_type_format($field_info['type']);
      foreach ($this->dsm_times[LANGUAGE_NONE] as $tod) {
        $startTime = new DateObject($tod['value'], $timezone_db, $db_format);
        $endTime = new DateObject($tod['value2'], $timezone_db, $db_format);
        
        $times[] = array(
          'start' => $startTime,
          'finish' => $endTime,
        );
      }
      $field = $this->getDateFieldForTimes($day, $times);
    }
    
    
    $this->dsm_dates[LANGUAGE_NONE] = $field;
  }

  /**
   * integrate the array of set dates for 1 repeating pattern 
   * with the different times of day that repeat on those set dates
   * 
   * @param array $day
   *  Data for a given day in a pattern, containing a list of dates 
   *  as well as any other additional data required.  Example:
   *  array(
   *    'dates' => array(
   *      0 => DateObject
   *      1 => DateObject
   *      2 => DateObject
   *    ),
   *    'data' => array(
   *      'rrule' => [RRULE]
   *    )
   *  )
   *  
   * @param array $times_of_day
   *  A chosen array of different times in a single day that will be 
   *  associated to each date in the given day's dates array
   *
   * @return array() field_dates
   *  An array structured in the way a date module repeating date needs
   *  to be structured.
   */
  public function getDateFieldForTimes($day=array(), $times_of_day=array()){
  
    $field_info = field_info_field("dsm_times");
    $timezone_db = date_get_timezone_db($field_info['settings']['tz_handling']);
    $db_format = date_type_format($field_info['type']);
    $timezone = date_get_timezone($field_info['settings']['tz_handling'], '');
    
    $field = array();
    //TODO: check day for ['dates'] throw error if not there
    
    foreach($day['dates'] as $date){
      //day needs to be 00:00 in dsm_dates input timezone even though it's stored in timezone_db timezone
      $date->setTimeZone(new DateTimeZone($timezone));
      $date->addGranularity("hour");
      $date->addGranularity("minute");
      $date->addGranularity("timezone");
      
      foreach($times_of_day as $tod){
        
        $times_this_day = array();
        
        $start = $tod['start'];
        //tod was set in field timezone setting, let's use that instead of timezone_db right now
        $start->setTimezone(new DateTimeZone($timezone));
        $date->setTime($start->format("H"),$start->format("i"),$start->format("s"));
        //now time is adjusted, set back to db-friendly timezone
        $date->setTimezone(new DateTimeZone($timezone_db));
        $times_this_day['value'] = $date->format("Y-m-d")."T".$date->format("H:i:s");
        
        //repeat process for finish date
        $finish = $tod['finish'];
        //tod was set in field timezone setting, let's use that instead of timezone_db right now
        $finish->setTimezone(new DateTimeZone($timezone));
        //revert day back to input timezone
        $date->setTimeZone(new DateTimeZone($timezone));
        $date->setTime($finish->format("H"),$finish->format("i"),$finish->format("s"));
        //now time is adjusted, set back to db-friendly timezone
        $date->setTimezone(new DateTimeZone($timezone_db));
        
        //push end date forward if this is an all-day, multi-day date
        if ($this->isAllDay()) {
          $incrementedDate = clone($date);
          $incrementedDate->add(new DateInterval("P".$this->numDays()."D"));
          $times_this_day['value2'] = $incrementedDate->format("Y-m-d")."T".$incrementedDate->format("H:i:s");
        } else {
          $times_this_day['value2'] = $date->format("Y-m-d")."T".$date->format("H:i:s");
        }

        //pass back timezone and field settings that were source of all values for field storage
        $times_this_day['date_type'] = 'datetime';
        $times_this_day['timezone'] = $timezone;
        $times_this_day['timezone_db'] = $timezone_db;
        
        //allow the day to contain additional information in a 'data' array (e.g. rrule)
        if (isset($day['data'])) {
          $times_this_day += $day['data'];
        }
        
        $field[] = $times_this_day;
      }
    }
    return $field;
  }
  
  /**
   * Return an array for the date field that will trigger
   * the date module's flag system for "All day" events
   */
  public function getDateFieldAllDay($day) {
    $field = array();
    $allDay = $this->getAllDayDateObject();    
    $times_of_day = array(
      array(
        'start' => $allDay,
        'finish' => $allDay,
      )
    );
    $field = $this->getDateFieldForTimes($day, $times_of_day);
    return $field;
    
  }
  
  /**
   * Get a DateObject that will trigger the date module's
   * logic for flagging an "All day" event
   */
  public function getAllDayDateObject($already_saved = false) {
    $field_info = field_info_field("dsm_times");
    $timezone = date_get_timezone($field_info['settings']['tz_handling'], '');
    $allDay = new DateObject();
    if ($already_saved) {
      $timezone_db = date_get_timezone_db($field_info['settings']['tz_handling']);
      $allDay->setTime(0, 0, 0);
      $allDay->setTimezone(new DateTimeZone($timezone_db));
    } else {
      $allDay->setTimezone(new DateTimeZone($timezone));
      // pass value that will cause date_is_all_day() to return true
      $allDay->setTime(0, 0, 0);
    }
    return $allDay;
  }
}
