<?php

class DatesetController extends EntityAPIController {

  public function create(array $values = array()) {
    global $user;
    $values += array(
      'title' => '',
      'description' => '',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
    );
    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('dateset', $entity);
    
    // $content['author'] = array('#markup' => t('Created by: !author', array('!author' => $wrapper->uid->name->value(array('sanitize' => TRUE)))));

    if ($view_mode != "schedule") {
    
    } else {
      $this->addScheduleManagerLinks($entity, $content, $langcode);
    }

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
  
  public function addScheduleManagerLinks($entity, &$content, $langcode = NULL) {
    extract($entity->getPatternPositions());
    $buttons = array();
    
    $internal_sched_path = arg(0) . '/' . arg(1) . '/';
    // @todo: again this "schedule" vs. "pattern" thing is creating more code... get some kind of oop fix?
    if ($entity->isRepeating()) {
      $dateset_parent = "pattern/$pid/";
    } else {
      $dateset_parent = "schedule/$sid/";
    }
    $buttons['dateset-edit'] = array(
      'data' => l("Edit", $internal_sched_path . $dateset_parent . "dateset/" . $entity->dsid . "/edit", array("query" => drupal_get_destination())),
    );
    
    // link edit option to the repeat pattern if viewing the item on another view
    if ($entity->isRepeating() && !(arg(2)=='pattern' && arg(3)==$pid)) {
      $buttons['repeat-edit'] = array(
        'data' => l("Edit repeat", $internal_sched_path. "pattern/" . $pid)
      );
    }
    
    foreach($buttons as $id => $button) {
      if (!isset($buttons[$id]['class'])) {
        $buttons[$id]['class'] = array();
      }
      $buttons[$id]['class'][] = 'schedule-manager-link';
      $buttons[$id]['class'][] = 'sml-' . $id;
    }
    
    $button_list = array(
      'items' => $buttons,
      'attributes' => array(
        'class' => array('inline','links'),
      ),
    );
    
    
    $content['schedule_manager'] = array(
      '#markup' => theme("item_list", $button_list),
      '#weight' => -100
    );
  }
  
  public function view($entities, $view_mode = 'full', $langcode = NULL, $page = NULL) {
    //supress title by passing page=true
    if ($view_mode == "schedule") {
      return parent::view($entities, $view_mode, $langcode, TRUE);
    } else {
      return parent::view($entities, $view_mode, $langcode, $page);
    }
  }
  
}