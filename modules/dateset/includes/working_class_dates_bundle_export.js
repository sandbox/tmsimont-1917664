{
  "type" : "class_dates",
  "label" : "Class dates",
  "weight" : 0,
  "description" : "An occurrence of a class that contains a reference to a location and a teacher.",
  "rdf_mapping" : [],
  "fields" : {
    "dsm_times" : {
      "translatable" : "0",
      "entity_types" : [],
      "settings" : {
        "granularity" : {
          "hour" : "hour",
          "minute" : "minute",
          "year" : "year",
          "month" : 0,
          "day" : 0,
          "second" : 0
        },
        "tz_handling" : "user",
        "timezone_db" : "UTC",
        "cache_enabled" : 0,
        "cache_count" : "4",
        "repeat" : "0",
        "profile2_private" : false,
        "todate" : "optional"
      },
      "storage" : {
        "type" : "field_sql_storage",
        "settings" : [],
        "module" : "field_sql_storage",
        "active" : "1",
        "details" : { "sql" : {
            "FIELD_LOAD_CURRENT" : { "field_data_dsm_times" : { "value" : "dsm_times_value", "value2" : "dsm_times_value2" } },
            "FIELD_LOAD_REVISION" : { "field_revision_dsm_times" : { "value" : "dsm_times_value", "value2" : "dsm_times_value2" } }
          }
        }
      },
      "foreign keys" : [],
      "indexes" : [],
      "id" : "124",
      "field_name" : "dsm_times",
      "type" : "datetime",
      "module" : "date",
      "active" : "1",
      "locked" : "0",
      "cardinality" : "-1",
      "deleted" : "0",
      "columns" : {
        "value" : {
          "type" : "datetime",
          "mysql_type" : "datetime",
          "pgsql_type" : "timestamp without time zone",
          "sqlite_type" : "varchar",
          "sqlsrv_type" : "smalldatetime",
          "not null" : false,
          "sortable" : true,
          "views" : true
        },
        "value2" : {
          "type" : "datetime",
          "mysql_type" : "datetime",
          "pgsql_type" : "timestamp without time zone",
          "sqlite_type" : "varchar",
          "sqlsrv_type" : "smalldatetime",
          "not null" : false,
          "sortable" : true,
          "views" : false
        }
      },
      "bundles" : { "dateset" : [ "class_dates" ] }
    },
    "dsm_pid" : {
      "translatable" : "0",
      "entity_types" : [],
      "settings" : {
        "profile2_private" : false,
        "target_type" : "pattern",
        "handler" : "base",
        "handler_settings" : {
          "target_bundles" : [],
          "sort" : { "type" : "none" },
          "behaviors" : { "views-select-list" : { "status" : 0 } }
        }
      },
      "storage" : {
        "type" : "field_sql_storage",
        "settings" : [],
        "module" : "field_sql_storage",
        "active" : "1",
        "details" : { "sql" : {
            "FIELD_LOAD_CURRENT" : { "field_data_dsm_pid" : { "target_id" : "dsm_pid_target_id" } },
            "FIELD_LOAD_REVISION" : { "field_revision_dsm_pid" : { "target_id" : "dsm_pid_target_id" } }
          }
        }
      },
      "foreign keys" : { "pattern" : { "table" : "pattern", "columns" : { "target_id" : "pid" } } },
      "indexes" : { "target_id" : [ "target_id" ] },
      "id" : "125",
      "field_name" : "dsm_pid",
      "type" : "entityreference",
      "module" : "entityreference",
      "active" : "1",
      "locked" : "0",
      "cardinality" : "1",
      "deleted" : "0",
      "columns" : { "target_id" : {
          "description" : "The id of the target entity.",
          "type" : "int",
          "unsigned" : true,
          "not null" : true
        }
      },
      "bundles" : { "dateset" : [ "class_dates" ] }
    },
    "dsm_yid" : {
      "translatable" : "0",
      "entity_types" : [],
      "settings" : { "profile2_private" : false },
      "storage" : {
        "type" : "field_sql_storage",
        "settings" : [],
        "module" : "field_sql_storage",
        "active" : "1",
        "details" : { "sql" : {
            "FIELD_LOAD_CURRENT" : { "field_data_dsm_yid" : { "value" : "dsm_yid_value" } },
            "FIELD_LOAD_REVISION" : { "field_revision_dsm_yid" : { "value" : "dsm_yid_value" } }
          }
        }
      },
      "foreign keys" : [],
      "indexes" : [],
      "id" : "126",
      "field_name" : "dsm_yid",
      "type" : "number_integer",
      "module" : "number",
      "active" : "1",
      "locked" : "0",
      "cardinality" : "1",
      "deleted" : "0",
      "columns" : { "value" : { "type" : "int", "not null" : false } },
      "bundles" : { "dateset" : [ "class_dates" ] }
    },
    "dsm_wid" : {
      "translatable" : "0",
      "entity_types" : [],
      "settings" : { "profile2_private" : false },
      "storage" : {
        "type" : "field_sql_storage",
        "settings" : [],
        "module" : "field_sql_storage",
        "active" : "1",
        "details" : { "sql" : {
            "FIELD_LOAD_CURRENT" : { "field_data_dsm_wid" : { "value" : "dsm_wid_value" } },
            "FIELD_LOAD_REVISION" : { "field_revision_dsm_wid" : { "value" : "dsm_wid_value" } }
          }
        }
      },
      "foreign keys" : [],
      "indexes" : [],
      "id" : "127",
      "field_name" : "dsm_wid",
      "type" : "number_integer",
      "module" : "number",
      "active" : "1",
      "locked" : "0",
      "cardinality" : "1",
      "deleted" : "0",
      "columns" : { "value" : { "type" : "int", "not null" : false } },
      "bundles" : { "dateset" : [ "class_dates" ] }
    },
    "dsm_did" : {
      "translatable" : "0",
      "entity_types" : [],
      "settings" : { "profile2_private" : false },
      "storage" : {
        "type" : "field_sql_storage",
        "settings" : [],
        "module" : "field_sql_storage",
        "active" : "1",
        "details" : { "sql" : {
            "FIELD_LOAD_CURRENT" : { "field_data_dsm_did" : { "value" : "dsm_did_value" } },
            "FIELD_LOAD_REVISION" : { "field_revision_dsm_did" : { "value" : "dsm_did_value" } }
          }
        }
      },
      "foreign keys" : [],
      "indexes" : [],
      "id" : "128",
      "field_name" : "dsm_did",
      "type" : "number_integer",
      "module" : "number",
      "active" : "1",
      "locked" : "0",
      "cardinality" : "1",
      "deleted" : "0",
      "columns" : { "value" : { "type" : "int", "not null" : false } },
      "bundles" : { "dateset" : [ "class_dates" ] }
    },
    "dsm_dates" : {
      "translatable" : "0",
      "entity_types" : [],
      "settings" : {
        "granularity" : {
          "month" : "month",
          "day" : "day",
          "hour" : "hour",
          "minute" : "minute",
          "year" : "year",
          "second" : 0
        },
        "tz_handling" : "site",
        "timezone_db" : "UTC",
        "cache_enabled" : 0,
        "cache_count" : "4",
        "repeat" : "1",
        "profile2_private" : false,
        "todate" : "optional"
      },
      "storage" : {
        "type" : "field_sql_storage",
        "settings" : [],
        "module" : "field_sql_storage",
        "active" : "1",
        "details" : { "sql" : {
            "FIELD_LOAD_CURRENT" : { "field_data_dsm_dates" : {
                "value" : "dsm_dates_value",
                "value2" : "dsm_dates_value2",
                "rrule" : "dsm_dates_rrule"
              }
            },
            "FIELD_LOAD_REVISION" : { "field_revision_dsm_dates" : {
                "value" : "dsm_dates_value",
                "value2" : "dsm_dates_value2",
                "rrule" : "dsm_dates_rrule"
              }
            }
          }
        }
      },
      "foreign keys" : [],
      "indexes" : [],
      "id" : "129",
      "field_name" : "dsm_dates",
      "type" : "datetime",
      "module" : "date",
      "active" : "1",
      "locked" : "0",
      "cardinality" : "-1",
      "deleted" : "0",
      "columns" : {
        "value" : {
          "type" : "datetime",
          "mysql_type" : "datetime",
          "pgsql_type" : "timestamp without time zone",
          "sqlite_type" : "varchar",
          "sqlsrv_type" : "smalldatetime",
          "not null" : false,
          "sortable" : true,
          "views" : true
        },
        "value2" : {
          "type" : "datetime",
          "mysql_type" : "datetime",
          "pgsql_type" : "timestamp without time zone",
          "sqlite_type" : "varchar",
          "sqlsrv_type" : "smalldatetime",
          "not null" : false,
          "sortable" : true,
          "views" : false
        },
        "rrule" : {
          "type" : "text",
          "not null" : false,
          "sortable" : false,
          "views" : false
        }
      },
      "bundles" : { "dateset" : [ "class_dates" ] }
    },
    "dsm_class_dates_eid" : {
      "translatable" : "0",
      "entity_types" : [],
      "settings" : {
        "profile2_private" : false,
        "target_type" : "node",
        "handler" : "base",
        "handler_settings" : {
          "target_bundles" : [],
          "sort" : { "type" : "none" },
          "behaviors" : { "views-select-list" : { "status" : 0 } }
        }
      },
      "storage" : {
        "type" : "field_sql_storage",
        "settings" : [],
        "module" : "field_sql_storage",
        "active" : "1",
        "details" : { "sql" : {
            "FIELD_LOAD_CURRENT" : { "field_data_dsm_class_dates_eid" : { "target_id" : "dsm_class_dates_eid_target_id" } },
            "FIELD_LOAD_REVISION" : { "field_revision_dsm_class_dates_eid" : { "target_id" : "dsm_class_dates_eid_target_id" } }
          }
        }
      },
      "foreign keys" : { "node" : { "table" : "node", "columns" : { "target_id" : "nid" } } },
      "indexes" : { "target_id" : [ "target_id" ] },
      "id" : "130",
      "field_name" : "dsm_class_dates_eid",
      "type" : "entityreference",
      "module" : "entityreference",
      "active" : "1",
      "locked" : "0",
      "cardinality" : "1",
      "deleted" : "0",
      "columns" : { "target_id" : {
          "description" : "The id of the target entity.",
          "type" : "int",
          "unsigned" : true,
          "not null" : true
        }
      },
      "bundles" : { "dateset" : [ "class_dates" ] }
    },
    "dsm_sid" : {
      "translatable" : "0",
      "entity_types" : [],
      "settings" : {
        "profile2_private" : false,
        "target_type" : "schedule",
        "handler" : "base",
        "handler_settings" : {
          "target_bundles" : [],
          "sort" : { "type" : "none" },
          "behaviors" : { "views-select-list" : { "status" : 0 } }
        }
      },
      "storage" : {
        "type" : "field_sql_storage",
        "settings" : [],
        "module" : "field_sql_storage",
        "active" : "1",
        "details" : { "sql" : {
            "FIELD_LOAD_CURRENT" : { "field_data_dsm_sid" : { "target_id" : "dsm_sid_target_id" } },
            "FIELD_LOAD_REVISION" : { "field_revision_dsm_sid" : { "target_id" : "dsm_sid_target_id" } }
          }
        }
      },
      "foreign keys" : { "schedule" : { "table" : "schedule", "columns" : { "target_id" : "sid" } } },
      "indexes" : { "target_id" : [ "target_id" ] },
      "id" : "131",
      "field_name" : "dsm_sid",
      "type" : "entityreference",
      "module" : "entityreference",
      "active" : "1",
      "locked" : "0",
      "cardinality" : "-1",
      "deleted" : "0",
      "columns" : { "target_id" : {
          "description" : "The id of the target entity.",
          "type" : "int",
          "unsigned" : true,
          "not null" : true
        }
      },
      "bundles" : { "dateset" : [ "class_dates" ] }
    },
    "field_number_of_days" : {
      "translatable" : "0",
      "entity_types" : [],
      "settings" : { "profile2_private" : false },
      "storage" : {
        "type" : "field_sql_storage",
        "settings" : [],
        "module" : "field_sql_storage",
        "active" : "1",
        "details" : { "sql" : {
            "FIELD_LOAD_CURRENT" : { "field_data_field_number_of_days" : { "value" : "field_number_of_days_value" } },
            "FIELD_LOAD_REVISION" : { "field_revision_field_number_of_days" : { "value" : "field_number_of_days_value" } }
          }
        }
      },
      "foreign keys" : [],
      "indexes" : [],
      "id" : "135",
      "field_name" : "field_number_of_days",
      "type" : "number_integer",
      "module" : "number",
      "active" : "1",
      "locked" : "0",
      "cardinality" : "1",
      "deleted" : "0",
      "columns" : { "value" : { "type" : "int", "not null" : false } },
      "bundles" : { "dateset" : [ "class_dates" ] }
    }
  },
  "field_instances" : {
    "dsm_times" : {
      "label" : "Times per day",
      "widget" : {
        "weight" : "5",
        "type" : "date_select",
        "module" : "date",
        "active" : 1,
        "settings" : {
          "input_format" : "custom",
          "input_format_custom" : "g:i a",
          "year_range" : "-0:+3",
          "increment" : "15",
          "label_position" : "above",
          "text_parts" : [],
          "display_all_day" : 1,
          "repeat_collapsed" : 0
        }
      },
      "settings" : {
        "default_value" : "blank",
        "default_value_code" : "today",
        "default_value2" : "same",
        "default_value_code2" : "",
        "user_register_form" : false
      },
      "display" : {
        "default" : {
          "label" : "above",
          "type" : "date_default",
          "settings" : {
            "format_type" : "long",
            "multiple_number" : "",
            "multiple_from" : "",
            "multiple_to" : "",
            "fromto" : "both",
            "show_repeat_rule" : "show"
          },
          "module" : "date",
          "weight" : 0
        },
        "schedule" : { "type" : "hidden", "label" : "above", "settings" : [], "weight" : 0 }
      },
      "required" : 0,
      "description" : "",
      "id" : "165",
      "field_id" : "124",
      "field_name" : "dsm_times",
      "entity_type" : "dateset",
      "bundle" : "class_dates",
      "deleted" : "0"
    },
    "dsm_pid" : {
      "label" : "Pattern ID Reference",
      "widget" : {
        "weight" : "1",
        "type" : "options_select",
        "module" : "options",
        "active" : 1,
        "settings" : []
      },
      "settings" : { "user_register_form" : false },
      "display" : {
        "default" : {
          "label" : "above",
          "type" : "hidden",
          "settings" : [],
          "module" : "entityreference",
          "weight" : 1
        },
        "schedule" : { "type" : "hidden", "label" : "above", "settings" : [], "weight" : 0 }
      },
      "required" : 0,
      "description" : "",
      "default_value" : null,
      "id" : "166",
      "field_id" : "125",
      "field_name" : "dsm_pid",
      "entity_type" : "dateset",
      "bundle" : "class_dates",
      "deleted" : "0"
    },
    "dsm_yid" : {
      "label" : "Year of Pattern",
      "widget" : {
        "weight" : "2",
        "type" : "number",
        "module" : "number",
        "active" : 0,
        "settings" : []
      },
      "settings" : {
        "min" : "0",
        "max" : "",
        "prefix" : "",
        "suffix" : "",
        "user_register_form" : false
      },
      "display" : {
        "default" : {
          "label" : "above",
          "type" : "hidden",
          "settings" : [],
          "module" : "number",
          "weight" : 3
        },
        "schedule" : { "type" : "hidden", "label" : "above", "settings" : [], "weight" : 0 }
      },
      "required" : 0,
      "description" : "",
      "default_value" : null,
      "id" : "167",
      "field_id" : "126",
      "field_name" : "dsm_yid",
      "entity_type" : "dateset",
      "bundle" : "class_dates",
      "deleted" : "0"
    },
    "dsm_wid" : {
      "label" : "Week of Pattern",
      "widget" : {
        "weight" : "3",
        "type" : "number",
        "module" : "number",
        "active" : 0,
        "settings" : []
      },
      "settings" : {
        "min" : "0",
        "max" : "",
        "prefix" : "",
        "suffix" : "",
        "user_register_form" : false
      },
      "display" : {
        "default" : {
          "label" : "above",
          "type" : "hidden",
          "settings" : [],
          "module" : "number",
          "weight" : 2
        },
        "schedule" : { "type" : "hidden", "label" : "above", "settings" : [], "weight" : 0 }
      },
      "required" : 0,
      "description" : "",
      "default_value" : null,
      "id" : "168",
      "field_id" : "127",
      "field_name" : "dsm_wid",
      "entity_type" : "dateset",
      "bundle" : "class_dates",
      "deleted" : "0"
    },
    "dsm_did" : {
      "label" : "Day of Pattern",
      "widget" : {
        "weight" : "4",
        "type" : "number",
        "module" : "number",
        "active" : 0,
        "settings" : []
      },
      "settings" : {
        "min" : "0",
        "max" : "",
        "prefix" : "",
        "suffix" : "",
        "user_register_form" : false
      },
      "display" : {
        "default" : {
          "label" : "above",
          "type" : "hidden",
          "settings" : [],
          "module" : "number",
          "weight" : 3
        },
        "schedule" : { "type" : "hidden", "label" : "above", "settings" : [], "weight" : 0 }
      },
      "required" : 0,
      "description" : "",
      "default_value" : null,
      "id" : "169",
      "field_id" : "128",
      "field_name" : "dsm_did",
      "entity_type" : "dateset",
      "bundle" : "class_dates",
      "deleted" : "0"
    },
    "dsm_dates" : {
      "label" : "Dates",
      "widget" : {
        "weight" : "8",
        "type" : "date_select",
        "module" : "date",
        "active" : 1,
        "settings" : {
          "input_format" : "m\/d\/Y - H:i:s",
          "input_format_custom" : "",
          "year_range" : "-3:+3",
          "increment" : "15",
          "label_position" : "above",
          "text_parts" : [],
          "display_all_day" : 0,
          "repeat_collapsed" : 1
        }
      },
      "settings" : {
        "default_value" : "now",
        "default_value_code" : "",
        "default_value2" : "same",
        "default_value_code2" : "",
        "user_register_form" : false
      },
      "display" : {
        "default" : {
          "label" : "above",
          "type" : "hidden",
          "settings" : [],
          "module" : "date",
          "weight" : 4
        },
        "schedule" : { "type" : "hidden", "label" : "above", "settings" : [], "weight" : 0 }
      },
      "required" : 0,
      "description" : "",
      "id" : "170",
      "field_id" : "129",
      "field_name" : "dsm_dates",
      "entity_type" : "dateset",
      "bundle" : "class_dates",
      "deleted" : "0"
    },
    "dsm_class_dates_eid" : {
      "label" : "Represented Entity",
      "widget" : {
        "weight" : "7",
        "type" : "options_select",
        "module" : "options",
        "active" : 1,
        "settings" : []
      },
      "settings" : { "user_register_form" : false },
      "display" : {
        "default" : {
          "label" : "above",
          "type" : "entityreference_label",
          "settings" : { "link" : false },
          "module" : "entityreference",
          "weight" : 5
        },
        "schedule" : { "type" : "hidden", "label" : "above", "settings" : [], "weight" : 0 }
      },
      "required" : 0,
      "description" : "The represented entity is a fieldable entity that is represented by this and possibly other date sets.  For example, several class date sets could individually represent a class node entity.  The class node entity would contain fields that present information that is relevant to the class regardless of which date is being presented by the date set.",
      "default_value" : null,
      "id" : "171",
      "field_id" : "130",
      "field_name" : "dsm_class_dates_eid",
      "entity_type" : "dateset",
      "bundle" : "class_dates",
      "deleted" : "0"
    },
    "dsm_sid" : {
      "label" : "Schedule(s)",
      "widget" : {
        "weight" : "0",
        "type" : "options_buttons",
        "module" : "options",
        "active" : 1,
        "settings" : []
      },
      "settings" : { "user_register_form" : false },
      "display" : {
        "default" : {
          "label" : "above",
          "type" : "hidden",
          "settings" : [],
          "module" : "entityreference",
          "weight" : 6
        },
        "schedule" : { "type" : "hidden", "label" : "above", "settings" : [], "weight" : 0 }
      },
      "required" : 1,
      "description" : "",
      "default_value" : null,
      "id" : "172",
      "field_id" : "131",
      "field_name" : "dsm_sid",
      "entity_type" : "dateset",
      "bundle" : "class_dates",
      "deleted" : "0"
    },
    "field_number_of_days" : {
      "label" : "Number of days",
      "widget" : {
        "weight" : "6",
        "type" : "number",
        "module" : "number",
        "active" : 0,
        "settings" : []
      },
      "settings" : {
        "min" : "1",
        "max" : "",
        "prefix" : "",
        "suffix" : "",
        "user_register_form" : false
      },
      "display" : {
        "default" : {
          "label" : "above",
          "type" : "number_integer",
          "settings" : {
            "thousand_separator" : " ",
            "decimal_separator" : ".",
            "scale" : 0,
            "prefix_suffix" : true
          },
          "module" : "number",
          "weight" : 7
        },
        "schedule" : { "type" : "hidden", "label" : "above", "settings" : [], "weight" : 0 }
      },
      "required" : 0,
      "description" : "",
      "default_value" : [ { "value" : "1" } ],
      "id" : "178",
      "field_id" : "135",
      "field_name" : "field_number_of_days",
      "entity_type" : "dateset",
      "bundle" : "class_dates",
      "deleted" : "0"
    }
  }
}