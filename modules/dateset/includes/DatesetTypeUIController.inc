<?php


/**
 * UI controller for Date set Type.
 */
class DatesetTypeUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage date set types.';
    return $items;
  }
}