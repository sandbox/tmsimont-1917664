<?php

function exclusion_overview($user, $parent_type, $parent_entity) {
  if ($parent_type == 'pattern') {
    return views_embed_view('exclusion', 'block_1', $parent_entity->uid, $parent_entity->pid);
  }
  return "Exclusion parent entity type not supported.";
}

/**
 * Generates the exclusion type editing form.
 */
function exclusion_type_form($form, &$form_state, $exclusion_type, $op = 'edit') {

  if ($op == 'clone') {
    $exclusion_type->label .= ' (cloned)';
    $exclusion_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $exclusion_type->label,
    '#description' => t('The human-readable name of this exclusion type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($exclusion_type->type) ? $exclusion_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $exclusion_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'exclusion_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this exclusion type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($exclusion_type->description) ? $exclusion_type->description : '',
    '#description' => t('Description about the exclusion type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save exclusion type'),
    '#weight' => 40,
  );

  if (!$exclusion_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete exclusion type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('exclusion_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Submit handler for creating/editing exclusion_type.
 */
function exclusion_type_form_submit(&$form, &$form_state) {
  $exclusion_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  exclusion_type_save($exclusion_type);

  // Redirect user back to list of exclusion types.
  $form_state['redirect'] = 'admin/structure/dsm/exclusion-types';
}

function exclusion_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/dsm/exclusion-types/' . $form_state['exclusion_type']->type . '/delete';
}

/**
 * Exclusion type delete form.
 */
function exclusion_type_form_delete_confirm($form, &$form_state, $exclusion_type) {
  $form_state['exclusion_type'] = $exclusion_type;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['exclusion_type_id'] = array('#type' => 'value', '#value' => entity_id('exclusion_type' ,$exclusion_type));
  return confirm_form($form,
    t('Are you sure you want to delete exclusion type %title?', array('%title' => entity_label('exclusion_type', $exclusion_type))),
    'exclusion' . entity_id('exclusion_type' ,$exclusion_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Exclusion type delete form submit handler.
 */
function exclusion_type_form_delete_confirm_submit($form, &$form_state) {
  $exclusion_type = $form_state['exclusion_type'];
  exclusion_type_delete($exclusion_type);

  watchdog('exclusion_type', '@type: deleted %title.', array('@type' => $exclusion_type->type, '%title' => $exclusion_type->label));
  drupal_set_message(t('@type %title has been deleted.', array('@type' => $exclusion_type->type, '%title' => $exclusion_type->label)));

  $form_state['redirect'] = 'admin/structure/dsm/exclusion-types';
}

/**
 * Page to select exclusion Type to add new exclusion.
 */
function exclusion_add_page($user, $parent_type, $parent) {
  $items = array();
  foreach (exclusion_types() as $exclusion_type_key => $exclusion_type) {
    $items[] = l(entity_label('exclusion_type', $exclusion_type), 'dsm/' . $user->uid . '/' . $parent_type . '/' . $parent->identifier() . '/exclusion/add/' . $exclusion_type_key , array('query'=>drupal_get_destination()));
  }
  return array(
    'list' => array(
      '#theme' => 'item_list',
      '#items' => $items,
      '#title' => t('Select type of exclusion to create.')
    )
  );
}

/**
 * Add new exclusion page callback.
 */
function exclusion_add($user, $parent_entity, $entity, $type) {
  
  $exclusion_type = exclusion_types($type);

  $exclusion = entity_create('exclusion', array('type' => $type));
  drupal_set_title(t('Create @name', array('@name' => entity_label('exclusion_type', $exclusion_type))));

  $output = drupal_get_form('exclusion_form', $exclusion, $parent_entity);
  
  
  return $output;
}

/**
 * Exclusion Form.
 */
function exclusion_form($form, &$form_state, $exclusion, $parent_entity) {
  $form_state['exclusion'] = $exclusion;

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name'),
    '#default_value' => $exclusion->title,
    '#weight' => -10
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $exclusion->uid,
  );
  
  field_attach_form('exclusion', $exclusion, $form, $form_state);

  $form_builder = new ExclusionFormBuilder($form, $form_state, $exclusion, $parent_entity);
  $form_builder->attach_form();
    
  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => $submit + array('exclusion_form_submit'),
  );

  // Show Delete button if we edit exclusion.
  $exclusion_id = entity_id('exclusion' ,$exclusion);
  if (!empty($exclusion_id) && exclusion_access('edit', $exclusion)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('exclusion_form_submit_delete'),
    );
  }

  $form['#validate'][] = 'exclusion_form_validate';

  return $form;
}

function exclusion_form_validate($form, &$form_state) {
}

/**
 * Exclusion submit handler.
 */
function exclusion_form_submit($form, &$form_state) {
  $exclusion = $form_state['exclusion'];

  entity_form_submit_build_entity('exclusion', $exclusion, $form, $form_state);

  exclusion_save($exclusion);

  $exclusion_uri = entity_uri('exclusion', $exclusion);

  $form_state['redirect'] = $exclusion_uri['path'];

  drupal_set_message(t('Exclusion %title saved.', array('%title' => entity_label('exclusion', $exclusion))));
}

function exclusion_form_submit_delete($form, &$form_state) {
  $exclusion = $form_state['exclusion'];
  $exclusion_uri = entity_uri('exclusion', $exclusion);
  
  $form_state['redirect'] = array(
    $exclusion_uri['path'] . '/delete', array(
      'query' => drupal_get_destination()
    )
  );

  unset($_GET['destination']);
  drupal_static_reset('drupal_get_destination');
  drupal_get_destination();
}

/**
 * Delete confirmation form.
 */
function exclusion_delete_form($form, &$form_state, $exclusion) {
  $form_state['exclusion'] = $exclusion;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['exclusion_type_id'] = array('#type' => 'value', '#value' => entity_id('exclusion' ,$exclusion));
  $exclusion_uri = entity_uri('exclusion', $exclusion);
  return confirm_form($form,
    t('Are you sure you want to delete exclusion %title?', array('%title' => entity_label('exclusion', $exclusion))),
    $exclusion_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function exclusion_delete_form_submit($form, &$form_state) {
  $exclusion = $form_state['exclusion'];
  exclusion_delete($exclusion);

  drupal_set_message(t('Exclusion %title deleted.', array('%title' => entity_label('exclusion', $exclusion))));

  $form_state['redirect'] = '<front>';
}
