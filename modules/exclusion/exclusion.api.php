<?php
/**
 * @file
 * Hooks provided by this module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Acts on exclusion being loaded from the database.
 *
 * This hook is invoked during $exclusion loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array $entities
 *   An array of $exclusion entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_exclusion_load(array $entities) {
  $result = db_query('SELECT pid, foo FROM {exclusion} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
 * Responds when a $exclusion is inserted.
 *
 * This hook is invoked after the $exclusion is inserted into the database.
 *
 * @param exclusion $exclusion
 *   The $exclusion that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_exclusion_insert(exclusion $exclusion) {
  db_insert('exclusion')
    ->fields(array(
      'id' => entity_id('exclusion', $exclusion),
      'extra' => print_r($exclusion, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a $exclusion being inserted or updated.
 *
 * This hook is invoked before the $exclusion is saved to the database.
 *
 * @param exclusion $exclusion
 *   The $exclusion that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_exclusion_presave(exclusion $exclusion) {
  $exclusion->name = 'foo';
}

/**
 * Responds to a $exclusion being updated.
 *
 * This hook is invoked after the $exclusion has been updated in the database.
 *
 * @param exclusion $exclusion
 *   The $exclusion that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_exclusion_update(exclusion $exclusion) {
  db_update('exclusion')
    ->fields(array('extra' => print_r($exclusion, TRUE)))
    ->condition('id', entity_id('exclusion', $exclusion))
    ->execute();
}

/**
 * Responds to $exclusion deletion.
 *
 * This hook is invoked after the $exclusion has been removed from the database.
 *
 * @param exclusion $exclusion
 *   The $exclusion that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_exclusion_delete(exclusion $exclusion) {
  db_delete('exclusion')
    ->condition('pid', entity_id('exclusion', $exclusion))
    ->execute();
}

/**
 * Act on a exclusion that is being assembled before rendering.
 *
 * @param $exclusion
 *   The exclusion entity.
 * @param $view_mode
 *   The view mode the exclusion is rendered in.
 * @param $langcode
 *   The language code used for rendering.
 *
 * The module may add elements to $exclusion->content prior to rendering. The
 * structure of $exclusion->content is a renderable array as expected by
 * drupal_render().
 *
 * @see hook_entity_prepare_view()
 * @see hook_entity_view()
 */
function hook_exclusion_view($exclusion, $view_mode, $langcode) {
  $exclusion->content['my_additional_field'] = array(
    '#markup' => $additional_field,
    '#weight' => 10,
    '#theme' => 'mymodule_my_additional_field',
  );
}

/**
 * Alter the results of entity_view() for exclusions.
 *
 * @param $build
 *   A renderable array representing the exclusion content.
 *
 * This hook is called after the content has been assembled in a structured
 * array and may be used for doing processing which requires that the complete
 * exclusion content structure has been built.
 *
 * If the module wishes to act on the rendered HTML of the exclusion rather than
 * the structured content array, it may use this hook to add a #post_render
 * callback. Alternatively, it could also implement hook_preprocess_exclusion().
 * See drupal_render() and theme() documentation respectively for details.
 *
 * @see hook_entity_view_alter()
 */
function hook_exclusion_view_alter($build) {
  if ($build['#view_mode'] == 'full' && isset($build['an_additional_field'])) {
    // Change its weight.
    $build['an_additional_field']['#weight'] = -10;

    // Add a #post_render callback to act on the rendered HTML of the entity.
    $build['#post_render'][] = 'my_module_post_render';
  }
}

/**
 * Acts on exclusion_type being loaded from the database.
 *
 * This hook is invoked during exclusion_type loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array $entities
 *   An array of exclusion_type entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_exclusion_type_load(array $entities) {
  $result = db_query('SELECT pid, foo FROM {exclusion} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
 * Responds when a exclusion_type is inserted.
 *
 * This hook is invoked after the exclusion_type is inserted into the database.
 *
 * @param exclusionType $exclusion_type
 *   The exclusion_type that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_exclusion_type_insert(exclusionType $exclusion_type) {
  db_insert('exclusion')
    ->fields(array(
      'id' => entity_id('exclusion_type', $exclusion_type),
      'extra' => print_r($exclusion_type, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a exclusion_type being inserted or updated.
 *
 * This hook is invoked before the exclusion_type is saved to the database.
 *
 * @param exclusionType $exclusion_type
 *   The exclusion_type that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_exclusion_type_presave(exclusionType $exclusion_type) {
  $exclusion_type->name = 'foo';
}

/**
 * Responds to a exclusion_type being updated.
 *
 * This hook is invoked after the exclusion_type has been updated in the database.
 *
 * @param exclusionType $exclusion_type
 *   The exclusion_type that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_exclusion_type_update(exclusionType $exclusion_type) {
  db_update('exclusion')
    ->fields(array('extra' => print_r($exclusion_type, TRUE)))
    ->condition('id', entity_id('exclusion_type', $exclusion_type))
    ->execute();
}

/**
 * Responds to exclusion_type deletion.
 *
 * This hook is invoked after the exclusion_type has been removed from the database.
 *
 * @param exclusionType $exclusion_type
 *   The exclusion_type that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_exclusion_type_delete(exclusionType $exclusion_type) {
  db_delete('exclusion')
    ->condition('pid', entity_id('exclusion_type', $exclusion_type))
    ->execute();
}

/**
 * Define default exclusion_type configurations.
 *
 * @return
 *   An array of default exclusion_type, keyed by machine names.
 *
 * @see hook_default_exclusion_type_alter()
 */
function hook_default_exclusion_type() {
  $defaults['main'] = entity_create('exclusion_type', array(
    // …
  ));
  return $defaults;
}

/**
 * Alter default exclusion_type configurations.
 *
 * @param array $defaults
 *   An array of default exclusion_type, keyed by machine names.
 *
 * @see hook_default_exclusion_type()
 */
function hook_default_exclusion_type_alter(array &$defaults) {
  $defaults['main']->name = 'custom name';
}
