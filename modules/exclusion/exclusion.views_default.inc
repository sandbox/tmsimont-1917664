<?php

/**
 * Implements hook_views_default_views().
 */
function exclusion_views_default_views() {
  $view = new view();
  $view->name = 'exclusion';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'exclusion';
  $view->human_name = 'Exclusions';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Exclusions';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'eid' => 'eid',
    'title' => 'title',
    'dsm_exc_dsid' => 'dsm_exc_dsid',
    'dsm_exclusions' => 'dsm_exclusions',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'eid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '<br/>',
      'empty_column' => 0,
    ),
    'dsm_exc_dsid' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'dsm_exclusions' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = '<a href="<?php print url(\'exclusion/add\'); ?>>Add Exclusion</a>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No repeat exclusions have been added to this pattern.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Relationship: Exclusion: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'exclusion';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['relationships']['uid']['label'] = 'Author';
  /* Field: Exclusion: Exclusion ID */
  $handler->display->display_options['fields']['eid']['id'] = 'eid';
  $handler->display->display_options['fields']['eid']['table'] = 'exclusion';
  $handler->display->display_options['fields']['eid']['field'] = 'eid';
  $handler->display->display_options['fields']['eid']['label'] = '';
  $handler->display->display_options['fields']['eid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['eid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['eid']['separator'] = '';
  /* Field: Exclusion: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'exclusion';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = 'dsm/!1/pattern/!2/exclusion/[eid]';
  /* Field: Exclusion: Patterns */
  $handler->display->display_options['fields']['dsm_pids']['id'] = 'dsm_pids';
  $handler->display->display_options['fields']['dsm_pids']['table'] = 'field_data_dsm_pids';
  $handler->display->display_options['fields']['dsm_pids']['field'] = 'dsm_pids';
  $handler->display->display_options['fields']['dsm_pids']['settings'] = array(
    'link' => 0,
  );
  $handler->display->display_options['fields']['dsm_pids']['delta_offset'] = '0';
  /* Field: Exclusion: Date sets */
  $handler->display->display_options['fields']['dsm_exc_dsid']['id'] = 'dsm_exc_dsid';
  $handler->display->display_options['fields']['dsm_exc_dsid']['table'] = 'field_data_dsm_exc_dsid';
  $handler->display->display_options['fields']['dsm_exc_dsid']['field'] = 'dsm_exc_dsid';
  $handler->display->display_options['fields']['dsm_exc_dsid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['dsm_exc_dsid']['settings'] = array(
    'link' => 0,
  );
  $handler->display->display_options['fields']['dsm_exc_dsid']['delta_offset'] = '0';
  /* Field: Exclusion: Dates */
  $handler->display->display_options['fields']['dsm_exclusions']['id'] = 'dsm_exclusions';
  $handler->display->display_options['fields']['dsm_exclusions']['table'] = 'field_data_dsm_exclusions';
  $handler->display->display_options['fields']['dsm_exclusions']['field'] = 'dsm_exclusions';
  $handler->display->display_options['fields']['dsm_exclusions']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  $handler->display->display_options['fields']['dsm_exclusions']['delta_offset'] = '0';

  /* Display: By Pattern */
  $handler = $view->new_display('block', 'By Pattern', 'block_1');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: User: Uid */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'users';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['relationship'] = 'uid';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Exclusion: Patterns (dsm_pids) */
  $handler->display->display_options['arguments']['dsm_pids_target_id']['id'] = 'dsm_pids_target_id';
  $handler->display->display_options['arguments']['dsm_pids_target_id']['table'] = 'field_data_dsm_pids';
  $handler->display->display_options['arguments']['dsm_pids_target_id']['field'] = 'dsm_pids_target_id';
  $handler->display->display_options['arguments']['dsm_pids_target_id']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['dsm_pids_target_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['dsm_pids_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['dsm_pids_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['dsm_pids_target_id']['summary_options']['items_per_page'] = '25';

  /* Display: By Dateset */
  $handler = $view->new_display('block', 'By Dateset', 'block_2');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No repeat exclusions have been added to this date set.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Exclusion: Exclusion ID */
  $handler->display->display_options['fields']['eid']['id'] = 'eid';
  $handler->display->display_options['fields']['eid']['table'] = 'exclusion';
  $handler->display->display_options['fields']['eid']['field'] = 'eid';
  $handler->display->display_options['fields']['eid']['label'] = '';
  $handler->display->display_options['fields']['eid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['eid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['eid']['separator'] = '';
  /* Field: Exclusion: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'exclusion';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = 'dsm/!1/dateset/!2/exclusion/[eid]';
  /* Field: Exclusion: Patterns */
  $handler->display->display_options['fields']['dsm_pids']['id'] = 'dsm_pids';
  $handler->display->display_options['fields']['dsm_pids']['table'] = 'field_data_dsm_pids';
  $handler->display->display_options['fields']['dsm_pids']['field'] = 'dsm_pids';
  $handler->display->display_options['fields']['dsm_pids']['settings'] = array(
    'link' => 0,
  );
  $handler->display->display_options['fields']['dsm_pids']['delta_offset'] = '0';
  /* Field: Exclusion: Date sets */
  $handler->display->display_options['fields']['dsm_exc_dsid']['id'] = 'dsm_exc_dsid';
  $handler->display->display_options['fields']['dsm_exc_dsid']['table'] = 'field_data_dsm_exc_dsid';
  $handler->display->display_options['fields']['dsm_exc_dsid']['field'] = 'dsm_exc_dsid';
  $handler->display->display_options['fields']['dsm_exc_dsid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['dsm_exc_dsid']['settings'] = array(
    'link' => 0,
  );
  $handler->display->display_options['fields']['dsm_exc_dsid']['delta_offset'] = '0';
  /* Field: Exclusion: Dates */
  $handler->display->display_options['fields']['dsm_exclusions']['id'] = 'dsm_exclusions';
  $handler->display->display_options['fields']['dsm_exclusions']['table'] = 'field_data_dsm_exclusions';
  $handler->display->display_options['fields']['dsm_exclusions']['field'] = 'dsm_exclusions';
  $handler->display->display_options['fields']['dsm_exclusions']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  $handler->display->display_options['fields']['dsm_exclusions']['delta_offset'] = '0';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: User: Uid */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'users';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['relationship'] = 'uid';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Exclusion: Date sets (dsm_exc_dsid) */
  $handler->display->display_options['arguments']['dsm_exc_dsid_target_id']['id'] = 'dsm_exc_dsid_target_id';
  $handler->display->display_options['arguments']['dsm_exc_dsid_target_id']['table'] = 'field_data_dsm_exc_dsid';
  $handler->display->display_options['arguments']['dsm_exc_dsid_target_id']['field'] = 'dsm_exc_dsid_target_id';
  $handler->display->display_options['arguments']['dsm_exc_dsid_target_id']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['dsm_exc_dsid_target_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['dsm_exc_dsid_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['dsm_exc_dsid_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['dsm_exc_dsid_target_id']['summary_options']['items_per_page'] = '25';


  $views['exclusion'] = $view;

  return $views;
}