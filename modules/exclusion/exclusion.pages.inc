<?php

/**
 * Exclusion view callback.
 */
function exclusion_view($exclusion) {
  drupal_set_title(entity_label('exclusion', $exclusion));
  return entity_view('exclusion', array(entity_id('exclusion', $exclusion) => $exclusion), 'full', NULL, TRUE);
}


function exclusion_dateset_tab($dateset) {
  return views_embed_view('exclusion', 'block_2', $dateset->uid, $dateset->dsid);
}
