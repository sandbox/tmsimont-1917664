<?php


/**
 * UI controller for Exclusion Type.
 */
class ExclusionTypeUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage exclusion types.';
    return $items;
  }
}