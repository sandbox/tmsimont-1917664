<?php


class ExclusionTypeController extends EntityAPIControllerExportableWithFields {
   public function create(array $values = array()) {
    $values += array(
      'label' => '',
      'description' => '',
    );
    return parent::create($values);
  }

  /**
   * Save Exclusion Type.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    parent::save($entity, $transaction);
    // Rebuild menu registry. We do not call menu_rebuild directly, but set
    // variable that indicates rebuild in the end.
    // @see http://drupal.org/node/1399618
    variable_set('menu_rebuild_needed', TRUE);
  }
  
  public function attachDefaultFields($entity) {
    $bundle_name = $entity->type;
    //@todo: re-implement unique-to-type dsid fields.  the problem here is that this messes up views that need to use the ref field.  a workaround would be intense.
    // $dsid_ref_field_name = $bundle_name;
    //@see: Exclusion::dateset_ref_field_name()
    $dsid_ref_field_name = "exc";
    //@TODO: move this var to another file
    $export = '{
  "fields" : {
    "dsm_pids" : {
      "translatable" : "0",
      "entity_types" : [],
      "settings" : {
        "profile2_private" : false,
        "target_type" : "pattern",
        "handler" : "base",
        "handler_settings" : {
          "target_bundles" : [],
          "sort" : { "type" : "none" },
          "behaviors" : { "views-select-list" : { "status" : 0 } }
        }
      },
      "storage" : {
        "type" : "field_sql_storage",
        "settings" : [],
        "module" : "field_sql_storage",
        "active" : "1",
        "details" : { "sql" : {
            "FIELD_LOAD_CURRENT" : { "field_data_dsm_pids" : { "target_id" : "dsm_pids_target_id" } },
            "FIELD_LOAD_REVISION" : { "field_revision_dsm_pids" : { "target_id" : "dsm_pids_target_id" } }
          }
        }
      },
      "foreign keys" : { "pattern" : { "table" : "pattern", "columns" : { "target_id" : "pid" } } },
      "indexes" : { "target_id" : [ "target_id" ] },
      "field_name" : "dsm_pids",
      "type" : "entityreference",
      "module" : "entityreference",
      "active" : "1",
      "locked" : "0",
      "cardinality" : "-1",
      "deleted" : "0",
      "columns" : { "target_id" : {
          "description" : "The id of the target entity.",
          "type" : "int",
          "unsigned" : true,
          "not null" : true
        }
      },
      "bundles" : { "exclusion" : [ "' . $bundle_name . '" ] }
    },
    "dsm_exclusions" : {
      "translatable" : "0",
      "entity_types" : [],
      "settings" : {
        "granularity" : {
          "month" : "month",
          "day" : "day",
          "year" : "year",
          "hour" : 0,
          "minute" : 0,
          "second" : 0
        },
        "tz_handling" : "none",
        "timezone_db" : "",
        "cache_enabled" : 0,
        "cache_count" : "4",
        "repeat" : "0",
        "profile2_private" : false,
        "todate" : "optional"
      },
      "storage" : {
        "type" : "field_sql_storage",
        "settings" : [],
        "module" : "field_sql_storage",
        "active" : "1",
        "details" : { "sql" : {
            "FIELD_LOAD_CURRENT" : { "field_data_dsm_exclusions" : { "value" : "dsm_exclusions_value", "value2" : "dsm_exclusions_value2" } },
            "FIELD_LOAD_REVISION" : { "field_revision_dsm_exclusions" : { "value" : "dsm_exclusions_value", "value2" : "dsm_exclusions_value2" } }
          }
        }
      },
      "foreign keys" : [],
      "indexes" : [],
      "field_name" : "dsm_exclusions",
      "type" : "datetime",
      "module" : "date",
      "active" : "1",
      "locked" : "0",
      "cardinality" : "-1",
      "deleted" : "0",
      "columns" : {
        "value" : {
          "type" : "datetime",
          "mysql_type" : "datetime",
          "pgsql_type" : "timestamp without time zone",
          "sqlite_type" : "varchar",
          "sqlsrv_type" : "smalldatetime",
          "not null" : false,
          "sortable" : true,
          "views" : true
        },
        "value2" : {
          "type" : "datetime",
          "mysql_type" : "datetime",
          "pgsql_type" : "timestamp without time zone",
          "sqlite_type" : "varchar",
          "sqlsrv_type" : "smalldatetime",
          "not null" : false,
          "sortable" : true,
          "views" : false
        }
      },
      "bundles" : { "exclusion" : [ "' . $bundle_name . '" ] }
    },
    "dsm_' . $dsid_ref_field_name . '_dsid" : {
      "translatable" : "0",
      "entity_types" : [],
      "settings" : {
        "profile2_private" : false,
        "target_type" : "dateset",
        "handler" : "base",
        "handler_settings" : {
          "target_bundles" : [],
          "sort" : { "type" : "none" },
          "behaviors" : { "views-select-list" : { "status" : 0 } }
        }
      },
      "storage" : {
        "type" : "field_sql_storage",
        "settings" : [],
        "module" : "field_sql_storage",
        "active" : "1",
        "details" : { "sql" : {
            "FIELD_LOAD_CURRENT" : { "field_data_dsm_' . $dsid_ref_field_name . '_dsid" : { "target_id" : "dsm_' . $dsid_ref_field_name . '_dsid_target_id" } },
            "FIELD_LOAD_REVISION" : { "field_revision_dsm_' . $dsid_ref_field_name . '_dsid" : { "target_id" : "dsm_' . $dsid_ref_field_name . '_dsid_target_id" } }
          }
        }
      },
      "foreign keys" : { "dateset" : { "table" : "dateset", "columns" : { "target_id" : "dsid" } } },
      "indexes" : { "target_id" : [ "target_id" ] },
      "field_name" : "dsm_' . $dsid_ref_field_name . '_dsid",
      "type" : "entityreference",
      "module" : "entityreference",
      "active" : "1",
      "locked" : "0",
      "cardinality" : "-1",
      "deleted" : "0",
      "columns" : { "target_id" : {
          "description" : "The id of the target entity.",
          "type" : "int",
          "unsigned" : true,
          "not null" : true
        }
      },
      "bundles" : { "exclusion" : [ "' . $bundle_name . '" ] }
    },
    "dsm_apply_to" : {
      "translatable" : "0",
      "entity_types" : [],
      "settings" : {
        "allowed_values" : {
          "pattern" : "All dates in this pattern",
          "datesets" : "Only specific dates in this pattern",
          "patterns_all" : "All dates in multiple patterns",
          "patterns_picked" : "Specific dates in specific patterns"
        },
        "allowed_values_function" : "",
        "profile2_private" : false
      },
      "storage" : {
        "type" : "field_sql_storage",
        "settings" : [],
        "module" : "field_sql_storage",
        "active" : "1",
        "details" : { "sql" : {
            "FIELD_LOAD_CURRENT" : { "field_data_dsm_apply_to" : { "value" : "dsm_apply_to_value" } },
            "FIELD_LOAD_REVISION" : { "field_revision_dsm_apply_to" : { "value" : "dsm_apply_to_value" } }
          }
        }
      },
      "foreign keys" : [],
      "indexes" : { "value" : [ "value" ] },
      "field_name" : "dsm_apply_to",
      "type" : "list_text",
      "module" : "list",
      "active" : "1",
      "locked" : "0",
      "cardinality" : "1",
      "deleted" : "0",
      "columns" : { "value" : { "type" : "varchar", "length" : 255, "not null" : false } },
      "bundles" : { "exclusion" : [ "' . $bundle_name . '" ] }
    }
  },
  "field_instances" : {
    "dsm_pids" : {
      "label" : "Patterns",
      "widget" : {
        "weight" : "2",
        "type" : "options_buttons",
        "module" : "options",
        "active" : 1,
        "settings" : []
      },
      "settings" : { "user_register_form" : false },
      "display" : { "default" : { "label" : "above", "type" : "hidden", "weight" : "3", "settings" : [] } },
      "required" : 0,
      "description" : "",
      "default_value" : null,
      "field_name" : "dsm_pids",
      "entity_type" : "exclusion",
      "bundle" : "' . $bundle_name . '",
      "deleted" : "0"
    },
    "dsm_exclusions" : {
      "label" : "Dates",
      "widget" : {
        "weight" : "0",
        "type" : "date_popup",
        "module" : "date",
        "active" : 1,
        "settings" : {
          "input_format" : "m\/d\/Y - H:i:s",
          "input_format_custom" : "",
          "year_range" : "-0:+3",
          "increment" : "15",
          "label_position" : "above",
          "text_parts" : [],
          "display_all_day" : 0,
          "repeat_collapsed" : 0
        }
      },
      "settings" : {
        "default_value" : "now",
        "default_value_code" : "",
        "default_value2" : "same",
        "default_value_code2" : "",
        "user_register_form" : false
      },
      "display" : { "default" : {
          "label" : "above",
          "type" : "date_default",
          "weight" : "0",
          "settings" : {
            "format_type" : "short",
            "fromto" : "both",
            "multiple_number" : "",
            "multiple_from" : "",
            "multiple_to" : "",
            "show_repeat_rule" : "show"
          },
          "module" : "date"
        }
      },
      "required" : 0,
      "description" : "",
      "field_name" : "dsm_exclusions",
      "entity_type" : "exclusion",
      "bundle" : "' . $bundle_name . '",
      "deleted" : "0"
    },
    "dsm_' . $dsid_ref_field_name . '_dsid" : {
      "label" : "Date sets",
      "widget" : {
        "weight" : "3",
        "type" : "options_buttons",
        "module" : "options",
        "active" : 1,
        "settings" : []
      },
      "settings" : { "user_register_form" : false },
      "display" : { "default" : { "label" : "above", "type" : "hidden", "weight" : "1", "settings" : [] } },
      "required" : 0,
      "description" : "",
      "default_value" : null,
      "field_name" : "dsm_' . $dsid_ref_field_name . '_dsid",
      "entity_type" : "exclusion",
      "bundle" : "' . $bundle_name . '",
      "deleted" : "0"
    },
    "dsm_apply_to" : {
      "label" : "Apply exclusion to",
      "widget" : {
        "weight" : "1",
        "type" : "options_select",
        "module" : "options",
        "active" : 1,
        "settings" : []
      },
      "settings" : { "user_register_form" : false },
      "display" : { "default" : { "label" : "above", "type" : "hidden", "weight" : "2", "settings" : [] } },
      "required" : 1,
      "description" : "",
      "default_value" : null,
      "field_name" : "dsm_apply_to",
      "entity_type" : "exclusion",
      "bundle" : "' . $bundle_name . '",
      "deleted" : "0"
    }
  }
}';
    $this->importFields($export);
  }
  
}