<?php 

class ExclusionFormBuilder {
  
  public function __construct(&$form, &$form_state, $exclusion, $parent_entity) {
    //set up references back to the Drupal form build function vars
    $this->form = &$form;
    $this->form_state = &$form_state;
    $this->exclusion = $exclusion;
    $this->parent_entity = $parent_entity;
    
    
    //set up quick references to buried Drupal-style form vars
    $this->dsid_ref_field_name = $form['#entity']->dateset_ref_field_name();
    $this->dsid_ref_field = &$this->form[$this->dsid_ref_field_name];
    $this->dsid_ref_field_data = &$this->dsid_ref_field[$this->form[$this->dsid_ref_field_name]['#language']];
    $this->dsid_ref_field_options = &$this->dsid_ref_field_data['#options'];
    
    $this->apply_to_field_name = 'dsm_apply_to';
    $this->apply_to_field = &$this->form[$this->apply_to_field_name];
    $this->apply_to_field_data = &$this->form[$this->apply_to_field_name][$this->form[$this->apply_to_field_name]['#language']];
    $this->apply_to_field_options = &$this->apply_to_field_data['#options'];
    
    $this->pattern_ref_field_name = 'dsm_pids';
    $this->pattern_ref_field = &$this->form[$this->pattern_ref_field_name];
    $this->pattern_ref_field_data = &$this->pattern_ref_field[$this->form[$this->dsid_ref_field_name]['#language']];
    $this->pattern_ref_field_options = &$this->pattern_ref_field[$this->form[$this->dsid_ref_field_name]['#language']]['#options'];
    
  }
  
  public function attach_form() {
    $datesets = $this->get_datesets();
    $patterns = $this->get_patterns();
    $datesets_by_pattern = pattern_get_repeating_datesets_by_pattern($datesets);
    
    // @todo: what about "cancellations" that might be relevant to non-repeating dates?
    $this->remove_set_dates_from_form($datesets);

    $this->apply_states();
    
    // use a helper "fielder" class for the given parent entity type make entity-type-specific alterations
    $parentFielderType = 'ExclusionFormBuilderParentBasedFields' . get_class($this->parent_entity);
    $parentFielder = new $parentFielderType($this);
    $parentFielder->set_defaults();
    
    $this->attach_parent_pattern_pid($parentFielder);
    $this->attach_old_dsids();
    
    // help text to explain "this pattern" references in "Apply exclusion to" options
    $this->apply_to_field_data['#description'] .= '<div>' . t("\"This pattern\" refers to the %title repeating date pattern", array("%title" => $parentFielder->parent_pattern->title)) . '</div>';
    
    
    if (count($this->dsid_ref_field_options) == 1) {
    
      // only 1 dateset is an option, don't confuse the UI unnecessarily
      $this->remove_apply_to_field();
      
    } elseif (count($this->dsid_ref_field_options) < 1) {
    
      // @todo: what if there are no options for datesets?  should the user even be here?
      // (user might be adding an exception to repeat pattern before adding a date set)
      
    } else {
    
      // pass mods off to parentFielder class to react based on exclusion parent type
      $parentFielder->apply_mods($datesets, $patterns, $datesets_by_pattern);
      
    }
    
    if (count($patterns) > 1) {
      // multiple patterns are available in the dateset field pool
    }
    
  }
  
  /**
   * Pass the parent pattern pid to the form for submit/validation handlers
   */
  function attach_parent_pattern_pid($parentFielder) {
    $parent_pattern = $parentFielder->parent_pattern();
    $this->form['parent_pattern_pid'] = array(
      '#type' => "hidden",
      '#value' => $parent_pattern->pid,
    );
  }
  
  /**
   * Pass the "old dsids" into the form so any removals will be recognized
   */
  function attach_old_dsids() {
    if (!isset($this->exclusion->is_new)) {
      $dsids = $this->exclusion->dsids();
      $this->form['old_dsids'] = array(
        '#type' => "hidden",
        '#value' => implode(",",$dsids),
      );
    }
  }
  
  /**
   * apply states to form fields
   */
  function apply_states() {
    // no-js messaging for states fallback
    $this->apply_to_field_data['#description'] .= '<div class="js-hide"><em>' . t("The selection you make in the above drop-down box supercedes the selections made with the checkboxes below.") . '</em></div>';

    $this->pattern_ref_field['#states'] = array(
      'visible' => array(
        array(
          ':input[name="' . $this->apply_to_field_name . '[und]"]' => array(
            array("value" => 'patterns_all'),
            array("value" => 'patterns_picked'),
          ),
        ),
      ),
    );
    
    $this->form[$this->dsid_ref_field_name]['#states'] = array(
      'visible' => array(
        array(
          ':input[name="' . $this->apply_to_field_name . '[und]"]' => array(
            array("value" => 'datesets'),
            array("value" => 'patterns_picked'),
          ),
        ),
      ),
    );
  }
    
  /**
   * get the pattern options available to this exclusion_form
   */
  function get_patterns() {
    // get dateset pattern data for pids available in pattern_ref_field_name field
    $pids = array_keys($this->pattern_ref_field_options);
    return pattern_load_multiple($pids);
  }

  /**
   * get the dateset options available to this exclusion_form
   */
  function get_datesets() {
    // pull dateset data out of the form options and load dateset objects
    $dsids = array_keys($this->dsid_ref_field_options);
    return dateset_load_multiple($dsids);
  }

  /**
   * Remove set date form options for this exclusion_form given an 
   * array of complete dateset entity objects
   */
  function remove_set_dates_from_form($datesets) {
    foreach ($datesets as $dateset) {
      if (!$dateset->isRepeating()) {
        unset($this->dsid_ref_field_options[$dateset->dsid]);
      }
    }
  }
   
  /**
   * take the "apply to field" and related fields off of the form
   */
  function remove_apply_to_field() {
    $this->apply_to_field_options = array(
      'pattern' => $this->apply_to_field_options['pattern']
    );
    $this->apply_to_field_data['#default_value'] = 'pattern';
    $this->apply_to_field_data['#access'] = false;
    $this->dsid_ref_field['#access'] = false;
    $this->pattern_ref_field['#access'] = false;
  }
}


class ExclusionFormBuilderParentBasedFields {
  public function __construct(&$form_builder) {
    $this->form_builder = &$form_builder;
    //notice required pid() and parent_pattern() functions for child classes
    $this->pid = $this->pid();
    $this->parent_pattern = $this->parent_pattern(); 
  }
  
  /**
   * extensions may override at this point to set default field values
   */
  function set_defaults() {
  }
  /**
   * extensions may override at this point to react to only 1 dateset in the parent pattern
   */
  function alter_for_single_dateset() {
  }
  
  /**
   * modify the form based on datesets and patterns available as options
   */
  function apply_mods($datesets, $patterns, $datesets_by_pattern) {
    $fb = &$this->form_builder;
    if (count($patterns) > 1 ) {
      $this->create_checkbox_optgroups($patterns, $datesets_by_pattern);
    } elseif (count($patterns) < 1) {
      // @todo: what if there are no patterns?  should the user even be here?      
    } else {
      $this->remove_multiple_pattern_options();
    }
    if (count($datesets_by_pattern[$this->pid])==1) {
      $this->alter_for_single_dateset();
    }
  }
  
  /**
   * only 1 pattern, don't show "multiple pattern" options
   */
  function remove_multiple_pattern_options() {  
    unset($this->form_builder->apply_to_field_options['patterns_all']);
    unset($this->form_builder->apply_to_field_options['patterns_picked']);
  }
  
  /**
   * this user has multiple patterns, provide a UI for managing datesets in each pattern.
   * this will remove the dsid_ref_field from the form and create a dummy "option group" for
   * each pattern.  Basically, this is creating "option group" support for checkboxes.  The 
   * faux option groups can then be hidden and shown with the states array.
   * The submit handler needs to react to the faux option groups data received.
   */
  function create_checkbox_optgroups($patterns, $datesets_by_pattern) {
    $fb = &$this->form_builder;
    
    // @todo: stick this somewhere that the validation/submit handler can get it?
    $option_group_name = 'dsm_dsids_for_pid';
    
    // initialize an option group container that mimics dsid_ref_field
    $fb->form[$option_group_name] = array(
      '#type' => 'container',
      '#weight' => $fb->dsid_ref_field['#weight'],
      '#states' => $fb->dsid_ref_field['#states'],
      // no-js messaging for states fallback
      '#suffix' => '<div class="js-hide"><em>' . t("If you don't select the patterns for your selected dates, the dates will be ignored.  Make sure your \"Apply exclusion to\" and \"Patterns\" selections match your date selections.") . '</em></div>',
    );
    
    // sort dateset objects into associative array keyed by pid
    foreach ($datesets_by_pattern as $ds_pid => $datesets) {
      // if this "option group" is not set into the form, initiate it
      if (!isset($fb->form[$option_group_name][$ds_pid])) {
        $pattern = isset($patterns[$ds_pid]) ? $patterns[$ds_pid] : pattern_load($ds_pid);
        
        // create a new dummy "optgroup" checkbox group
        $fb->form[$option_group_name]['dsm_dsid_faux_optgroup_' . $ds_pid] = array(
          '#type' => 'checkboxes'
        );
        
        // make the title reference the pattern
        $fb->form[$option_group_name]['dsm_dsid_faux_optgroup_' . $ds_pid]['#title'] = t("Dates in ") . $pattern->title;
        
        // set up #states to only show this option group when the relevant parent pattern is checked
        $fb->form[$option_group_name]['dsm_dsid_faux_optgroup_' . $ds_pid]['#states'] = array(
          'visible' => array(
            ':input[name="' . $fb->pattern_ref_field_name . '[und][' . $ds_pid . ']"]' => array("checked" => true),
          ),
        );
        
        // if this is not the optgroup for "this" pattern, then only show it when "patterns_picked" is selected
        if ($this->parent_pattern->pid != $ds_pid) {
          $fb->form[$option_group_name]['dsm_dsid_faux_optgroup_' . $ds_pid]['#states']['visible'][':input[name="' . $fb->apply_to_field_name . '[und]"]'] = array("value" => "patterns_picked");;
        } else {
          $fb->form[$option_group_name]['dsm_dsid_faux_optgroup_' . $ds_pid]['#states'] = array(
            'visible' => array(
              // use of "OR" states does not work, see http://drupal.org/node/735528#comment-7094044
              // ':input[name="' . $fb->apply_to_field_name . '[und]"]' => array("value" => "datesets"),
              // 'or',
              ':input[name="' . $fb->pattern_ref_field_name . '[und][' . $ds_pid . ']"]' => array("checked" => true),
              ':input[name="' . $fb->apply_to_field_name . '[und]"]' => array("value" => "patterns_picked"),
            ),
          );
        }
        
        // initialize the options of the option group
        $fb->form[$option_group_name]['dsm_dsid_faux_optgroup_' . $ds_pid]['#options'] = array();
      }
      
      // add the options to the "optgroup"
      foreach ($datesets as $dateset) {
        $fb->form[$option_group_name]['dsm_dsid_faux_optgroup_' . $ds_pid]['#options'][$dateset->dsid] = $dateset->title;
      }
      
      // set up default values based on the "real" field
      $fb->form[$option_group_name]['dsm_dsid_faux_optgroup_' . $ds_pid]['#default_value'] = array();
      foreach ($fb->dsid_ref_field_data['#default_value'] as $dsid) {
        if (isset($fb->form[$option_group_name]['dsm_dsid_faux_optgroup_' . $ds_pid]['#options'][$dsid])) {
          $fb->form[$option_group_name]['dsm_dsid_faux_optgroup_' . $ds_pid]['#default_value'][] = $dsid;
        }
      }
    }

    // use the "real" field only for the "only specific dates in this pattern" apply_to value
    $fb->dsid_ref_field_options = $fb->form[$option_group_name]['dsm_dsid_faux_optgroup_' . $this->parent_pattern->pid]['#options'];
    $fb->dsid_ref_field['#states'] = array(
      'visible' => array(
        array(
          ':input[name="' . $fb->apply_to_field_name . '[und]"]' => array("value" => "datesets"),
        )
      )
    );
  }
}

class ExclusionFormBuilderParentBasedFieldsDateset extends ExclusionFormBuilderParentBasedFields {
  function __construct($form_builder) {
    parent::__construct($form_builder);
    $this->dsid = $form_builder->parent_entity->dsid;
  }

  function pid() {
    return $this->form_builder->parent_entity->pid();
  }

  function parent_pattern() {
    return pattern_load($this->pid());
  }

  /**
   * only 1 datest can be used, don't show "specific dateset" options related to "this" pattern
   */
  function alter_for_single_dateset() {
    unset($this->form_builder->apply_to_field_options['datesets']);
  }

  function set_defaults() {
    $fb = &$this->form_builder;
    // set default dsid to "this" pattern based on dsid passed to form
    if (isset($this->dsid) && $this->dsid != 0) {
      if ($applies = $fb->exclusion->appliesTo()) {
        $fb->apply_to_field_data['#default_value'] = $applies;
      } else {
        $fb->dsid_ref_field_data['#default_value'] = array('target_id'=>$this->dsid);
        $fb->apply_to_field_data['#default_value'] = 'datesets';
      }
    }
  }
}

class ExclusionFormBuilderParentBasedFieldsPattern extends ExclusionFormBuilderParentBasedFields {
  function pid() {
    return $this->form_builder->parent_entity->pid;
  }

  function parent_pattern() {
    return $this->form_builder->parent_entity;
  }
  
  /**
   * only 1 datest can be used, don't show "specific dateset" options related to "this" pattern
   */
  function alter_for_single_dateset() {
    unset($this->form_builder->apply_to_field_options['datesets']);
  }

  function set_defaults() {
    $fb = &$this->form_builder;
    // set default pattern to "this" pattern based on pid passed to form
    if ($this->pid() && $this->pid() != 0) {
      if (!$fb->exclusion->pids()) {
        $fb->pattern_ref_field_data['#default_value'] = array('target_id'=>$this->pid());
        $fb->apply_to_field_data['#default_value'] = 'pattern';
      }
    }
  }
}