<?php


/**
 * Exclusion class.
 */
class Exclusion extends Entity {
  protected function defaultLabel() {
    return $this->title;
  }

  protected function defaultUri() {
    extract($this->getPatternPositions());
    return array('path' => "dsm/$uid/pattern/$pid/exclusion/" . $this->identifier());
  }
  
  /**
   * Pull various pattern-positioning values from fields on the exclusion
   * @todo: don't use this shitty function and replace with better with support for multiple pids
   * @see: $this->pids()
   */
  public function getPatternPositions() {
    return array(
      'uid' => !empty($this->uid)?$this->uid:0,
      'pid' => !empty($this->dsm_pids[LANGUAGE_NONE])?$this->dsm_pids[LANGUAGE_NONE][0]['target_id']:0,
    );
  }
  
  /**
   * Get the parent pattern object for this exclusion.  All exclusions should have a parent
   */
  public function parentPattern() {
    // @see: ExclusionFormBuider::attach_parent_pattern_pid()
    if (isset($this->parent_pattern_pid)) {
      return pattern_load($this->parent_pattern_pid);
    } else {
      // could be multiple parents, just return the first one...??
      $patterns = $this->patterns();
      if (count($patterns) > 0) {
        return $patterns[0];
      }
    }
    // bad news
    return false;
  }
  
  /**
   * Return a list of dsids for each dateset this exclusion pertains to
   */
  public function dsids($old = false) {
    // might be looking for "old_dsids"
    // @see: ExclusionFormBuilder::attach_old_dsids()
    if ($old && isset($this->old_dsids)) {
      return explode(",", $this->old_dsids);
    }
    
    $dsids_val = !empty($this->{$this->dateset_ref_field_name()}[LANGUAGE_NONE])?$this->{$this->dateset_ref_field_name()}[LANGUAGE_NONE]:array();
    $dsids = array();
    foreach ($dsids_val as $fieldval) {
      $dsids[] = $fieldval['target_id'];
    }
    return $dsids;
  }
  
  /**
   * Return a list of loaded dateset objects this exclusion pertains to
   */
  public function datesets($old = false) {
    return dateset_load_multiple($this->dsids($old));
  }
  
  /**
   * Return a list of pids for each pattern this exclusion pertains to
   */
  public function pids() {
    if (!isset($this->dsm_pids[LANGUAGE_NONE]) || empty($this->dsm_pids[LANGUAGE_NONE])) {
      return false;
    }
    $pids = array();
    foreach ($this->dsm_pids[LANGUAGE_NONE] as $field_val) {
      $pids[] = $field_val['target_id'];
    }
    return $pids;
  }
  
  /**
   * Return a list of loaded pattern objects for each pattern this exclusion pertains to
   */
  public function patterns() {
    return pattern_load_multiple($this->pids());
  }
  
  /** 
   * Get the unique field name for the dsid reference field.
   * This is different for each bundle so each bundle can be individually configured.
   */
  public function dateset_ref_field_name($type=false) {
    return 'dsm_exc_dsid';
    //@todo: re-implement unique-to-type dsid fields.  the problem here is that this messes up views that need to use the ref field.  a workaround would be intense.
    //@see: ExclusionTypeController::attachDefaultFields()
    if ($type === false) {
      $type = $this->type;
    }
    return 'dsm_' . $type . '_dsid';
  }
  
  /**
   * return the type of application this exclusion is configured to be.
   */
  public function appliesTo() {
    if (!isset($this->dsm_apply_to[LANGUAGE_NONE])) {
      return false;
    }
    if (empty($this->dsm_apply_to[LANGUAGE_NONE][0])) {
      return false;
    }
    if (empty($this->dsm_apply_to[LANGUAGE_NONE][0]['value'])) {
      return false;
    }
    return $this->dsm_apply_to[LANGUAGE_NONE][0]['value'];
  }
  
  /**
   * Get an array of date exclusions formatted to pass to _date_repeat_calc()
   */
  public function getFormattedDateExclusions() {
    $formatted = array();
    foreach ($this->dsm_exclusions[LANGUAGE_NONE] as $date_field) {
      $start = new DateObject($date_field['value'], $date_field['timezone']);
      $formatted[] = $start->format("Y-m-d");
      
      //check for end date indicating numerous days of exception
      $end = new DateObject($date_field['value2'], $date_field['timezone']);
      $dif = $start->difference($end, 'days');
      if ($dif > 0) {
        for ($i=1; $i<$dif; $i++) {
          $start->add(new DateInterval("P1D"));
          $formatted[] = $start->format("Y-m-d");
        }
        $formatted[] = $end->format("Y-m-d");
      }
      
    }
    return $formatted;
  }
  
  /**
   * override Entity API CRUD save.
   * configure the dates field based on pattern data.
   * This override makes it possible for the pattern entity UI to manage complex
   * repeat rules.
   */
  public function save() {
    
    // "All in this pattern" selected, make sure dsid ref pattern has selected all dsid's in "this pattern'
    if ($this->appliesTo() == 'pattern') {
      // deselect everything else first
      $this->{$this->dateset_ref_field_name()}[LANGUAGE_NONE] = array();
      // populate selection based on parent pattern selection
      $pattern = $this->parentPattern();
      foreach ($pattern->datesets_flat() as $dateset) {
        $this->{$this->dateset_ref_field_name()}[LANGUAGE_NONE][] = array(
          'target_id' => $dateset->dsid,
        );
      }
    }

    // only specific datesets in "this pattern" -- make sure "this pattern" is the only pid referenced
    if ($this->appliesTo() == 'datesets') {
      $parent_pattern = $this->parentPattern();
      $pid = $parent_pattern->pid;
      // reset the field with 1 value
      $this->dsm_pids[LANGUAGE_NONE] = array(
        array( 'target_id' => $pid ),
      );
      //normal dateset_ref field should have accepted actual values only
    }
    
    // "All in multiple patterns" selected, select only all dsid's in each pattern
    if ($this->appliesTo() == 'patterns_all') {
      // deselect everything else first
      $this->{$this->dateset_ref_field_name()}[LANGUAGE_NONE] = array();
      // populate selection based on parent pattern selections
      foreach ($this->patterns() as $pattern) {
        foreach ($pattern->datesets_flat() as $dateset) {
          $this->{$this->dateset_ref_field_name()}[LANGUAGE_NONE][] = array(
            'target_id' => $dateset->dsid,
          );
        }
      }
    }
    
    // hand-picked patterns and datesets, apply hand-picked faux-fields into selected patterns
    if ($this->appliesTo() == 'patterns_picked') {
      $pids = $this->pids();
      // deselect everything else first
      $this->{$this->dateset_ref_field_name()}[LANGUAGE_NONE] = array();
      $this->dsm_pids[LANGUAGE_NONE] = array();
      foreach ($pids as $pid) {
        $group = 'dsm_dsid_faux_optgroup_' . $pid;
        $from_this_pattern = 0;
        if (isset($this->{$group})) {
          foreach ($this->{$group} as $dsid) {
            if ($dsid != 0) {
              $this->{$this->dateset_ref_field_name()}[LANGUAGE_NONE][] = array(
                'target_id' => $dsid,
              );
              $from_this_pattern++;
            }
          }
        }
        if ($from_this_pattern > 0 ) {
          $this->dsm_pids[LANGUAGE_NONE][] = array('target_id' => $pid);
        }
      }
    }
  
    parent::save();
    
    // now that this entity has exclusion data, update "old" referenced datesets
    foreach ($this->datesets(true) as $dateset) {
      $dateset->save();
    }
    // now that this entity has exclusion data, update "new" referenced datesets
    foreach ($this->datesets() as $dateset) {
      $dateset->save();
    }
  }
  
  /**
   * override Entity API CRUD delete.
   * configure the dates field based on pattern data.
   * This override makes it possible for the pattern entity UI to manage complex
   * repeat rules.
   */
  public function delete() {
    $datesets = $this->datesets();
    
    parent::delete();
    
    // now that this entity is gone, update referenced datesets
    foreach ($datesets as $dateset) {
      $dateset->save();
    }
  }
  
}
