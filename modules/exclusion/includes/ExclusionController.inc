<?php

class ExclusionController extends EntityAPIController {

  public function create(array $values = array()) {
    global $user;
    $values += array(
      'title' => '',
      'description' => '',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
    );
    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('exclusion', $entity);
    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

  public function view($entities, $view_mode = 'full', $langcode = NULL, $page = NULL) {
    return parent::view($entities, $view_mode, $langcode, $page);
  }
  
}